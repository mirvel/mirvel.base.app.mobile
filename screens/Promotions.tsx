import React from 'react';
import { StyleSheet, Dimensions, ScrollView, View, FlatList, Text } from 'react-native';
import { Block, theme } from '../components/custom/src';
import { Card } from '../components';
import articles from '../constants/articles';
import { BaseComponent } from "../BaseComponent";
import FabMenu from './FabMenu';
import { AppContext } from '../AppContext';


const { width } = Dimensions.get('screen');

class Promotions extends BaseComponent {
  static contextType = AppContext;

  subscriptions$ = [];

  renderArticles = () => {
    const { navigation } = this.props;
    return (
      <View style={{ flex: 1, backgroundColor: '#f3f3f3' }}>
        <ScrollView
          showsVerticalScrollIndicator={false}
          contentContainerStyle={styles.articles}>
          <Block flex>
            <Card item={articles[0]} horizontal imageStyle={{ flex: 1, height: undefined, width: undefined }} imageContainerStyle={{ overflow: 'visible', flex: 1, height: undefined, width: undefined }} />
            <Card item={articles[1]} horizontal imageStyle={{ flex: 1, height: undefined, width: undefined }} imageContainerStyle={{ overflow: 'visible', flex: 1, height: undefined, width: undefined }} />
            <Card item={articles[2]} horizontal imageStyle={{ flex: 1, height: undefined, width: undefined }} imageContainerStyle={{ overflow: 'visible', flex: 1, height: undefined, width: undefined }} />
            <Card item={articles[3]} horizontal imageStyle={{ flex: 1, height: undefined, width: undefined }} imageContainerStyle={{ overflow: 'visible', flex: 1, height: undefined, width: undefined }} />
            <Card item={articles[4]} horizontal imageStyle={{ flex: 1, height: undefined, width: undefined }} imageContainerStyle={{ overflow: 'visible', flex: 1, height: undefined, width: undefined }} />
          </Block>
        </ScrollView>
        <FabMenu navigation={navigation} />
      </View>
    )
  }

  componentDidMount() { }

  componentWillUnmount() {
    this.subscriptions$.forEach(subscription => subscription.unsubscribe());
  }
  render() {
    return (
      <Block flex center style={styles.home}>
        {this.renderArticles()}
      </Block>
    );
  }
}

const styles = StyleSheet.create({
  home: {
    width: width,
  },
  articles: {
    width: width - theme.SIZES.BASE * 2,
    paddingVertical: theme.SIZES.BASE,
  },
});

export default Promotions;
