import React from "react";
import { Dimensions, StyleSheet, View } from "react-native";
import { ScrollView } from "react-native-gesture-handler";
import { SafeAreaView } from "react-native-safe-area-context";
import { AppContext } from "../../AppContext";
import { BaseComponent } from "../../BaseComponent";
import { Block, Button, Text } from "../../components/custom/src";
import Dialog from "../../components/Dialog";
import { ScreensTypes } from "../../navigation/ScreensTypes";
import { AccountModel } from "../../services/models/AccountModel";
import FabMenu from "../FabMenu";
import Loading from "../Loading";

const { width, height } = Dimensions.get('screen');

export class Accounts extends BaseComponent {
    static contextType = AppContext;
    getInitialState() {
        return {
            isSaving: false,
            isUpdating: false,
            isLoadingComplete: false,
            errorResponse: undefined,
            accounts: [],
        }
    }
    state = this.getInitialState();
    componentDidMount(): void {
        const { accountService } = this.context.serviceProviders;
        const { navigation } = this.props;
        this.focusListener = navigation.addListener("focus", (payload) => {
            this.setState({ isLoadingComplete: false });
            accountService.getAll().subscribe((accounts) => this.setState({ accounts: accounts, isLoadingComplete: true }));
        });

    }

    render() {
        const { navigation } = this.props;
        return !this.state.isLoadingComplete ? (<Loading></Loading>) : (
            <View style={{ flex: 1, flexDirection: 'column' }}>
                <ScrollView showsVerticalScrollIndicator={true} style={{ flex: 1, minHeight: height * 0.3 }} >
                    {this.state.accounts.map((item, index) => {
                        return (
                            <Block key={index} style={styles.itemContainer}>
                                <Button style={{ ...styles.button }} onPress={() => { this.clickAccount(item) }}><Text style={{ ...styles.buttonText }}>{item.username}</Text><Text style={{ ...styles.buttonText, ...styles.amount }}>{item.amount} </Text></Button>
                            </Block>
                        );
                    })}
                </ScrollView>
                <FabMenu navigation={navigation} />
            </View >
        )
    }

    clickAccount(account: AccountModel): void {
        const { navigation } = this.props;
        console.log({ account });
        navigation.navigate(ScreensTypes.Account, { accountId: account.id })
    }
}

const styles = StyleSheet.create({
    scrollViewContentContainer: {
        flex: 1
    },
    itemContainer: {
        flex: 1,
        flexDirection: 'row',
        padding: 0,
    },
    buttonText: {
        color: 'black',
        flex: 1,
    },
    amount: {
        width: 70,
        flex: 0,
        textAlign: 'right'
    },
    button: {
        flex: 1,
        flexDirection: 'row',
        margin: 0,
        marginTop: 1,
        borderRadius: 0,
        backgroundColor: '#FFFFFF',
        paddingHorizontal: 20,
    },
});

export default Accounts;