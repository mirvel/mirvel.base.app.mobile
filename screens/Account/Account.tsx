import React from "react";
import { Dimensions, ScrollView, StyleSheet, View } from "react-native";
import { BaseComponent } from "../../BaseComponent";
import { Block, Text } from '../../components/custom/src';
import Dialog from "../../components/Dialog";
import IconHelper from "../../components/IconHelper";
import { argonTheme } from "../../constants";
import { Transaction } from "../../services/models/Transaction";
import FabMenu from "../FabMenu";
import Loading from "../Loading";


const { width, height } = Dimensions.get("screen");

class Account extends BaseComponent {
  getInitialState() {
    return {
      isLoadingComplete: false,
      errorResponse: undefined,
      transactions: [],
      account: { username: undefined, total_amount: undefined }
    }
  }

  state = this.getInitialState();

  componentDidMount() {
    const { navigation } = this.props;
    const { accountService } = this.context.serviceProviders;
    this.focusListener = navigation.addListener("focus", (payload) => {
      const { accountId } = this.props.route.params;
      this.setState({ isLoadingComplete: false })
      accountService.get(accountId).subscribe((account) => this.setState({ account: account }));
      accountService.transactions(accountId).subscribe((transactions) => this.setState({ transactions: transactions, isLoadingComplete: true }))
    });

  }

  render() {
    const { navigation } = this.props;
    return !this.state.isLoadingComplete ? (<Loading></Loading>) : (
      <Block flex>
        <Block center >
          <Text style={{ fontSize: 24, padding: 20 }}>{IconHelper.renderItem(IconHelper.icons.User, { color: 'black', fontSize: 16 })}  {this.state.account.username}</Text>
          <Text >Total:</Text><Text small> {this.state.account.total_amount}</Text>
        </Block>
        <ScrollView showsVerticalScrollIndicator={true}>
          <View style={styles.scrollViewContentContainer}>
            {this.state.transactions.map((item: Transaction, index) => {
              return (
                (<Block key={index} style={styles.itemContainer}>
                  <View style={{ ...styles.rowContainer, width: 80 }}>
                    <Text>{item.transaction_date_formatted}</Text>
                  </View>
                  <View style={{ ...styles.rowContainer, flex: 1 }}>
                    <Text>  {item.title}    </Text>
                  </View>
                  <View style={{ ...styles.rowContainer, alignItems: 'flex-end', width: 50 }}>
                    <Text>  {item.amount}   </Text>
                  </View>
                </Block>)
              );
            })}
          </View>
        </ScrollView>
        <FabMenu navigation={navigation} />
      </Block>
    );
  }

}

const styles = StyleSheet.create({
  registerContainer: {
    width: width * 0.9,
    height: height * 0.78,
    backgroundColor: "#F4F5F7",
    borderRadius: 4,
    shadowColor: argonTheme.COLORS.BLACK,
    shadowOffset: {
      width: 0,
      height: 4
    },
    shadowRadius: 8,
    shadowOpacity: 0.1,
    elevation: 1,
    overflow: "hidden"
  },
  scrollViewContentContainer: {
    display: 'flex',
    flexDirection: "column",
    flex: 1,
    margin: 20
  },
  itemContainer: {
    flex: 1,
    flexDirection: 'row',
    padding: 0,
  },
  rowContainer: {
    margin: 0.5,
    padding: 2,
  },

});

export default Account;
