import React from "react";
import { Dimensions, ScrollView, StyleSheet, View } from "react-native";
import RNVIcons from 'react-native-vector-icons/FontAwesome';
import { AppContext } from "../../AppContext";
import { BaseComponent } from "../../BaseComponent";
import { Block, Button, Input, Text } from "../../components/custom/src";
import Dialog from "../../components/Dialog";
import { argonTheme } from "../../constants";
import { AccountModel } from "../../services/models/AccountModel";

const { width, height } = Dimensions.get("screen");

class QuickSearchAccounts extends BaseComponent {
    static contextType = AppContext;
    getInitialState() {
        return {
            term: '',
            accounts: [],
            filtered: [],
            selectedIds: [],
            isSaving: false,
            isLoadingComplete: false,
        }
    }
    state = this.getInitialState();

    componentDidMount() {
        const { navigation } = this.props;
        const { accountService } = this.context.serviceProviders;
        this.focusListener = navigation.addListener("focus", async (payload) => {
            this.setState(this.getInitialState(), () => {
                const { route } = this.props;
                const routeParams = route.params || { selectedIds: [] };
                accountService.getAll().subscribe((accounts) => this.setState({ accounts: accounts, isLoadingComplete: true, selectedIds: routeParams.selectedIds, filtered: this.mapSelections(accounts, routeParams.selectedIds) }));
            });
        });
    }

    select() {
        const { navigation, route } = this.props;
        if (route.params && route.params.selectionCompleteScreen)
            navigation.navigate(route.params.selectionCompleteScreen, {
                selectedIds: this.state.selectedIds,
                notInitial: true
            })
        else
            navigation.goBack();
    }

    cancel() {
        this.props.navigation.goBack();
    }

    onSearch(term: string = '') {
        let filtered = [];
        if (term && term.length > 0) {
            filtered = this.state.accounts.filter((item) => item.username && item.username.toLowerCase().indexOf(term.toLowerCase()) >= 0);
        }
        else {
            filtered = this.state.accounts;
        }

        this.setState({ filtered: this.mapSelections(filtered, this.state.selectedIds), term: term });
    }

    clickAccount(account: AccountModel) {
        const itemIndex = this.state.selectedIds.findIndex((itemId) => itemId === account.id);
        const newSelectedIds = [...this.state.selectedIds];
        if (itemIndex < 0)
            newSelectedIds.push(account.id);
        else
            newSelectedIds.splice(itemIndex, 1);

        const filtered = this.mapSelections(this.state.filtered, newSelectedIds);
        this.setState({ selectedIds: newSelectedIds, filtered: filtered });
    }

    mapSelections(list: AccountModel[], selectedIds: number[]) {
        return list.map((item) => {
            const newItem = { ...item, selected: selectedIds && selectedIds.indexOf(item.id) >= 0 };
            return newItem;
        });
    }

    render() {
        const { languageService } = this.context.serviceProviders;

        return (<Dialog>
            <View style={styles.container}>
                <Text style={{ fontSize: 24, padding: 20 }}><RNVIcons name="book" style={{ fontSize: 24 }} />  {languageService.tags.SearchAccount}</Text>
                <Block width={width * 0.8} style={styles.inputContainer}>
                    <Input
                        borderless
                        color='black'
                        textInputStyle={{ marginLeft: 10 }}
                        placeholder={languageService.tags.Search}
                        value={this.state.term}
                        required={true}
                        onChangeText={value => this.onSearch(value)}
                        iconContent={<RNVIcons name="info" size={16} color={argonTheme.COLORS.ICON} />}
                    />
                </Block>

                <ScrollView showsVerticalScrollIndicator={true} style={{ flex: 1, minHeight: height * 0.3 }} >
                    {this.state.filtered.map((item, index) => {
                        return (
                            <Block key={index} style={styles.itemContainer}>
                                <Button style={{ ...styles.button }} onPress={() => { this.clickAccount(item) }}><Text style={{ ...styles.buttonText }}>{item.username}</Text><Text style={{ ...styles.buttonText, ...styles.amount }}><RNVIcons name="check-circle" size={16} color={item.selected ? argonTheme.COLORS.ICON : argonTheme.COLORS.MUTED} /></Text></Button>
                            </Block>
                        );
                    })}
                </ScrollView>
                <Block middle style={styles.buttonContainer}>
                    <Button color={argonTheme.COLORS.PRIMARY} style={styles.actionButton}
                        onPress={e => { this.select(); }}
                    >
                        <Text bold size={14} color={argonTheme.COLORS.WHITE}>
                            {languageService.tags.Select}
                        </Text>
                    </Button>
                    <Button color={argonTheme.COLORS.PRIMARY} style={styles.actionButton}
                        onPress={e => { this.cancel(); }}
                    >
                        <Text bold size={14} color={argonTheme.COLORS.WHITE}>
                            {languageService.tags.Cancel}
                        </Text>
                    </Button>
                </Block>
            </View>

        </Dialog>)
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
    },
    inputIcons: {
        marginRight: 12
    },
    inputContainer: {
        margin: 1,
    },

    actionButton: {
        width: width * 0.3,
        marginTop: 25,
        marginHorizontal: 5
    },
    buttonContainer: {
        marginTop: 'auto',
        flexDirection: 'row'
    },
    // scrollViewContentContainer: {
    //     flex: 1
    // },
    itemContainer: {
        flex: 1,
        flexDirection: 'row',
        padding: 0,
    },
    buttonText: {
        color: 'black',
        flex: 1,
    },
    amount: {
        width: 70,
        flex: 0,
        textAlign: 'right'
    },
    button: {
        flex: 1,
        flexDirection: 'row',
        margin: 0,
        marginTop: 1,
        borderRadius: 0,
        backgroundColor: '#FFFFFF',
        paddingHorizontal: 20,
    },
});

export default QuickSearchAccounts;