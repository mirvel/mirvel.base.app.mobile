import React from "react";
import { Dimensions, StyleSheet, View } from "react-native";
import RNVIcons from 'react-native-vector-icons/FontAwesome';
import { AppContext } from "../../AppContext";
import { BaseComponent } from "../../BaseComponent";
import { Block, Button, Input, Text } from "../../components/custom/src";
import Dialog from "../../components/Dialog";
import { argonTheme } from "../../constants";

const { width, height } = Dimensions.get("screen");

class CreateAccount extends BaseComponent {
    static contextType = AppContext;
    getInitialState() {
        return {
            title: '',
            isSaving: false,
            isLoadingComplete: false,
        }
    }
    state = this.getInitialState();

    submit() {
        const { accountService } = this.context.serviceProviders;
        accountService.create({ username: this.state.title }).subscribe((teamMembership) => {
            this.props.navigation.goBack();
        });
    }

    cancel() {
        this.props.navigation.goBack();
    }

    render() {
        const { languageService } = this.context.serviceProviders;

        return (<Dialog>
            <View style={styles.container}>
                <Text style={{ fontSize: 24, padding: 20 }}><RNVIcons name="user-plus" style={{ fontSize: 24 }} />  {languageService.tags.CreateAccount}</Text>
                <Block width={width * 0.8} style={styles.inputContainer}>
                    <Input
                        borderless
                        color='black'
                        textInputStyle={{ marginLeft: 10 }}
                        placeholder={languageService.tags.Title}
                        value={this.state.title}
                        required={true}
                        onChangeText={value => this.setState({ title: value })}
                        iconContent={<RNVIcons name="user" size={16} color={argonTheme.COLORS.ICON} />}
                    />

                </Block>
                <Block middle style={styles.buttonContainer}>
                    <Button color={argonTheme.COLORS.PRIMARY} style={styles.actionButton}
                        onPress={e => { this.submit(); }}
                    >
                        <Text bold size={14} color={argonTheme.COLORS.WHITE}>
                            {languageService.tags.Submit}
                        </Text>
                    </Button>
                    <Button color={argonTheme.COLORS.PRIMARY} style={styles.actionButton}
                        onPress={e => { this.cancel(); }}
                    >
                        <Text bold size={14} color={argonTheme.COLORS.WHITE}>
                            {languageService.tags.Cancel}
                        </Text>
                    </Button>
                </Block>
            </View>

        </Dialog>)
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
    },
    inputIcons: {
        marginRight: 12
    },
    inputContainer: {
        margin: 1,
    },

    actionButton: {
        width: width * 0.3,
        marginTop: 25,
        marginHorizontal: 5
    },
    buttonContainer: {
        marginTop: 'auto',
        flexDirection: 'row'
    }
});

export default CreateAccount;