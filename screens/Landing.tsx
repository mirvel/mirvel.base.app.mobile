import React from "react";
import { Dimensions, Image, StyleSheet } from "react-native";
import { BaseComponent } from "../BaseComponent";
import { Block } from '../components/custom/src';
import FabMenu from "./FabMenu";
const image = require('../assets/splash.png')


const { width, height } = Dimensions.get("screen");

class Landing extends BaseComponent {

    render() {
        const { navigation } = this.props;
        return (
            <Block flex middle>
                <Image source={image} style={{ height: '100%', width: '100%' }} />
                <FabMenu navigation={navigation} />
            </Block>
        );
    }
}

const styles = StyleSheet.create({
    label: {
        marginTop: 8,
        fontSize: 14,
        fontWeight: 'bold'
    }
});

export default Landing;
