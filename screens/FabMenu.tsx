import React from 'react';
import { Dimensions, StyleSheet, Text } from 'react-native';
import ActionButton from "react-native-action-button";
import RNVIcons from 'react-native-vector-icons/FontAwesome';
import { AppContext } from '../AppContext';
import { BaseComponent } from "../BaseComponent";
import { theme } from '../components/custom/src';
import { argonTheme } from '../constants';
import { ScreensTypes } from "../navigation/ScreensTypes";

const { width } = Dimensions.get('window');

export class FabMenu extends BaseComponent {
    static contextType = AppContext;
    render() {
        const { languageService } = this.context.serviceProviders;
        const { navigation } = this.props;
        return (
            <ActionButton buttonColor={argonTheme.COLORS.PRIMARY}>
                <ActionButton.Item buttonColor={argonTheme.COLORS.SECONDARY} title={languageService.tags.CreateTask} onPress={() => navigation.navigate(ScreensTypes.CreateTask)}>
                    <Text><RNVIcons name="list-alt" style={styles.actionButtonIcon} /></Text>
                </ActionButton.Item>
                <ActionButton.Item buttonColor={argonTheme.COLORS.SECONDARY} title={languageService.tags.CreateTransaction} onPress={() => navigation.navigate(ScreensTypes.CreateTransaction)}>
                    <Text><RNVIcons name="list-alt" style={styles.actionButtonIcon} /></Text>
                </ActionButton.Item>
                <ActionButton.Item buttonColor={argonTheme.COLORS.SECONDARY} title={languageService.tags.CreateAccount} onPress={() => navigation.navigate(ScreensTypes.CreateAccount)}>
                    <Text><RNVIcons name="user-plus" style={styles.actionButtonIcon} /></Text>
                </ActionButton.Item>
            </ActionButton>
        )
    }
}

const styles = StyleSheet.create({
    home: {
        width: width,
    },
    articles: {
        width: width - theme.SIZES.BASE * 2,
        paddingVertical: theme.SIZES.BASE,
    },
    actionButtonIcon: {
        fontSize: 20,
        height: 22,
        color: '#003333',
    },
});

export default FabMenu;