import React from "react";
import { Dimensions, StyleSheet, View } from "react-native";
import { ScrollView } from "react-native-gesture-handler";
import { AppContext } from "../../AppContext";
import { BaseComponent } from "../../BaseComponent";
import { Block, Button, Text } from "../../components/custom/src";
import { ScreensTypes } from "../../navigation/ScreensTypes";
import { Project } from "../../services/models/Project";
import FabMenu from "../FabMenu";
import Loading from "../Loading";

const { width, height } = Dimensions.get('screen');

export class Projects extends BaseComponent {
    static contextType = AppContext;
    getInitialState() {
        return {
            isSaving: false,
            isUpdating: false,
            isLoadingComplete: false,
            errorResponse: undefined,
            list: [],
        }
    }
    state = this.getInitialState();
    componentDidMount(): void {
        const { projectService } = this.context.serviceProviders;
        const { navigation } = this.props;
        this.focusListener = navigation.addListener("focus", (payload) => {
            this.setState({ isLoadingComplete: false });
            projectService.getAll().subscribe((list) => this.setState({ list: list, isLoadingComplete: true }));
        });

    }

    render() {
        const { navigation } = this.props;
        return !this.state.isLoadingComplete ? (<Loading></Loading>) : (
            <View style={{ flex: 1, flexDirection: 'column' }}>
                <ScrollView showsVerticalScrollIndicator={true} style={{ flex: 1, minHeight: height * 0.3 }} >
                    {this.state.list.map((item: Project, index) => {
                        return (
                            <Block key={index} style={styles.itemContainer}>
                                <Button style={{ ...styles.button }} onPress={() => { this.click(item) }}><Text style={{ ...styles.buttonText }}>{item.title}</Text><Text style={{ ...styles.buttonText, ...styles.amount }}>0</Text></Button>
                            </Block>
                        );
                    })}
                </ScrollView>
                <FabMenu navigation={navigation} />
            </View >
        )
    }

    click(item: Project): void {
        const { navigation } = this.props;
        navigation.navigate(ScreensTypes.CreateProject, { id: item.id })
    }
}

const styles = StyleSheet.create({
    scrollViewContentContainer: {
        flex: 1
    },
    itemContainer: {
        flex: 1,
        flexDirection: 'row',
        padding: 0,
    },
    buttonText: {
        color: 'black',
        flex: 1,
    },
    amount: {
        width: 70,
        flex: 0,
        textAlign: 'right'
    },
    button: {
        flex: 1,
        flexDirection: 'row',
        margin: 0,
        marginTop: 1,
        borderRadius: 0,
        backgroundColor: '#FFFFFF',
        paddingHorizontal: 20,
    },
});

export default Projects;