import React from "react";
import { Dimensions, StyleSheet, View } from "react-native";
import RNVIcons from 'react-native-vector-icons/FontAwesome';
import { AppContext } from "../../AppContext";
import { BaseComponent } from "../../BaseComponent";
import { Block, Button, Input, Text } from "../../components/custom/src";
import Dialog from "../../components/Dialog";
import { argonTheme } from "../../constants";
import { ScreensTypes } from "../../navigation/ScreensTypes";
import { Project } from "../../services/models/Project";

const { width, height } = Dimensions.get("screen");

class CreateProject extends BaseComponent {
    static contextType = AppContext;
    getInitialState() {
        return {
            editMode: false,
            title: '',
            isSaving: false,
            isLoadingComplete: false,
            participantIds: [],
            id: undefined
        }
    }
    state = this.getInitialState();

    componentDidMount() {
        const { navigation } = this.props;
        const { projectService } = this.context.serviceProviders;
        this.focusListener = navigation.addListener("focus", async (payload) => {
            this.setState({ isLoadingComplete: false });
            const { route } = this.props;
            const routeParams = route.params || {};
            const currentId = routeParams.id;
            let newParticipants: number[] = [];

            if (routeParams.selectedIds && routeParams.selectedIds !== this.state.participantIds)
                newParticipants = routeParams.selectedIds;
            if (routeParams.notInitial)
                if (currentId) {
                    projectService.get(currentId).subscribe((project: Project) => {
                        this.setState({ ...project, participantIds: newParticipants, isLoadingComplete: true });
                    });
                }
                else {
                    this.setState({ participantIds: newParticipants, isLoadingComplete: true });
                }

            else {
                if (currentId) {
                    projectService.get(currentId).subscribe((project: Project) => {
                        this.setState({ ...this.getInitialState(), ...project, editMode: !!currentId, participantIds: project.participants, isLoadingComplete: true });
                    });
                }
                else {
                    this.setState({ ...this.getInitialState(), isLoadingComplete: true });
                }
            }



            navigation.setParams({ notInitial: false, selectedIds: undefined, id: undefined });
        });
    }

    componentDidUpdate() {

    }

    submit() {
        const { projectService } = this.context.serviceProviders;
        const data = { title: this.state.title, participants: this.state.participantIds };
        if (this.state.id)
            projectService.update(this.state.id, data).subscribe(() => {
                this.props.navigation.navigate(ScreensTypes.Projects);
            });
        else
            projectService.create(data).subscribe(() => {
                this.props.navigation.navigate(ScreensTypes.Projects);
            });
    }

    cancel() {
        this.props.navigation.goBack();
    }

    participants() {
        this.props.navigation.navigate(ScreensTypes.QuickSearchAccounts, {
            selectionCompleteScreen: ScreensTypes.CreateProject,
            selectedIds: this.state.participantIds
        })
    }

    render() {
        const { languageService } = this.context.serviceProviders;

        return (<Dialog>
            <View style={styles.container}>
                <Text style={{ fontSize: 24, padding: 20 }}><RNVIcons name="book" style={{ fontSize: 24 }} />  {this.state.editMode ? languageService.tags.EditProject : languageService.tags.CreateProject}</Text>
                <Block width={width * 0.8} style={styles.inputContainer}>
                    <Input
                        borderless
                        color='black'
                        textInputStyle={{ marginLeft: 10 }}
                        placeholder={languageService.tags.Title}
                        value={this.state.title}
                        required={true}
                        onChangeText={value => this.setState({ title: value })}
                        iconContent={<RNVIcons name="info" size={16} color={argonTheme.COLORS.ICON} />}
                    />
                    <Button color={argonTheme.COLORS.WHITE}
                        onPress={e => { this.participants(); }}
                    >
                        <Text bold size={14} color={argonTheme.COLORS.BLACK}>
                            {languageService.tags.Participants}:{this.state.participantIds.length}
                        </Text>
                    </Button>

                </Block>
                <Block middle style={styles.buttonContainer}>
                    <Button color={argonTheme.COLORS.PRIMARY} style={styles.actionButton}
                        onPress={e => { this.submit(); }}
                    >
                        <Text bold size={14} color={argonTheme.COLORS.WHITE}>
                            {languageService.tags.Submit}
                        </Text>
                    </Button>
                    <Button color={argonTheme.COLORS.PRIMARY} style={styles.actionButton}
                        onPress={e => { this.cancel(); }}
                    >
                        <Text bold size={14} color={argonTheme.COLORS.WHITE}>
                            {languageService.tags.Cancel}
                        </Text>
                    </Button>
                </Block>
            </View>

        </Dialog>)
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
    },
    inputIcons: {
        marginRight: 12
    },
    inputContainer: {
        margin: 1,
    },

    actionButton: {
        width: width * 0.3,
        marginTop: 25,
        marginHorizontal: 5
    },
    buttonContainer: {
        marginTop: 'auto',
        flexDirection: 'row'
    }
});

export default CreateProject;