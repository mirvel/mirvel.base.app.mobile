import Constants from "expo-constants";
import React from "react";
import {
  Dimensions, StyleSheet
} from "react-native";
import RNVIcons from 'react-native-vector-icons/FontAwesome';
import { BaseComponent } from "../BaseComponent";
import { Text } from '../components/custom/src';
import Dialog from "../components/Dialog";
import IconHelper from "../components/IconHelper";
import { argonTheme } from "../constants";
import { ScreensTypes } from "../navigation/ScreensTypes";


const { width, height } = Dimensions.get("screen");

class Login extends BaseComponent {
  getInitialState() {
    return {
      isLoading: false,
      password: '',
      username: '',
      errorResponse: undefined
    }
  }

  state = this.getInitialState();

  render() {
    const { languageService } = this.context.serviceProviders;
    return (
      <Dialog>
        <Text style={{ fontSize: 24, padding: 20 }}><RNVIcons name={IconHelper.getIconNameByScreenName(ScreensTypes.Info)} style={{ fontSize: 24 }} />  {languageService.tags.About}</Text>
        <Text style={styles.label}>Manifest Name:</Text><Text small> {Constants.manifest.name}</Text>
        <Text style={styles.label}>Manifest Version:</Text><Text small> {Constants.manifest.version}</Text>
        <Text style={styles.label}>Expo Version:</Text><Text small> {Constants.expoVersion}</Text>
        <Text style={styles.label}>App Version:</Text><Text small> {Constants.nativeAppVersion}</Text>
        <Text style={styles.label}>Build Version:</Text><Text small> {Constants.nativeBuildVersion}</Text>
      </Dialog>
    );
  }

  login(e) {
    const { navigation } = this.props;
    const { authService } = this.context.serviceProviders;
    this.setState({ isLoading: true });
    const subscription = authService.login(this.state).subscribe(r => {
      this.setState(this.getInitialState());
      subscription.unsubscribe();
      navigation.navigate('Home');

    }, errorResponse => {
      this.setState({ errorResponse: errorResponse })
    });

  }
}

const styles = StyleSheet.create({
  registerContainer: {
    width: width * 0.9,
    height: height * 0.78,
    backgroundColor: "#F4F5F7",
    borderRadius: 4,
    shadowColor: argonTheme.COLORS.BLACK,
    shadowOffset: {
      width: 0,
      height: 4
    },
    shadowRadius: 8,
    shadowOpacity: 0.1,
    elevation: 1,
    overflow: "hidden"
  },
  inputIcons: {
    marginRight: 12
  },
  createButton: {
    width: width * 0.5,
    marginTop: 25
  },
  label: {
    marginTop: 8,
    fontSize: 14,
    fontWeight: 'bold'
  }
});

export default Login;
