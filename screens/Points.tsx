import React from 'react';
import { Dimensions, ScrollView, StyleSheet, View } from 'react-native';
import { merge } from 'rxjs';
import { AppContext } from '../AppContext';
import { BaseComponent } from "../BaseComponent";
import { Block, Text, theme } from '../components/custom/src';
import { Transaction } from '../services/models/Transaction';
import { FabMenu } from './FabMenu';
import Loading from './Loading';


const { width } = Dimensions.get('screen');

class Points extends BaseComponent {
  static contextType = AppContext;

  state = { transactions: [], isLoadingComplete: false }
  subscriptions$ = [];

  renderPage = () => {
    const { navigation } = this.props;
    const { dateTimeService } = this.context.serviceProviders;
    return !this.state.isLoadingComplete ? (<Loading></Loading>) : (
      <View style={{ flex: 1, backgroundColor: '#f3f3f3' }}>
        <ScrollView showsVerticalScrollIndicator={true}>
          <View style={styles.scrollViewContentContainer}>
            {this.state.transactions.map((item: Transaction, index) => {
              return (
                (<Block key={index} style={styles.itemContainer}>
                  <View style={{ ...styles.rowContainer, width: 85 }}>
                    <Text>{item.transaction_date_formatted}</Text>
                  </View>
                  <View style={{ ...styles.rowContainer, flex: 1 }}>
                    <Text>  {item.title}    </Text>
                  </View>
                  <View style={{ ...styles.rowContainer, alignItems: 'flex-end', width: 50 }}>
                    <Text>  {item.amount}   </Text>
                  </View>
                </Block>)
              );
            })}
          </View>
        </ScrollView>
        <FabMenu navigation={navigation} />
      </View>
    )
  }



  getLastTransactions() {
    const { transactionService } = this.context.serviceProviders;
    this.setState({ isLoadingComplete: false });
    return transactionService.getAll().subscribe((results) => {
      this.setState({
        isLoadingComplete: true,
        transactions: results
      });
    },
      e => {
        console.log(e);
      });
  }

  componentDidMount() {
    const { authService, transactionService, taskService } = this.context.serviceProviders;
    const { navigation } = this.props;
    this.focusListener = navigation.addListener("focus", (payload) => {
      this.subscriptions$.push(this.getLastTransactions());
    });

    const updateEvents = merge(transactionService.updated, authService.signinComplete);

    // Whenever there is a new transaction update the list
    this.subscriptions$.push(updateEvents.subscribe(() => {
      this.getLastTransactions();
    },
      e => {
        console.log(e);
      }));
  }

  componentWillUnmount() {
    this.subscriptions$.forEach(subscription => subscription.unsubscribe());
  }
  render() {
    return (
      <Block flex center style={styles.home}>
        {this.renderPage()}
      </Block>
    );
  }
}

const styles = StyleSheet.create({
  home: {
    width: width,
  },
  articles: {
    width: width - theme.SIZES.BASE * 2,
    paddingVertical: theme.SIZES.BASE,
  },
  scrollViewContentContainer: {
    display: 'flex',
    flexDirection: "column",
    flex: 1,
    width: width * 0.8,
    marginTop: 20
  },
  itemContainer: {
    flex: 1,
    flexDirection: 'row',
    padding: 0,
  },
  rowContainer: {
    margin: 0.5,
    padding: 2,
  },
});

export default Points;
