import React, { useState, useEffect } from 'react';
import {
  StyleSheet,
  Dimensions,
} from "react-native";
import { Block, Text } from '../components/custom/src';
import { Button, Icon } from "../components";
import * as queryString from 'query-string';
import { AppContext } from '../AppContext';
import { BaseComponent } from "../BaseComponent";
import RNVIcons from 'react-native-vector-icons/FontAwesome';

const { width, height } = Dimensions.get("screen");

interface Props {
  navigation: any,
}

class AquirePoints extends BaseComponent {
  focusListener;
  getInitialState() {
    return {
      isLoading: false,
      title: '',
      description: '',
      scanned: false,
      data: undefined,
      error: undefined,
    }
  }
  state = this.getInitialState();

  componentDidMount() {
    const { navigation } = this.props;
    this.focusListener = navigation.addListener("didFocus", () => {
      try {
        const { qrdata } = this.props.navigation.state.params;
        const parsed = queryString.parseUrl(qrdata);
        if (parsed.query) {
          const { s: amount, d: description } = parsed.query;

          const data = { amount, dump: parsed.query, description: description };
          console.log(data);
          this.setState({ data: data });
        }
        else {
          throw 'Failed to parse QR data or link';
        }


      } catch (error) {
        this.setState({ error: `Error: ${error}` });
      }
    });

  }

  componentWillUnmount() {
    // Remove the event listener
    this.focusListener.remove();
  }


  submit() {
    const { navigation } = this.props;
    const { transactionService, authService } = this.context.serviceProviders;
    transactionService.create({
      points: this.state.data.amount,
      comment: JSON.stringify(this.state.data.dump),
      transaction_type: 1,
      recipient: authService.currentUser.value.id
    }).subscribe((response) => {
      console.log(response);
      navigation.navigate('Points');
    })

  }
  scan() {
    const { navigation } = this.props;
    navigation.navigate('QRReader');
  }
  render() {
    const { authService, languageService } = this.context.serviceProviders;
    const { navigation } = this.props;
    const iconBlockStyle = { height: 80, flex: 1 };
    const iconStyle = { fontSize: 36, color: 'orange' };

    return (
      <Block flex style={{ flexDirection: 'column' }}>
        <Block flex style={{ flexDirection: 'row', flex: 0, }}>
          <Block style={iconBlockStyle}>
            <Icon name="md-local_grocery_store" style={iconStyle} />
          </Block>
          <Block style={iconBlockStyle}>
            <Icon name="md-person" style={iconStyle} />
          </Block>
        </Block>
        <Block flex style={{ flexDirection: 'column', flex: 1, alignItems: 'center' }}>
          <Text style={{ fontSize: 24, padding: 20 }}><RNVIcons name="diamond" style={{ fontSize: 24 }} />  {languageService.tags.AquirePointsTitle}</Text>
          <Block>
            <Text style={{ fontSize: 36 }}>{this.state.data ? this.state.data.amount : ''}</Text>
          </Block>
          <Block>
            <Text style={{ color: 'red' }}>{this.state.error}</Text>
          </Block>
          <Block flex style={{ flexDirection: 'column', flex: 1, padding: 10 }}>
            <Block flex style={{ borderWidth: 3, borderColor: 'white', flex: 1, margin: 80, borderStyle: 'dotted', ...StyleSheet.absoluteFillObject }}>
            </Block>
          </Block>
          <Block>
            <Text>{!authService.currentUser.value ? 'Please sign-in to submit points' : ''}</Text>
          </Block>
          <Block flex style={{ alignItems: 'center', padding: 10, flexDirection: 'row', flex: 0 }}>
            <Button onPress={e => { this.scan(); }} style={{ margin: 2, flex: 1 }}>
              <Text>{languageService.tags.Scan}</Text>
            </Button>
            <Button onPress={e => { this.submit(); }} style={!authService.currentUser.value ? { margin: 2, flex: 1, opacity: 0.5 } : { margin: 2, flex: 1, opacity: 1 }} disabled={!authService.currentUser.value}>
              <Text>{languageService.tags.Submit}</Text>
            </Button>
          </Block>
        </Block>
      </Block>
    );
  }
}

const styles = StyleSheet.create({
  inputIcons: {
    marginRight: 12
  },
  footerButtons: {
    margin: 2,
    flex: 1,
  }
});

export default AquirePoints;
