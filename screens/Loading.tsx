import React from "react";
import { View, Text } from "react-native";
import { BaseComponent } from "../BaseComponent";

class Loading extends BaseComponent {
    render() {
        return (<View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}><Text>Loading</Text></View>);
    }
}

export default Loading;