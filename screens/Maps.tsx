import React from 'react';
import { StyleSheet, Dimensions, View } from 'react-native';
import { Block, theme } from '../components/custom/src';

import MapView from 'react-native-maps';
import MapViewDirections from 'react-native-maps-directions';
import { BaseComponent } from '../BaseComponent';


const { width, height } = Dimensions.get('screen');

class Maps extends BaseComponent {
  subscriptions$ = [];
  state = { calls: [], isLoading: false }


  renderMap = () => {
    const { navigation } = this.props;
    const ASPECT_RATIO = width / height;
    const LATITUDE_DELTA = 0.0922;
    const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

    const initialRegion = { latitude: 42.8628063, longitude: 74.6097822, longitudeDelta: LONGITUDE_DELTA, latitudeDelta: LATITUDE_DELTA };
    const origin = { latitude: initialRegion.latitude, longitude: initialRegion.longitude };
    const destination = { latitude: 45.4133763, longitude: -73.9859624 };
    const GOOGLE_MAPS_APIKEY = 'AIzaSyBxb2oSyXjhMmMODTrp2Xm6-sLmBBbqE9M'; //mirvelsolutions@gmail.com API key
    return (
      <View style={{ flex: 1, backgroundColor: '#f3f3f3' }}>
        <MapView style={styles.mapStyle} initialRegion={initialRegion} >
          <MapViewDirections
            origin='Льва%20Толстого%202-а%2C%2061%2C%20Бишкек'
            destination='8%20Волокаламская%20улица%2C%20Bishkek%2C%20Kyrgyzstan'
            apikey={GOOGLE_MAPS_APIKEY}
          />
        </MapView>
      </View>
    )
  }



  componentWillUnmount() {
    this.subscriptions$.forEach(subscription => subscription.unsubscribe());
  }
  render() {
    return (
      <Block flex center style={styles.home}>
        {this.renderMap()}
      </Block>
    );
  }
}

const styles = StyleSheet.create({
  home: {
    width: width,
  },
  mapStyle: {
    width: width,
    height: height,
  },
});

export default Maps;
