import React from "react";
import {
  Dimensions,

  KeyboardAvoidingView, StyleSheet
} from "react-native";
import RNVIcons from 'react-native-vector-icons/FontAwesome';
import { BaseComponent } from "../BaseComponent";
import { Button, Icon, Input } from "../components";
import { Block, Text } from '../components/custom/src';
import Dialog from "../components/Dialog";
import { ErrorMessage } from '../components/ErrorMessage';
import { argonTheme } from "../constants";


const { width, height } = Dimensions.get("screen");

class Login extends BaseComponent {
  getInitialState() {
    return {
      isLoading: false,
      password: '',
      username: '',
      errorResponse: undefined
    }
  }

  state = this.getInitialState();

  render() {
    const { languageService } = this.context.serviceProviders;
    return (
      <Dialog>
        <Text style={{ fontSize: 24, padding: 20 }}><RNVIcons name="sign-in" style={{ fontSize: 24 }} />  {languageService.tags.SignIn}</Text>
        <KeyboardAvoidingView
          style={{ flex: 1 }}
          behavior="padding"
          enabled
        >
          <Block width={width * 0.8} style={{ marginBottom: 15 }}>
            <Input
              borderless
              placeholder={languageService.tags.Username}
              value={this.state.username}
              onChangeText={username => this.setState({ username: username })}
              iconContent={
                <Text style={{ paddingRight: 8 }}><RNVIcons name="user" size={16} color={argonTheme.COLORS.ICON} /></Text>
              }
            />
          </Block>
          <Block width={width * 0.8}>
            <Input
              password
              borderless
              placeholder={languageService.tags.Password}
              value={this.state.password}
              onChangeText={password => this.setState({ password: password })}
              iconContent={
                <Icon
                  size={16}
                  color={argonTheme.COLORS.ICON}
                  name="padlock-unlocked"
                  family="ArgonExtra"
                  style={styles.inputIcons}
                />
              }
            />
          </Block>
          <Block middle>
            <Button color="primary" style={styles.createButton}
              onPress={e => { this.login(e); }}>
              <Text bold size={14} color={argonTheme.COLORS.WHITE}>
                {languageService.tags.SignIn}
              </Text>
            </Button>
          </Block>
          <Block style={{ marginTop: 10 }}>
            <ErrorMessage errorResponse={this.state.errorResponse} />
          </Block>
        </KeyboardAvoidingView>
      </Dialog>
    );
  }

  login(e) {
    const { navigation } = this.props;
    const { authService } = this.context.serviceProviders;
    this.setState({ isLoading: true });
    const subscription = authService.login(this.state).subscribe(r => {
      this.setState(this.getInitialState());
      subscription.unsubscribe();
      navigation.navigate('Home');

    }, errorResponse => {
      this.setState({ errorResponse: errorResponse })
    });

  }
}

const styles = StyleSheet.create({
  registerContainer: {
    width: width * 0.9,
    height: height * 0.78,
    backgroundColor: "#F4F5F7",
    borderRadius: 4,
    shadowColor: argonTheme.COLORS.BLACK,
    shadowOffset: {
      width: 0,
      height: 4
    },
    shadowRadius: 8,
    shadowOpacity: 0.1,
    elevation: 1,
    overflow: "hidden"
  },
  inputIcons: {
    marginRight: 12
  },
  createButton: {
    width: width * 0.5,
    marginTop: 25
  }
});

export default Login;
