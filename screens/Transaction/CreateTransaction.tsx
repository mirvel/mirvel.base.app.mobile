import { Picker } from '@react-native-picker/picker';
import React from 'react';
import { DatePickerAndroid, Dimensions, StyleSheet, TouchableOpacity, View } from 'react-native';
import RNVIcons from 'react-native-vector-icons/FontAwesome';
import { AppContext } from '../../AppContext';
import { BaseComponent } from "../../BaseComponent";
import { Block, Button, Input, Text } from '../../components/custom/src';
import Dialog from '../../components/Dialog';
import ErrorMessage from '../../components/ErrorMessage';
import IconHelper from '../../components/IconHelper';
import { argonTheme } from '../../constants';
import { Project } from '../../services/models/Project';
import { Transaction } from '../../services/models/Transaction';


const { width, height } = Dimensions.get("screen");

class CreateTransaction extends BaseComponent {
  static contextType = AppContext;
  subscriptions$ = [];

  getInitialState() {
    return {
      title: '',
      date: new Date(),
      amount: 0,
      description: '',
      recipient: undefined,
      members: [],
      errorResponse: undefined,
      projects: [],
      project: undefined
    }
  }
  state = this.getInitialState();
  resetState() {
    return new Promise<void>((resolve) => {
      this.state = undefined;
      this.setState(this.getInitialState(), () => {
        resolve();
      });
    })

  }

  componentDidMount() {
    const { navigation } = this.props;
    this.focusListener = navigation.addListener("focus", async (payload) => {
      const { userService } = this.context.serviceProviders;
      await this.resetState();
      userService.getAllFiltered().toPromise().then(
        (members) => {
          this.setState({ members });
        }
      );

    });
  }

  static propTypes: any;
  submit() {
    const { transactionService } = this.context.serviceProviders;
    const { navigation } = this.props;
    const transaction = new Transaction();
    transaction.recipient = this.state.recipient;
    transaction.amount = this.state.amount;
    transaction.source_FK = 0;
    transaction.source_Type = 'Manual';
    transaction.title = 'Awards';
    transaction.date = this.state.date;
    transaction.description = this.state.description;
    transaction.title = this.state.title;
    transaction.project = this.state.project;
    transactionService.create(transaction).subscribe((createTransactionResponse) => {
      this.resetState(); // It's ok not to wait for callback
      navigation.goBack();
    }, errorResponse => {
      this.setState({ errorResponse: errorResponse })
    });
  }

  async showDateTimePicker() {
    const { action, year, month, day } = await DatePickerAndroid.open({
      date: this.state.date ? new Date(this.state.date) : new Date(),
    }) as any;
    console.log('showdatetimepicker')
    if (action !== DatePickerAndroid.dismissedAction) {
      const selectedDate = new Date(year, month, day)
      console.log(selectedDate);
      this.setState({ date: selectedDate })
    }
  }

  onRecipientChange(itemValue) {
    console.log({ itemValue });
    this.setState({ recipient: itemValue });
    const { projectService } = this.context.serviceProviders;
    projectService.getAllByParticipant(itemValue).toPromise().then((projects: Project[]) => {
      this.setState({ projects })
    })
  }

  render() {
    const { languageService } = this.context.serviceProviders;
    return (
      <Dialog style={styles.dialog}>

        <View style={styles.container} >
          <View style={styles.headerContainer}>
            <Text style={{ fontSize: 24, padding: 20 }}>{IconHelper.renderItem(IconHelper.icons.Info, { color: 'black' })}  Transaction</Text>
          </View>
          <View style={styles.bodyContainer}>
            <TouchableOpacity onPress={_ => this.showDateTimePicker()}>
              <Block style={styles.dateTimePicker}>
                <Input
                  borderless
                  placeholder={languageService.tags.Date}
                  value={this.state.date ? new Date(this.state.date).toLocaleDateString(languageService.language) : ''}
                  editable={false}
                  iconContent={
                    <Text style={{ ...styles.inputIcons, paddingRight: 5 }}><RNVIcons name="calendar" /></Text>
                  }
                />
              </Block>
            </TouchableOpacity>
            <Picker
              style={styles.recipient}
              selectedValue={this.state.recipient}
              onValueChange={(itemValue, itemIndex) => this.onRecipientChange(itemValue)}
            >
              <Picker.Item label="Recipient" key="picker-default" value={undefined} />
              {this.state.members.map((member, index) => {
                return (
                  (<Picker.Item label={member.username} key={`${member.id}`} value={member.id} />)
                );
              })}
            </Picker>
            <Input
              style={{ ...styles.input, ...styles.inputTitle }}
              placeholder={languageService.tags.Title}
              placeholderTextColor={argonTheme.COLORS.PLACEHOLDER}
              value={this.state.title}
              onChangeText={value => this.setState({ title: value })}
              color="black"
            />
            <Input
              style={{ ...styles.input, ...styles.inputTitle }}
              placeholder={languageService.tags.Amount}
              placeholderTextColor={argonTheme.COLORS.PLACEHOLDER}
              value={this.state.amount ? this.state.amount.toString() : ''}
              onChangeText={value => this.setState({ amount: value })}
              color="black"
            />
            <Picker
              style={styles.project}
              selectedValue={this.state.project}
              onValueChange={(item, itemIndex) => { console.log({ itemValue: item }); this.setState({ project: item }) }}
            >
              <Picker.Item label={languageService.tags.Projects} key="picker-project" value={undefined} />
              {this.state.projects.map((item: Project, index) => {
                return (
                  (<Picker.Item label={item.title} key={`${item.id}`} value={item.id} />)
                );
              })}
            </Picker>
          </View>
          <View style={styles.footerContainer} >
            <ErrorMessage errorResponse={this.state.errorResponse} style={styles.errorMessageContainer} />
            <Button onPress={() => { this.submit(); }} >
              Submit
          </Button>
          </View>
        </View>
      </Dialog>
    );
  }
}

CreateTransaction.propTypes = {

}

const styles = StyleSheet.create({
  dialog: {
    flexDirection: 'row'
  },
  container: {
    flex: 1,
    flexDirection: 'column',
    display: 'flex',
    flexGrow: 1
  },
  headerContainer: {
    alignItems: 'center',
    flexDirection: 'column',
  },
  dateTimePicker: {
    margin: 1,
  },
  bodyContainer: {
    flexDirection: 'column',
    alignItems: 'center',
  },
  input: {
    width: width * 0.7
  },
  inputTitle: {

  },
  inputIcons: {
    marginRight: 12
  },
  recipient: {
    height: 50,
    width: width * 0.7
  },
  project: {
    height: 50,
    width: width * 0.7
  },
  footerContainer: {
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'flex-end',
    flex: 1
  },
  errorMessageContainer: {
    flex: 1
  }

});

export default CreateTransaction;