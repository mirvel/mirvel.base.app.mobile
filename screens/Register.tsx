import React from "react";
import {
  Dimensions,

  KeyboardAvoidingView, StyleSheet
} from "react-native";
import RNVIcons from 'react-native-vector-icons/FontAwesome';
import { BaseComponent } from "../BaseComponent";
import { Button, Icon, Input } from "../components";
import { Block, Text } from '../components/custom/src';
import Dialog from "../components/Dialog";
import ErrorMessage from "../components/ErrorMessage";
import { argonTheme } from "../constants";


const { width, height } = Dimensions.get("screen");

class Register extends BaseComponent {

  getInitialState() {
    return {
      isLoading: false,
      email: '',
      password: '',
      username: '',
      submitted: false,
      errorResponse: undefined
    }
  }
  state = this.getInitialState();

  render() {
    const { languageService } = this.context.serviceProviders;
    return (
      <Dialog>
        <Text style={{ fontSize: 24, padding: 20 }}><RNVIcons name="pencil-square-o" style={{ fontSize: 24 }} />  {languageService.tags.Registration}</Text>
        <KeyboardAvoidingView
          style={{ flex: 1 }}
          behavior="padding"
          enabled
        >
          <Block width={width * 0.8} style={{ marginBottom: 15 }}>
            <Input
              borderless
              placeholder={languageService.tags.Username}
              value={this.state.username}
              onChangeText={username => this.setState({ username: username })}
              iconContent={
                <Text style={{ paddingRight: 8 }}><RNVIcons name="user" size={16} color={argonTheme.COLORS.ICON} /></Text>
              }
            />
          </Block>
          <Block width={width * 0.8} style={{ marginBottom: 15 }}>
            <Input
              borderless
              placeholder={languageService.tags.Email}
              value={this.state.email}
              onChangeText={email => this.setState({ email: email })}
              iconContent={
                <Icon
                  size={16}
                  color={argonTheme.COLORS.ICON}
                  name="ic_mail_24px"
                  family="ArgonExtra"
                  style={styles.inputIcons}
                />
              }
            />
          </Block>
          <Block width={width * 0.8} >
            <Input
              password
              borderless
              placeholder={languageService.tags.Password}
              value={this.state.password}
              onChangeText={password => this.setState({ password: password })}
              iconContent={
                <Icon
                  size={16}
                  color={argonTheme.COLORS.ICON}
                  name="padlock-unlocked"
                  family="ArgonExtra"
                  style={styles.inputIcons}
                />
              }
            />
          </Block>
          <Block middle>
            <Button color="primary" style={styles.createButton}
              onPress={e => { this.register(e); }}>
              <Text bold size={14} color={argonTheme.COLORS.WHITE}>
                {languageService.tags.Register}
              </Text>
            </Button>
          </Block>
          <Block style={{ marginTop: 10 }}>
            <ErrorMessage errorResponse={this.state.errorResponse} />
          </Block>
        </KeyboardAvoidingView>
      </Dialog>
    );
  }

  resetState() {
    this.setState(this.getInitialState());
  }

  register(e) {
    const { authService } = this.context.serviceProviders;
    const { navigation } = this.props;
    this.setState({ isLoading: true });
    const subscription = authService.register(this.state).subscribe(
      r => {
        console.log('register complete');
        console.log(r);
        this.resetState();
        subscription.unsubscribe();
        navigation.navigate('Home');
      },
      errorResponse => {
        this.setState({ errorResponse: errorResponse })
      });

  }
}

const styles = StyleSheet.create({
  registerContainer: {
    width: width * 0.9,
    height: height * 0.78,
    backgroundColor: "#F4F5F7",
    borderRadius: 4,
    shadowColor: argonTheme.COLORS.BLACK,
    shadowOffset: {
      width: 0,
      height: 4
    },
    shadowRadius: 8,
    shadowOpacity: 0.1,
    elevation: 1,
    overflow: "hidden"
  },
  inputIcons: {
    marginRight: 12
  },
  passwordCheck: {
    paddingLeft: 15,
    paddingTop: 13,
    paddingBottom: 30
  },
  createButton: {
    width: width * 0.5,
    marginTop: 25
  }
});

export default Register;
