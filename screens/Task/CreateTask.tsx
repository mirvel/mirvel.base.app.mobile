import { Picker } from '@react-native-picker/picker';
import React from 'react';
import {
  DatePickerAndroid, Dimensions, StyleSheet, TouchableOpacity
} from "react-native";
import RNVIcons from 'react-native-vector-icons/FontAwesome';
import { BaseComponent } from "../../BaseComponent";
import { Button, Icon, Input } from "../../components";
import { Block, Text } from '../../components/custom/src';
import Dialog from '../../components/Dialog';
import ErrorMessage from '../../components/ErrorMessage';
import { argonTheme } from "../../constants";
import { TaskModel, TaskState } from '../../services/models/Task';
import Loading from '../Loading';

const { width, height } = Dimensions.get("screen");

class CreateTask extends BaseComponent {
  subscriptions$ = [];
  focusListener;
  getInitialState() {
    return {
      title: '',
      description: '',
      points: 0,
      assigned_to: undefined,
      assigned_date: undefined,
      scheduled_date: undefined,
      visible_to_team: undefined,
      isSaving: false,
      isLoadingComplete: false,
      errorResponse: undefined,
      showTime: false,
      showPoints: true,
      teams: [],
      members: [],
    }
  }
  state = this.getInitialState();

  covertStateToModel(state): TaskModel {
    const model = Object.assign(new TaskModel(), state);
    delete model.isSaving;
    delete model.isLoadingComplete;
    delete model.errorResponse;
    delete model.showTime;
    delete model.showPoints;
    delete model.teams;
    delete model.members;
    return model;
  }

  dateWithoutTime(date: Date) {
    if (date && date.setHours)
      return new Date(date.setHours(0, 0, 0, 0));
    return date;
  }


  componentDidMount() {
    let { navigation } = this.props;
    this.focusListener = navigation.addListener("focus", (payload) => {
      const { teamService, userService, taskService } = this.context.serviceProviders;
      this.setState({ isLoadingComplete: false });
      navigation = this.props.navigation;
      const currentItem = this.props.route && this.props.route.params && this.props.route.params.currentItem;

      if (currentItem && currentItem.id) {
        taskService.getTask(currentItem.id).subscribe((task) => {
          this.setState({ ...task, isLoadingComplete: true });
          navigation.setParams({ currentItem: undefined }); // Once loaded clean params currentItem
        })

      }
      else {
        this.setState(this.getInitialState());
        this.setState({ isLoadingComplete: true });
      }
      this.subscriptions$.push(teamService.getAll().subscribe(
        (teams) => {
          this.setState({ teams });
        }
      ));
      this.subscriptions$.push(userService.getAllFiltered().subscribe(
        (members) => {
          this.setState({ members });
        }
      ));

      console.log({ currentItem });

    });
  }

  componentWillUnmount() {
    // Remove the event listener
    if (this.focusListener && this.focusListener.remove)
      this.focusListener.remove();
    this.subscriptions$.forEach(subscription => {
      subscription.unsubsribe();
    });
  }

  save() {
    const { taskService, notificationService } = this.context.serviceProviders;
    const { navigation } = this.props;
    this.setState({ isSaving: true });
    taskService.createOrUpdateTask(this.covertStateToModel(this.state)).subscribe(
      r => {
        this.setState(this.getInitialState());
        navigation.goBack();
      }, errorResponse => {
        this.setState({ errorResponse: errorResponse })
      });
  }
  complete() {
    const { taskService, notificationService } = this.context.serviceProviders;
    const { navigation } = this.props;
    this.setState({ isSaving: true, state: TaskState.complete }, () => {
      taskService.createOrUpdateTask(this.covertStateToModel(this.state)).subscribe(
        r => {
          this.setState(this.getInitialState());
          navigation.goBack();
        }, errorResponse => {
          this.setState({ errorResponse: errorResponse })
        });
    });

  }
  async showDateTimePicker() {
    const { action, year, month, day } = await DatePickerAndroid.open({
      date: this.state.scheduled_date ? new Date(this.state.scheduled_date) : new Date(),
    }) as any;
    console.log('showdatetimepicker')
    if (action !== DatePickerAndroid.dismissedAction) {
      const selectedDate = new Date(year, month, day)
      console.log(selectedDate);
      this.setState({ scheduled_date: selectedDate })
    }
  }

  render() {
    const { languageService } = this.context.serviceProviders;
    return !this.state.isLoadingComplete ? <Loading></Loading> : (
      <Dialog>
        <Text style={{ fontSize: 24, padding: 20 }}><RNVIcons name="list-alt" style={{ fontSize: 24 }} />  {languageService.tags.TaskTitle}</Text>
        <Block width={width * 0.8} style={styles.inputContainer}>
          <Input
            borderless
            placeholder={languageService.tags.Title}
            value={this.state.title}
            required={true}
            onChangeText={value => this.setState({ title: value })}
            iconContent={
              <Icon
                size={16}
                color={argonTheme.COLORS.ICON}
                name="hat-3"
                family="ArgonExtra"
                style={styles.inputIcons}
              />
            }
          />
        </Block>
        <Block width={width * 0.8} style={styles.inputContainer}>
          <Input
            multiline={true}
            placeholder={languageService.tags.Description}
            value={this.state.description}
            onChangeText={value => this.setState({ description: value })}
            iconContent={
              <Icon
                size={16}
                color={argonTheme.COLORS.ICON}
                name="hat-3"
                family="ArgonExtra"
                style={{ marginTop: 5, ...styles.inputIcons }}
              />
            }
          />
        </Block>
        <TouchableOpacity onPress={_ => this.showDateTimePicker()}>
          <Block width={width * 0.8} style={styles.inputContainer}>
            <Input
              borderless
              placeholder={languageService.tags.Date}
              value={this.state.scheduled_date ? new Date(this.state.scheduled_date).toLocaleDateString(languageService.language) : ''}
              editable={false}
              iconContent={
                <Text style={{ ...styles.inputIcons, paddingRight: 5 }}><RNVIcons name="calendar" /></Text>
              }
            />
          </Block>
        </TouchableOpacity>
        {this.state.showPoints &&
          (
            <Block width={width * 0.8} style={styles.inputContainer}>
              <Input
                borderless
                placeholder={languageService.tags.Points}
                value={`${this.state.points}`}
                onChangeText={value => this.setState({ points: value })}
                iconContent={
                  <Icon
                    size={16}
                    color={argonTheme.COLORS.ICON}
                    name="hat-3"
                    family="ArgonExtra"
                    style={styles.inputIcons}
                  />
                }
              />
            </Block>
          )
        }
        <Block>
          <Picker
            style={{ height: 50, width: 250 }}
            selectedValue={this.state.visible_to_team}
            onValueChange={(itemValue, itemIndex) => { console.log({ itemValue }); this.setState({ visible_to_team: itemValue }) }}
          >
            <Picker.Item label="Choose Team" key="picker-default" value="" />
            {this.state.teams.map((team, index) => {
              return (
                (<Picker.Item label={team.title} key={`${team.id}`} value={team.id} />)
              );
            })}
          </Picker>
        </Block>
        <Block>
          <Picker
            style={{ height: 50, width: 250 }}
            selectedValue={this.state.assigned_to}
            onValueChange={(itemValue, itemIndex) => { console.log({ itemValue }); this.setState({ assigned_to: itemValue }) }}
          >
            <Picker.Item label="Assign To" key="picker-default" value="" />
            {this.state.members.map((member, index) => {
              return (
                (<Picker.Item label={member.username} key={`${member.id}`} value={member.id} />)
              );
            })}
          </Picker>
        </Block>

        <Block middle style={{ flexDirection: 'row', width: width * 0.8 }}>
          <Button color="primary" style={styles.createButton}
            onPress={e => { this.complete(); }}
          >
            <Text bold size={14} color={argonTheme.COLORS.WHITE}>
              {languageService.tags.Done}
            </Text>
          </Button>
          <Button color="primary" style={styles.createButton}
            onPress={e => { this.save(); }}
          >
            <Text bold size={14} color={argonTheme.COLORS.WHITE}>
              {languageService.tags.Save}
            </Text>
          </Button>
        </Block>
        <Block>
          <ErrorMessage errorResponse={this.state.errorResponse} />
        </Block>
      </Dialog>
    );
  }
}

const styles = StyleSheet.create({
  registerContainer: {
    width: width * 0.9,
    height: height * 0.78,
    backgroundColor: "#F4F5F7",
    borderRadius: 4,
    shadowColor: argonTheme.COLORS.BLACK,
    shadowOffset: {
      width: 0,
      height: 4
    },
    shadowRadius: 8,
    shadowOpacity: 0.1,
    elevation: 1,
    overflow: "hidden"
  },

  inputIcons: {
    marginRight: 12
  },
  inputContainer: {
    margin: 1,
  },

  createButton: {
    width: width * 0.3,
    marginTop: 25,
    marginHorizontal: 5
  }
});

export default CreateTask;
