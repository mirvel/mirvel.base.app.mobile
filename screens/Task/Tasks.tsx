import React from 'react';
import { StyleSheet, Dimensions, View, FlatList, Text, SafeAreaView } from 'react-native';
import { Block, theme } from '../../components/custom/src';
import { CardTask } from '../../components';

import { BaseComponent } from "../../BaseComponent";
import { merge } from 'rxjs';
import { FabMenu } from '../FabMenu';
import { TaskModel } from '../../services/models/Task';
import { ScreensTypes } from "../../navigation/ScreensTypes";

const { width } = Dimensions.get('screen');

class Tasks extends BaseComponent {
  notificationSound: any;
  state = { refreshing: false, dataSource: [], transactions: [], tasks: [], totalPoints: undefined, isLoading: false }
  subscriptions$ = [];
  openTask(currentItem: TaskModel) {
    console.log('open');
    console.log({ currentItem });
    const { navigation } = this.props;
    navigation.navigate(ScreensTypes.CreateTask, { currentItem });

  }
  renderArticles = () => {
    const { navigation } = this.props;
    return (
      <View style={{ flex: 1, backgroundColor: '#f3f3f3' }}>
        <SafeAreaView style={styles.articles}>

          <FlatList
            data={this.state.tasks}
            onRefresh={() => { this.getLastTasks() }}
            renderItem={({ item }) =>
              (<CardTask item={item} horizontal onCustomClick={() => { this.openTask(item) }} />)}
            keyExtractor={({ id }, index) => id.toString()}
            refreshing={this.state.refreshing}
          />

        </SafeAreaView>
        <FabMenu navigation={navigation} />
      </View>
    )
  }
  getLastTasks() {
    const { taskService } = this.context.serviceProviders;
    return taskService.getAllFiltered().subscribe((response) => {
      this.setState({
        isLoading: false,
        tasks: response,
        refreshing: false
      });
    },
      e => {
        console.log('home error');
      });
  }

  async componentDidMount() {
    const { authService, transactionService, taskService, notificationService } = this.context.serviceProviders;

    // Load last transactions when component mounted
    this.subscriptions$.push(this.getLastTasks());

    const updateEvents = merge(transactionService.updated, authService.signinComplete, taskService.updated, notificationService.notificationReceived);

    // Whenever there is a new transaction update the list
    this.subscriptions$.push(updateEvents.subscribe(() => {
      console.log('update event------------------------------------------------------>')
      this.getLastTasks();
    },
      e => {
        console.log('home error');
      }));


  }

  componentWillUnmount() {
    this.subscriptions$.forEach(subscription => subscription.unsubscribe());
  }
  render() {
    return (
      <Block flex center style={styles.home}>
        {this.renderArticles()}
      </Block>
    );
  }
}

const styles = StyleSheet.create({
  home: {
    width: width,
  },
  articles: {
    width: width - theme.SIZES.BASE * 2,
    paddingVertical: theme.SIZES.BASE,
  },
});

export default Tasks;
