import React from "react";
import { Dimensions, KeyboardAvoidingView, ScrollView, StyleSheet, View } from "react-native";
import RNVIcons from 'react-native-vector-icons/FontAwesome';
import { merge } from "rxjs";
import { BaseComponent } from "../../BaseComponent";
import { Button, Input } from "../../components";
import { Block, Text, theme } from '../../components/custom/src';
import Dialog from "../../components/Dialog";
import { ErrorMessage } from '../../components/ErrorMessage';
import { argonTheme } from "../../constants";
import { TeamModel } from "../../services/models/TeamModel";
import { UserModel } from "../../services/models/UserModel";
import Loading from "../Loading";


const { width, height } = Dimensions.get("screen");

class EditTeam extends BaseComponent {
    getInitialState() {
        return {
            id: undefined,
            isSaving: false,
            isUpdating: false,
            isLoadingComplete: false,
            title: undefined,
            users: [],
            members: [],
            existingMembers: [],
            errorResponse: undefined
        }
    }

    state = this.getInitialState();
    subscriptions$ = [];

    covertStateToModel(state): TeamModel {
        const model = Object.assign({}, state);
        delete model.isSaving;
        delete model.isUpdating;
        delete model.errorResponse;
        delete model.users;
        delete model.members;
        delete model.existingMembers;
        return model;
    }

    formatDate(date: Date) {
        if (date && date.toUTCString)
            return date.toUTCString();
        return date;
    }

    getTeam() {
        const { teamService, accountService } = this.context.serviceProviders;
        const { selectedId } = this.props.route.params;
        this.subscriptions$.push(teamService.get(selectedId).subscribe((data: any) => {
            this.setState(data);
            this.subscriptions$.push(accountService.getAll().subscribe((response) => {
                const existingMembers = data.members.map((id) => {
                    return response.find(user => user.id === id);
                });
                const users = response.filter((user) => {
                    return !existingMembers.some(member => member.id === user.id);
                })
                this.setState({ existingMembers, users, data, isLoadingComplete: true });
            }));
        }));

    }
    componentDidMount() {
        let { navigation } = this.props;
        this.focusListener = navigation.addListener("focus", (payload) => {
            this.getTeam();
        });

        const { teamService, authService, userService } = this.context.serviceProviders;
        const updateEvents = merge(authService.signinComplete, teamService.updated);

        // Whenever there is a new transaction update the list
        this.subscriptions$.push(updateEvents.subscribe(() => {
            console.log('updateEvents');
        },
            e => {
                console.log('home error');
            }));
    }
    componentWillUnMount() {
        this.subscriptions$.forEach(subscription => {
            subscription.unsubsribe();
        });
    }

    submit() {
        const { teamService } = this.context.serviceProviders;
        const { navigation } = this.props;
        this.setState({ isSaving: true });
        teamService.update(this.state.id, this.covertStateToModel(this.state)).subscribe(
            r => {
                console.log(r);
                this.setState(this.getInitialState());
                navigation.navigate('Teams');
            }, errorResponse => {
                this.setState({ errorResponse: errorResponse })
            });
    }

    toggleSelection(list, name, id) {
        list = list.slice();
        list.forEach(el => {
            if (el.id === id)
                el.selected = !el.selected;
        });
        const newValue = {};
        newValue[name] = list;
        this.setState(newValue);
    }
    toggleSelectionUser(id: number) {
        this.toggleSelection(this.state.users, 'users', id);
    }
    addToMembers(newMember: UserModel) {
        const { teamMembershipService } = this.context.serviceProviders;
        teamMembershipService.create({ team: this.state.id, member: newMember.id }).subscribe((response) => {
            this.setState({
                existingMembers: [...this.state.existingMembers, newMember],
                users: this.state.users.filter((user) => user.id !== newMember.id)
            });
        }, errorResponse => {
            this.setState({ errorResponse: errorResponse })
        });
    }
    removeFromMembers(removedMember: UserModel) {
        const { teamMembershipService } = this.context.serviceProviders;
        this.setState({ isUpdating: true });
        // Temporary solution. We should already have teamMembershipIds
        teamMembershipService.findByMemberAndTeamId(removedMember.id, this.state.id).subscribe((teamMembership) => {
            console.log({ teamMembership })
            if (teamMembership && teamMembership[0])
                teamMembershipService.delete(teamMembership[0].id).subscribe((response) => {
                    console.log('delete complete');
                    console.log(response);
                    if (!response) {
                        console.log(this.state.existingMembers);
                        console.log(removedMember);

                        const newExistingMembers = this.state.existingMembers.filter((member) => member.id !== removedMember.id);
                        console.log(newExistingMembers);
                        this.setState({
                            existingMembers: newExistingMembers,
                            users: [...this.state.users, removedMember],
                            isUpdating: false
                        });
                    }
                }, errorResponse => {
                    this.setState({ errorResponse: errorResponse })
                });
            else {
                this.setState({ errorResponse: { field: 'detail', message: 'Team Not found' } });
            }
        }, errorResponse => {
            this.setState({ errorResponse: errorResponse })
        });

    }

    render() {
        const { languageService } = this.context.serviceProviders;
        return !this.state.isLoadingComplete ? <Loading /> : (
            <Dialog>
                <Text style={styles.title}> {this.state.title}</Text>
                <KeyboardAvoidingView
                    style={{ flex: 1 }}
                    behavior="padding"
                    enabled
                >
                    <View style={{ flexDirection: 'column' }}>
                        <View style={styles.blockStyle}>
                            <Input
                                borderless
                                placeholder={languageService.tags.TeamName}
                                value={this.state.title}
                                onChangeText={value => this.setState({ title: value })}
                                iconContent={
                                    <Text style={{ paddingRight: 8 }}><RNVIcons name="user" size={16} color={argonTheme.COLORS.ICON} /></Text>
                                }
                            />
                        </View>
                        <View style={styles.blockStyle}>
                            <Button color="primary" style={styles.createButton}
                                onPress={e => { this.submit(); }}>
                                <Text bold size={14} color={argonTheme.COLORS.WHITE}>
                                    {languageService.tags.Update}
                                </Text>
                            </Button>
                        </View>
                        <View style={styles.blockStyle}>
                            {this.state.errorResponse && (<ErrorMessage errorResponse={this.state.errorResponse} />)}
                        </View>
                        <View style={styles.scrollViewContainer} pointerEvents={this.state.isUpdating ? 'none' : 'auto'}>
                            <ScrollView showsVerticalScrollIndicator={true}>
                                <View style={styles.scrollViewContentContainer}>
                                    {this.state.existingMembers.map((item, index) => {
                                        return (
                                            (<Block key={index} style={styles.buttonContainer}><Button style={{ ...styles.buttonUsers, ...styles.existingMembers }} onPress={() => { this.removeFromMembers(item) }}><Text style={{ color: 'white' }}><RNVIcons name="minus-circle" />  {item.username} </Text></Button></Block>)
                                        );
                                    })}
                                    {this.state.users.map((item, index) => {
                                        return (
                                            (<Block key={index} style={styles.buttonContainer}><Button style={{ ...styles.buttonUsers, ...styles.newUsers }} onPress={() => { this.addToMembers(item) }}><Text style={{ color: 'white' }}><RNVIcons name="plus-circle" />  {item.username}</Text></Button></Block>)
                                        );
                                    })}
                                </View>
                            </ScrollView>
                        </View>
                    </View>
                </KeyboardAvoidingView>
            </Dialog>
        );
    }
}

const styles = StyleSheet.create({
    registerContainer: {
        width: width * 0.9,
        height: height * 0.78,
        backgroundColor: "#F4F5F7",
        borderRadius: 4,
        shadowColor: argonTheme.COLORS.BLACK,
        shadowOffset: {
            width: 0,
            height: 4
        },
        shadowRadius: 8,
        shadowOpacity: 0.1,
        elevation: 1,
        overflow: "hidden"
    },
    title: {
        fontSize: 24,
        padding: 10
    },
    input: {
        margin: 0
    },
    inputIcons: {
        marginRight: 12
    },
    createButton: {
        width: width * 0.5,
    },
    buttonContainer: {
        flex: 1,
        flexDirection: 'row',
        padding: 0
    },
    buttonUsers: {
        flex: 1,
        margin: 1
    },
    existingMembers: {
        backgroundColor: 'orange',
    },
    newUsers: {
        color: 'white',
    },
    scrollView: {
        width: width - theme.SIZES.BASE * 2,
        paddingVertical: theme.SIZES.BASE,
    },
    scrollViewContainer: {
        margin: 10,
        flex: 1,
        width: width * 0.8,
    },
    scrollViewContentContainer: {
        display: 'flex',
        flexDirection: "column",
        flex: 1,
    },
    blockStyle: {
        flex: 0,
        alignItems: 'center',
        paddingHorizontal: 10,
        paddingVertical: 3,
        flexGrow: 0,
    }
});

export default EditTeam;
