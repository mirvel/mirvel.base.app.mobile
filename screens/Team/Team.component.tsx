import React from "react";
import { Dimensions, ScrollView, StyleSheet, View } from "react-native";
import { merge } from "rxjs";
import { BaseComponent } from "../../BaseComponent";
import { Button } from "../../components";
import { Block, Text, theme } from '../../components/custom/src';
import Dialog from "../../components/Dialog";
import { argonTheme } from "../../constants";
import { ScreensTypes } from "../../navigation/ScreensTypes";
import { TeamModel } from "../../services/models/TeamModel";
import Loading from "../Loading";


const { width, height } = Dimensions.get("screen");

class Team extends BaseComponent {
    getInitialState() {
        return {
            id: undefined,
            isSaving: false,
            isUpdating: false,
            isLoadingComplete: false,
            title: undefined,
            users: [],
            members: [],
            existingMembers: [],
            errorResponse: undefined
        }
    }

    state = this.getInitialState();
    subscriptions$ = [];

    covertStateToModel(state): TeamModel {
        const model = Object.assign({}, state);
        delete model.isSaving;
        delete model.isUpdating;
        delete model.errorResponse;
        delete model.users;
        delete model.members;
        delete model.existingMembers;
        return model;
    }

    formatDate(date: Date) {
        if (date && date.toUTCString)
            return date.toUTCString();
        return date;
    }
    click(account): void {
        const { navigation } = this.props;
        console.log('click');
        console.log({ account });
        navigation.navigate(ScreensTypes.Account, { accoundId: account.id });
    }


    getTeam() {
        const { teamService, accountService } = this.context.serviceProviders;
        const { selectedId } = this.props.route.params;
        this.subscriptions$.push(teamService.get(selectedId).subscribe((data: TeamModel) => {
            console.log({ data });
            this.setState({ existingMembers: data.members, data, isLoadingComplete: true, title: data.title });
        }));

    }
    componentDidMount() {
        let { navigation } = this.props;
        this.focusListener = navigation.addListener("focus", (payload) => {
            this.getTeam();
        });

        const { teamService, authService, userService } = this.context.serviceProviders;
        const updateEvents = merge(authService.signinComplete, teamService.updated);

        // Whenever there is a new transaction update the list
        this.subscriptions$.push(updateEvents.subscribe(() => {
            console.log('updateEvents');
        },
            e => {
                console.log('home error');
            }));
    }
    componentWillUnMount() {
        this.subscriptions$.forEach(subscription => {
            subscription.unsubsribe();
        });
    }

    render() {
        const { languageService } = this.context.serviceProviders;
        return !this.state.isLoadingComplete ? <Loading /> : (
            <Dialog>
                <Text style={styles.title}> {this.state.title}</Text>
                <View style={{ flexDirection: 'column' }}>
                    <View style={styles.scrollViewContainer} pointerEvents={this.state.isUpdating ? 'none' : 'auto'}>
                        <ScrollView showsVerticalScrollIndicator={true}>
                            <View style={styles.scrollViewContentContainer}>
                                {this.state.existingMembers.map((item, index) => {
                                    return (
                                        <Block key={index} style={styles.itemContainer}>
                                            <Button style={{ ...styles.button }} onPress={() => { this.click(item) }}><Text style={{ ...styles.buttonText }}> {item.username} </Text></Button>
                                        </Block>
                                    );
                                })}

                            </View>
                        </ScrollView>
                    </View>
                </View>
            </Dialog>
        );
    }
}

const styles = StyleSheet.create({
    registerContainer: {
        width: width * 0.9,
        height: height * 0.78,
        backgroundColor: "#F4F5F7",
        borderRadius: 4,
        shadowColor: argonTheme.COLORS.BLACK,
        shadowOffset: {
            width: 0,
            height: 4
        },
        shadowRadius: 8,
        shadowOpacity: 0.1,
        elevation: 1,
        overflow: "hidden"
    },
    title: {
        fontSize: 24,
        padding: 10
    },
    input: {
        margin: 0
    },
    inputIcons: {
        marginRight: 12
    },
    createButton: {
        width: width * 0.5,
    },
    itemContainer: {
        flex: 1,
        flexDirection: 'row',
        padding: 0,
    },
    buttonText: {
        color: 'white'
    },
    button: {
        flex: 1,
        margin: 1,
        borderRadius: 0,
        backgroundColor: 'grey',
    },

    scrollView: {
        width: width - theme.SIZES.BASE * 2,
        paddingVertical: theme.SIZES.BASE,
    },
    scrollViewContainer: {
        margin: 10,
        flex: 1,
        width: width * 0.8,
    },
    scrollViewContentContainer: {
        display: 'flex',
        flexDirection: "column",
        flex: 1,
    },
    blockStyle: {
        flex: 0,
        alignItems: 'center',
        paddingHorizontal: 10,
        paddingVertical: 3,
        flexGrow: 0,
    }
});

export default Team;
