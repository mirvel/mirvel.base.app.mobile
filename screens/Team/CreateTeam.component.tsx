import React from "react";
import {
  Dimensions,

  KeyboardAvoidingView, StyleSheet
} from "react-native";
import RNVIcons from 'react-native-vector-icons/FontAwesome';
import { BaseComponent } from "../../BaseComponent";
import { Button, Input } from "../../components";
import { Block, Text } from '../../components/custom/src';
import Dialog from "../../components/Dialog";
import { ErrorMessage } from '../../components/ErrorMessage';
import { argonTheme } from "../../constants";
import { TeamModel } from "../../services/models/TeamModel";


const { width, height } = Dimensions.get("screen");

class CreateTeam extends BaseComponent {
  getInitialState() {
    return {
      isLoading: false,
      title: '',
      errorResponse: undefined
    }
  }

  state = this.getInitialState();

  covertStateToModel(state): TeamModel {
    const model = Object.assign(new TeamModel(), state);
    delete model.isLoading;
    delete model.errorResponse;
    return model;
  }

  formatDate(date: Date) {
    if (date && date.toUTCString)
      return date.toUTCString();
    return date;
  }

  submit() {
    const { teamService } = this.context.serviceProviders;
    const { navigation } = this.props;
    this.setState({ isLoading: true });
    teamService.create(this.covertStateToModel(this.state)).subscribe(
      r => {
        this.setState(this.getInitialState());
        navigation.navigate('Teams');
      }, errorResponse => {
        this.setState({ errorResponse: errorResponse })
      });
  }

  render() {
    const { languageService } = this.context.serviceProviders;
    return (
      <Dialog>
        <Text style={{ fontSize: 24, padding: 20 }}><RNVIcons name="sign-in" style={{ fontSize: 24 }} />  {languageService.tags.Team}</Text>
        <KeyboardAvoidingView
          style={{ flex: 1 }}
          behavior="padding"
          enabled
        >
          <Block width={width * 0.8} style={{ marginBottom: 15 }}>
            <Input
              borderless
              placeholder={languageService.tags.TeamName}
              value={this.state.title}
              onChangeText={value => this.setState({ title: value })}
              iconContent={
                <Text style={{ paddingRight: 8 }}><RNVIcons name="user" size={16} color={argonTheme.COLORS.ICON} /></Text>
              }
            />
          </Block>
          <Block middle>
            <Button color="primary" style={styles.createButton}
              onPress={e => { this.submit(); }}>
              <Text bold size={14} color={argonTheme.COLORS.WHITE}>
                {languageService.tags.Submit}
              </Text>
            </Button>
          </Block>
          <Block style={{ marginTop: 10 }}>
            <ErrorMessage errorResponse={this.state.errorResponse} />
          </Block>
        </KeyboardAvoidingView>
      </Dialog>
    );
  }
}

const styles = StyleSheet.create({
  registerContainer: {
    width: width * 0.9,
    height: height * 0.78,
    backgroundColor: "#F4F5F7",
    borderRadius: 4,
    shadowColor: argonTheme.COLORS.BLACK,
    shadowOffset: {
      width: 0,
      height: 4
    },
    shadowRadius: 8,
    shadowOpacity: 0.1,
    elevation: 1,
    overflow: "hidden"
  },
  inputIcons: {
    marginRight: 12
  },
  createButton: {
    width: width * 0.5,
    marginTop: 25
  }
});

export default CreateTeam;
