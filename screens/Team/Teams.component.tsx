import React from 'react';
import { Dimensions, FlatList, SafeAreaView, StyleSheet, View } from 'react-native';
import { merge } from 'rxjs';
import { map } from 'rxjs/operators';
import { BaseComponent } from "../../BaseComponent";
import { Card } from '../../components';
import { Block, theme } from '../../components/custom/src';
import Dialog from '../../components/Dialog';
import { ScreensTypes } from '../../navigation/ScreensTypes';


const { width } = Dimensions.get('screen');

class Teams extends BaseComponent {
  state = { dataSource: [], transactions: [], items: [], totalPoints: undefined, isLoading: false }
  subscriptions$ = [];

  renderArticles = () => {
    const { navigation } = this.props;
    return (
      <Dialog>
        <View style={{ flex: 1, flexDirection: 'row' }}>
          <SafeAreaView style={{ flex: 1, margin: 10 }}>
            <FlatList
              data={this.state.items}
              renderItem={({ item }) =>
                <Card item={item}
                  pointsStyle={{ fontSize: 14, alignSelf: 'flex-end', position: 'absolute', zIndex: 1, right: 10, top: 5, padding: 3, minWidth: 60, textAlign: 'center' }}
                  titleStyle={{ fontWeight: 'bold', color: 'black', fontSize: 16, maxWidth: 200, alignSelf: 'flex-start', }}
                  horizontal imageContainerStyle={{ display: 'none' }} style={{ minHeight: 40, marginBottom: 2, marginVertical: 2 }}
                  contentContainerStyle={{ alignItems: 'flex-start', justifyContent: 'center' }}
                  descriptionStyle={{ marginLeft: 50 }}
                  onCustomClick={e => { console.log('navigate'); navigation.navigate(ScreensTypes.Team, { selectedId: item.id }); }}
                />}
              keyExtractor={({ id }, index) => id}
            />
          </SafeAreaView>
        </View>
      </Dialog>
    )
  }
  getRemapFn() {
    const mapTo = (response) => {
      if (!response || !response.results)
        return { results: [] };
      console.log(response);
      const newResults = response.results.map(x => {
        let newObj =
        {
          title: x.title,
          horizontal: true,
          description: x.description,
          id: x.id.toString()
        };
        return newObj;
      });

      return Object.assign(response, { results: newResults });
    };
    return mapTo;
  }
  getLastTasks() {
    const { teamService } = this.context.serviceProviders;
    return teamService.getLast(10, 0).pipe(map(this.getRemapFn())).subscribe((response) => {
      console.log(response);
      if (response && response.results)
        this.setState({
          isLoading: false,
          items: response.results
        });
    },
      e => {
        console.log('home error');
      });
  }
  componentDidMount() {
    const { authService, teamService } = this.context.serviceProviders;

    // Load last transactions when component mounted
    this.subscriptions$.push(this.getLastTasks());

    const updateEvents = merge(authService.signinComplete, teamService.updated);

    // Whenever there is a new transaction update the list
    this.subscriptions$.push(updateEvents.subscribe(() => {
      this.getLastTasks();
    },
      e => {
        console.log('home error');
      }));
  }

  componentWillUnmount() {
    this.subscriptions$.forEach(subscription => subscription.unsubscribe());
  }
  render() {
    return (
      <Block flex center style={styles.home}>
        {this.renderArticles()}
      </Block>
    );
  }
}

const styles = StyleSheet.create({
  home: {
    width: width,
  },
  articles: {
    width: width - theme.SIZES.BASE * 2,
    paddingVertical: theme.SIZES.BASE,
  },
});

export default Teams;
