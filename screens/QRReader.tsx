import React, { useState, useEffect } from 'react';
import {
  StyleSheet,
  Dimensions,
} from "react-native";
import { Block, Text } from '../components/custom/src';
import { Button, Icon, Input } from "../components";
import { BarCodeScanner } from 'expo-barcode-scanner';
import * as Permissions from 'expo-permissions';
import { BaseComponent } from "../BaseComponent";
import RNVIcons from 'react-native-vector-icons/FontAwesome';

const { width, height } = Dimensions.get("screen");

class QRReader extends BaseComponent {
  state = {
    isLoading: false,
    title: 'new title:' + new Date(),
    description: 'new description:' + new Date(),
    scanned: false,
    qrdata: undefined,
    hasCameraPermission: null,
  }

  async componentDidMount() {
    this.getPermissionsAsync();
  }

  getPermissionsAsync = async () => {
    const { status } = await Permissions.askAsync(Permissions.CAMERA);
    this.setState({ hasCameraPermission: status === 'granted' });
  };

  close() {
    const { navigation } = this.props;
    this.setState({ scanned: false }, () => {
      navigation.navigate('Home');
    });;

  }

  handleBarCodeScanned({ type, data }) {
    console.log(data);
    if (data) {
      const { navigation } = this.props;
      this.setState({ scanned: true, qrdata: data }, () => {
        navigation.navigate('AquirePoints', { qrdata: data });
      });
    }
  };

  render() {
    const { hasCameraPermission, scanned } = this.state;
    const { isFocused, navigation } = this.props
    const { languageService } = this.context.serviceProviders;

    if (hasCameraPermission === null) {
      return <Text>Requesting for camera permission</Text>;
    }
    if (hasCameraPermission === false) {
      return <Text>No access to camera</Text>;
    }
    return (
      <Block flex style={{ flexDirection: 'column' }} middle>
        <Block flex style={{ flexDirection: 'column', flex: 1 }}>
          <Block center>
            <Text style={{ fontSize: 24, padding: 20 }}><RNVIcons name="diamond" style={{ fontSize: 24 }} />  {languageService.tags.AquirePointsTitle}</Text>
          </Block>
          <Block flex style={{ flexDirection: 'column', flex: 1, padding: 10 }}>
            <BarCodeScanner
              onBarCodeScanned={navigation.isFocused() ? this.handleBarCodeScanned.bind(this) : null}
              style={StyleSheet.absoluteFillObject}
            />
            <Block flex style={{ borderWidth: 3, borderColor: 'white', flex: 1, margin: 80, borderStyle: 'dotted', ...StyleSheet.absoluteFillObject }}>
            </Block>
          </Block>
          <Block style={{ alignItems: 'center', padding: 10 }}>
            <Button onPress={e => { this.close(); }}>
              <Text>{languageService.tags.Close}</Text>
            </Button>
          </Block>
        </Block>
      </Block>
    );

  }
}

export default QRReader;
