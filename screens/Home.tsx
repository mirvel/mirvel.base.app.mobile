import React from 'react';
import { StyleSheet, Dimensions, View, FlatList, Text, SafeAreaView } from 'react-native';
import { Block, theme } from '../components/custom/src';
import { Card } from '../components';
import articles from '../constants/articles';
import { map } from 'rxjs/operators';
import ActionButton from 'react-native-action-button';
import Icon from 'react-native-vector-icons/Ionicons';
import { BaseComponent } from "../BaseComponent";
import { merge } from 'rxjs';
import { ScreensTypes } from "../navigation/ScreensTypes";

const { width } = Dimensions.get('screen');



class Home extends BaseComponent {

  state = { dataSource: [], transactions: [], tasks: [], totalPoints: undefined, isLoading: false }
  movies$;
  users$;
  subscriptions$ = [];

  renderArticles = () => {
    const { navigation } = this.props;
    return (
      <View style={{ flex: 1, backgroundColor: '#f3f3f3' }}>
        <SafeAreaView style={styles.articles}          >
          <FlatList
            data={this.state.transactions}
            renderItem={({ item }) =>
              <Card item={item} titleStyle={{ fontWeight: 'bold', color: 'orange', fontSize: 16 }}
                horizontal imageContainerStyle={{ display: 'none' }} style={{ minHeight: 40, marginBottom: 2, marginVertical: 2 }}
                contentContainerStyle={{ alignItems: 'center', justifyContent: 'center' }} />}
            keyExtractor={({ id }, index) => id}
          />
          <Block flex>
            <View style={{ flex: 1, paddingTop: 20 }}>
              <FlatList
                data={this.state.tasks}
                renderItem={({ item }) =>
                  <Card item={item}
                    pointsStyle={{ color: item.points > 0 ? 'green' : 'red', fontSize: 18, alignSelf: 'flex-end', position: 'absolute', zIndex: 1, backgroundColor: 'lightgrey', right: 10, top: 5, borderRadius: 25, padding: 3, minWidth: 60, textAlign: 'center' }}
                    titleStyle={{ fontWeight: 'bold', color: 'black', fontSize: 16, maxWidth: 200, alignSelf: 'flex-start', }}
                    horizontal imageContainerStyle={{ display: 'none' }} style={{ minHeight: 40, marginBottom: 2, marginVertical: 2 }}
                    contentContainerStyle={{ alignItems: 'center', justifyContent: 'center' }} />}
                keyExtractor={({ id }, index) => id}
              />
            </View>
          </Block>
          <Block flex>
            <Card item={articles[0]} horizontal imageStyle={{ flex: 1, height: undefined, width: undefined }} imageContainerStyle={{ overflow: 'visible', flex: 1, height: undefined, width: undefined }} />
            <Card item={articles[1]} horizontal imageStyle={{ flex: 1, height: undefined, width: undefined }} imageContainerStyle={{ overflow: 'visible', flex: 1, height: undefined, width: undefined }} />
            <Card item={articles[2]} horizontal imageStyle={{ flex: 1, height: undefined, width: undefined }} imageContainerStyle={{ overflow: 'visible', flex: 1, height: undefined, width: undefined }} />
            <Card item={articles[3]} horizontal imageStyle={{ flex: 1, height: undefined, width: undefined }} imageContainerStyle={{ overflow: 'visible', flex: 1, height: undefined, width: undefined }} />
            <Card item={articles[4]} horizontal imageStyle={{ flex: 1, height: undefined, width: undefined }} imageContainerStyle={{ overflow: 'visible', flex: 1, height: undefined, width: undefined }} />

          </Block>
        </SafeAreaView>
        <ActionButton buttonColor="rgba(231,76,60,1)">
          <ActionButton.Item buttonColor='#9b59b6' title="New Task" onPress={() => navigation.navigate(ScreensTypes.CreateTask)}>
            <Icon name="md-create" style={styles.actionButtonIcon} />
          </ActionButton.Item>
          <ActionButton.Item buttonColor='#3498db' title="Aquire Points" onPress={() => navigation.navigate('QRReader')}>
            <Icon name="md-notifications-off" style={styles.actionButtonIcon} />
          </ActionButton.Item>
          <ActionButton.Item buttonColor='#1abc9c' title="All Tasks" onPress={() => { }}>
            <Icon name="md-done-all" style={styles.actionButtonIcon} />
          </ActionButton.Item>
        </ActionButton>
      </View>
    )
  }

  mappingForTransactions() {
    const mapTo = (response) => {
      if (!response.results)
        return { results: [] };
      const newResults = response.results.map(x => {
        let newObj =
        {
          title: x.points.toString() + ' KGS',
          horizontal: true,
          description: new Date(x.award_date).toLocaleString(),
          id: x.id.toString(),
        };
        return newObj;
      });

      return Object.assign(response, { results: newResults });
    };
    return mapTo;
  }

  mappingForTasks() {
    const mapTo = (response) => {
      if (!response || !response.results)
        return { results: [] };
      console.log(response);
      const newResults = response.results.map(x => {
        let newObj =
        {
          title: x.title,
          horizontal: true,
          description: x.description,
          id: x.id.toString(),
          points: x.points
        };
        return newObj;
      });

      return Object.assign(response, { results: newResults });
    };
    return mapTo;
  }

  getLastTransactions() {
    const { transactionService } = this.context.serviceProviders;
    return transactionService.getLast().pipe(map(this.mappingForTransactions())).subscribe((response) => {
      if (response && response.results)
        this.setState({
          isLoading: false,
          transactions: response.results
        });
    },
      e => {
        console.log('home error');
      });
  }

  getLastTasks() {
    const { taskService } = this.context.serviceProviders;
    return taskService.getAllFiltered().pipe(map(this.mappingForTasks())).subscribe((response) => {
      console.log(response);
      if (response && response.results)
        this.setState({
          isLoading: false,
          tasks: response.results
        });
    },
      e => {
        console.log('home error');
      });
  }
  componentDidMount() {
    const { authService, transactionService, taskService } = this.context.serviceProviders;

    // Load last transactions when component mounted
    this.subscriptions$.push(this.getLastTransactions());
    this.subscriptions$.push(this.getLastTasks());

    const updateEvents = merge(transactionService.updated, authService.signinComplete, taskService.updated);

    // Whenever there is a new transaction update the list
    this.subscriptions$.push(updateEvents.subscribe(() => {
      this.getLastTransactions();
      this.getLastTasks();
    },
      e => {
        console.log('home error');
      }));
  }

  componentWillUnmount() {
    this.subscriptions$.forEach(subscription => subscription.unsubscribe());
  }
  render() {
    return (
      <Block flex center style={styles.home}>
        {this.renderArticles()}
      </Block>
    );
  }
}

const styles = StyleSheet.create({
  home: {
    width: width,
  },
  articles: {
    width: width - theme.SIZES.BASE * 2,
    paddingVertical: theme.SIZES.BASE,
  },
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: 'white',
  },
});

export default Home;
