import Constants from "expo-constants";
import { Platform } from "react-native";

const localhost =
    Platform.OS === "ios" ? "http://192.168.88.199:8888/" : "http://192.168.88.199:8888/";

export interface EnvironmentVariables {
    url: string;
    webSocketUrl: string;
    envName: string
}
interface ENVIRONMENT {
    [key: string]: EnvironmentVariables;
}
const ENV: ENVIRONMENT = {
    dev: {
        url: localhost,
        webSocketUrl: 'ws://192.168.88.199:8888/chat/stream/',
        envName: 'dev'
    },
    staging: {
        url: "https://mirvel-base-app-api.herokuapp.com/",
        webSocketUrl: 'wss://mirvel-base-app-api.herokuapp.com/chat/stream/',
        envName: 'stg'
    },
    prod: {
        url: "https://mirvel-base-app-api.herokuapp.com/",
        webSocketUrl: 'wss://mirvel-base-app-api.herokuapp.com/chat/stream/',
        envName: 'prod'
    }
};

const getEnvVars = (): EnvironmentVariables => {
    const env = Constants.manifest.releaseChannel;
    // What is __DEV__ ?
    // This variable is set to true when react-native is running in Dev mode.
    // __DEV__ is true when run locally, but false when published.
    if (__DEV__) {
        return ENV.dev;
    } else if (env === 'staging') {
        return ENV.staging;
    } else if (env === 'prod') {
        return ENV.prod;
    }
};

export default getEnvVars;