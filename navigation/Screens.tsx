import {
  createDrawerNavigator, DrawerContentScrollView, DrawerItem
} from "@react-navigation/drawer";
import { NavigationContainer } from "@react-navigation/native";
import React from "react";
import { DevSettings, StyleSheet, View } from "react-native";
import { AppContext } from "../AppContext";
import { BaseComponent } from "../BaseComponent";
import Header from "../components/Header";
import IconHelper from "../components/IconHelper";
import { screenMappings } from "./ScreenMappings";
import { ScreensTypes } from "./ScreensTypes";

class DrawerContent extends BaseComponent {
  static contextType = AppContext;
  render() {
    const { drawerProps } = this.props;
    const { serviceProviders } = this.context;

    return (
      <DrawerContentScrollView {...this.props}>
        {Object.entries(screenMappings).filter(([screenType, configuration]) => configuration.isDrawerItem).map(([screenType, configuration]) => (
          <DrawerItem
            key={screenType}
            label={configuration.drawerLabel}
            onPress={() => {
              drawerProps.navigation.navigate(screenType);
            }}
            icon={() => <View style={styles.drawerIconContainer}>{IconHelper.getIconComponentByScreenName(screenType)}</View>}
          />
        ))}

        <DrawerItem
          label={serviceProviders.languageService.tags.SignOut}
          onPress={() => {
            serviceProviders.authService.logout().then(() => {
              // TODO: Temporary solution. Should be replaced with proper logout workflow
              DevSettings.reload();
            });

          }}
          icon={() => <View style={styles.drawerIconContainer}>{IconHelper.getIconComponentByScreenName(ScreensTypes.LogOut)}</View>}
        />
        <DrawerItem
          label={serviceProviders.languageService.tags.InvokeError}
          onPress={() => {
            throw new Error('Custom Error Requested');
          }}
          icon={() => <View style={styles.drawerIconContainer}>{IconHelper.getIconComponent(IconHelper.icons.Bomb)}</View>}
        />
      </DrawerContentScrollView>
    );
  }
}

const Drawer = createDrawerNavigator();

const MainDrawer = createDrawerNavigator();
//const AppContainer = createAppContainer(AppStack);
function AppContainer() {
  return (<NavigationContainer>
    <MainDrawer.Navigator
      drawerStyle={{ height: '90%', width: '75%' }}
      screenOptions={({ navigation, route }) => ({
        headerShown: true,
        headerStyle: { backgroundColor: 'green', flex: 1 },
        header: (scene) => {
          const headerParams = {
            navigation: {
              ...navigation,
              state: { routeName: route.name },
            },
            title: route.name,
            search: false,
            options: true
          }
          return (<Header {...headerParams} />);
        },
      })}

      drawerContent={(props) => <DrawerContent drawerProps={{ ...props }} />}>

      {Object.entries(screenMappings).map(([screenType, configuration]) => (
        <Drawer.Screen
          key={screenType}
          name={screenType}
          component={configuration.component}
          options={{
            headerShown: configuration.headerShown,
            drawerLabel: configuration.drawerLabel,
            drawerIcon: () => IconHelper.getIconComponentByScreenName(screenType)
          }}
        />
      ))}

    </MainDrawer.Navigator>
  </NavigationContainer>)

}
const styles = StyleSheet.create({
  drawerIconContainer: {
    width: 7
  },

});

export default AppContainer;
