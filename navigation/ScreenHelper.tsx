import {
    createStackNavigator
} from '@react-navigation/stack';
import React from "react";
import { Animated, Easing, Image, Text, TouchableHighlight, View } from "react-native";
import MyDrawerItem from "../components/DrawerItem";
import Header from "../components/Header";
import About from "../screens/About";
import AquirePoints from '../screens/AquirePoints';
import Login from "../screens/Login";
import QRReader from '../screens/QRReader';
import Register from "../screens/Register";
import CreateTask from "../screens/Task/CreateTask";
import CreateTeam from '../screens/Team/CreateTeam.component';
import EditTeam from "../screens/Team/EditTeam.component";
import Teams from '../screens/Team/Teams.component';
import { ScreensTypes } from "./ScreensTypes";

export const transitionConfig = (transitionProps, prevTransitionProps) => ({
    transitionSpec: {
        duration: 400,
        easing: Easing.out(Easing.poly(4)),
        timing: Animated.timing
    },
    screenInterpolator: sceneProps => {
        const { layout, position, scene } = sceneProps;
        const thisSceneIndex = scene.index;
        const width = layout.initWidth;

        const scale = position.interpolate({
            inputRange: [thisSceneIndex - 1, thisSceneIndex, thisSceneIndex + 1],
            outputRange: [4, 1, 1]
        });
        const opacity = position.interpolate({
            inputRange: [thisSceneIndex - 1, thisSceneIndex, thisSceneIndex + 1],
            outputRange: [0, 1, 1]
        });
        const translateX = position.interpolate({
            inputRange: [thisSceneIndex - 1, thisSceneIndex],
            outputRange: [width, 0]
        });

        const scaleWithOpacity = { opacity };
        const screenName = "Search";

        if (
            screenName === transitionProps.scene.route.routeName ||
            (prevTransitionProps &&
                screenName === prevTransitionProps.scene.route.routeName)
        ) {
            return scaleWithOpacity;
        }
        return { transform: [{ translateX }] };
    }
});

export const homeStacjObjSecondParam = {
    defaultNavigationOptions: {
        cardStyle: {
            backgroundColor: "#F8F9FE"
        },
    }
};


export const teamStackObjFirstParam = {
    Teams: {
        screen: Teams,
        navigationOptions: ({ navigation }) => ({
            header: () => <Header search options title="Teams" navigation={navigation} />
        })
    },
    EditTeam: {
        screen: EditTeam
    },
}
export const teamStackObjSecondParam =
{
    defaultNavigationOptions: {
        cardStyle: {
            backgroundColor: "#F8F9FE"
        },
    }
};

const HomeStack = createStackNavigator();

export const drawerNavigationRoutes = {};
drawerNavigationRoutes[ScreensTypes.Home] = {
    screen: HomeStack,
    navigationOptions: navOpt => ({
        drawerLabel: ({ focused }) => (
            <MyDrawerItem focused={focused} screen="Home" title="Главная" />
        )
    })
};
drawerNavigationRoutes[ScreensTypes.CreateTask] = {
    screen: CreateTask,
    navigationOptions: navOpt => ({
        drawerLabel: ({ focused }) => (
            <MyDrawerItem focused={focused} screen="Task" title="Создать лист покупок" />
        )
    })
};

drawerNavigationRoutes[ScreensTypes.Teams] = {
    screen: TeamStack,
    navigationOptions: navOpt => ({
        drawerLabel: ({ focused }) => (
            <MyDrawerItem focused={focused} screen="Teams" title="Teams" />
        )
    })
};
drawerNavigationRoutes[ScreensTypes.Team] = {
    screen: CreateTeam,
    navigationOptions: navOpt => ({
        drawerLabel: ({ focused }) => (
            <MyDrawerItem focused={focused} screen="Team" title="Create Team" />
        )
    })
},
    drawerNavigationRoutes[ScreensTypes.QRReader] = {
        screen: QRReader,
        navigationOptions: navOpt => ({
            drawerLabel: ({ focused }) => (
                <MyDrawerItem focused={focused} screen="QRReader" title="Сканировать QR код" />
            )
        })
    };
drawerNavigationRoutes[ScreensTypes.AquirePoints] = {
    screen: AquirePoints,
    navigationOptions: navOpt => ({
        drawerLabel: ({ focused }) => (
            <MyDrawerItem focused={focused} screen="AquirePoints" title="Принять Поинты" />
        )
    })
};
drawerNavigationRoutes[ScreensTypes.Login] = {
    screen: Login,
    params: { is_login: true },
    navigationOptions: navOpt => ({
        drawerLabel: ({ focused }) => (
            <MyDrawerItem focused={focused} screen="Login" title="Вход" />
        )
    })
};
drawerNavigationRoutes[ScreensTypes.Register] = {
    screen: Register,
    navigationOptions: navOpt => ({
        drawerLabel: ({ focused }) => (
            <MyDrawerItem focused={focused} screen="Register" title="Регистрация" />
        )
    })
};





export const TeamStackNavigator = createStackNavigator();
const teamScreens = {
    TeamsComponent: Teams,
    About: About
}
export function TeamStack() {
    return (
        <TeamStackNavigator.Navigator
            screenOptions={({ navigation, route }) => ({
                headerShown: true,
                headerTitle: (scene) => {
                    const headerParams = {
                        navigation: {
                            ...navigation,
                            state: { routeName: route.name },
                        },
                        title: route.name,
                        search: true,
                        options: true,
                        screens: teamScreens
                    }
                    return (<LogoTitle {...headerParams} />);
                },
            })}>
            {Object.entries({ ...teamScreens }).map(([name, component]) => (
                <TeamStackNavigator.Screen name={name} component={component} key={name}></TeamStackNavigator.Screen>
            ))}
        </TeamStackNavigator.Navigator>
    )
}


export function ImageLogo(navigation) {
    return (<TouchableHighlight
        onPress={() => navigation.navigate('About')}>
        <Image
            style={{ width: 50, height: 50, flex: 1, justifyContent: 'center', alignItems: 'center', borderWidth: 2, borderColor: 'red' }}
            source={require('../assets/imgs/icon.png')}
        />
    </TouchableHighlight>)
}

export function LogoTitle(props) {
    const { navigation, screens } = props;
    return (
        <View style={{ flex: 1, flexDirection: 'row' }}>
            {Object.entries({ ...screens }).map(([name, component]) => (
                <TouchableHighlight
                    onPress={() => navigation.navigate(name)} key={name}>
                    <Text style={{ padding: 20 }}>{name}</Text>
                </TouchableHighlight>
            ))}
        </View>
    );
}