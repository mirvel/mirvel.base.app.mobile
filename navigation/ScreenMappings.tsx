import Constants from "expo-constants";
import { serviceProviders } from "../AppProvider";
import About from "../screens/About";
import Account from "../screens/Account/Account";
import Accounts from "../screens/Account/Accounts";
import CreateAccount from "../screens/Account/CreateAccount";
import QuickSearchAccounts from "../screens/Account/QuickSearchAccounts";
import AquirePoints from '../screens/AquirePoints';
import Landing from "../screens/Landing";
import Login from "../screens/Login";
import Maps from "../screens/Maps";
import Points from "../screens/Points";
import CreateProject from "../screens/Project/CreateProject";
import Projects from "../screens/Project/Projects";
import Promotions from "../screens/Promotions";
import QRReader from '../screens/QRReader';
import Register from "../screens/Register";
import CreateTask from "../screens/Task/CreateTask";
import MyTasks from "../screens/Task/MyTasks";
import Tasks from "../screens/Task/Tasks";
import CreateTeam from "../screens/Team/CreateTeam.component";
import EditTeam from "../screens/Team/EditTeam.component";
import Team from "../screens/Team/Team.component";
import Teams from "../screens/Team/Teams.component";
import CreateTransaction from "../screens/Transaction/CreateTransaction";
import { ScreensTypes } from "./ScreensTypes";


export interface ScreenMappingInterface {
  [key: string]: {
    component: any;
    headerShown?: boolean;
    drawerLabel?: string;
    isDrawerItem?: boolean;
  };
}


const _lang = serviceProviders.languageService;
export const screenMappings: ScreenMappingInterface =
{

  [ScreensTypes.Landing]: { component: Landing, headerShown: true, },
  [ScreensTypes.Home]: { component: Promotions, headerShown: true, },
  [ScreensTypes.Tasks]: { component: Tasks, headerShown: true, },
  [ScreensTypes.Points]: { component: Points, headerShown: true, },
  [ScreensTypes.MyTasks]: { component: MyTasks, headerShown: true, drawerLabel: _lang.tags.MyTasks },
  [ScreensTypes.Maps]: { component: Maps, headerShown: true, },
  [ScreensTypes.CreateTask]: { component: CreateTask, headerShown: false, drawerLabel: _lang.tags.CreateTask },
  [ScreensTypes.CreateTransaction]: { component: CreateTransaction, headerShown: false, drawerLabel: _lang.tags.CreateTransaction },
  [ScreensTypes.Login]: { component: Login, headerShown: false, drawerLabel: _lang.tags.SignIn, isDrawerItem: true },
  [ScreensTypes.Team]: { component: Team, headerShown: false, drawerLabel: 'Team' },
  [ScreensTypes.EditTeam]: { component: EditTeam, headerShown: false, drawerLabel: 'Edit Team' },
  [ScreensTypes.CreateTeam]: { component: CreateTeam, headerShown: false, drawerLabel: 'Create Team' },
  [ScreensTypes.Teams]: { component: Teams, headerShown: false, drawerLabel: 'Teams' },
  [ScreensTypes.TeamDetails]: { component: Accounts, headerShown: false, drawerLabel: _lang.tags.Team },
  [ScreensTypes.CreateAccount]: { component: CreateAccount, headerShown: false, drawerLabel: _lang.tags.CreateAccount },
  [ScreensTypes.QRReader]: { component: QRReader, headerShown: false, drawerLabel: _lang.tags.Scan },
  [ScreensTypes.AquirePoints]: { component: AquirePoints, headerShown: false, drawerLabel: _lang.tags.AquirePoints },
  [ScreensTypes.Register]: { component: Register, headerShown: false, drawerLabel: _lang.tags.Register, isDrawerItem: true },
  [ScreensTypes.About]: { component: About, headerShown: false, drawerLabel: `About ${Constants.manifest.version}`, isDrawerItem: true },
  [ScreensTypes.Accounts]: { component: Accounts, headerShown: true, },
  [ScreensTypes.Account]: { component: Account, headerShown: true, drawerLabel: `Account` },
  [ScreensTypes.CreateProject]: { component: CreateProject, headerShown: false, drawerLabel: _lang.tags.CreateProject, isDrawerItem: true },
  [ScreensTypes.Projects]: { component: Projects, headerShown: true, drawerLabel: _lang.tags.Projects, isDrawerItem: true },
  [ScreensTypes.QuickSearchAccounts]: { component: QuickSearchAccounts, headerShown: false, drawerLabel: _lang.tags.SearchAccount, isDrawerItem: true },
}