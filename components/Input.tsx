import React from "react";
import { StyleSheet } from "react-native";
import Icon from './Icon';
import { argonTheme } from "../constants";
import { Input } from '../components/custom/src';


class ArInput extends React.Component<ArInputDefaultProps> {
  render() {
    const { shadowless, success, error, style } = this.props;

    const inputStyles = [
      styles.input,
      !shadowless && styles.shadow,
      success && styles.success,
      error && styles.error,
      { ...style }
    ];

    return (
      <Input
        placeholder="write something here"
        placeholderTextColor={argonTheme.COLORS.MUTED}
        style={inputStyles}
        styles={inputStyles}
        color={argonTheme.COLORS.HEADER}
        iconContent={
          <Icon
            size={14}
            color={argonTheme.COLORS.ICON}
            name="link"
            family="AntDesign"
          />
        }
        {...this.props}
      />
    );
  }
}

class ArInputDefaultProps {
  shadowless?: boolean = false;
  success?: boolean = false;
  error?: boolean = false;
  style?: any = {};
  [key: string]: any;
};

const styles = StyleSheet.create({
  input: {
    borderRadius: 4,
    borderColor: argonTheme.COLORS.BORDER,
    backgroundColor: '#FFFFFF',
    margin: 0,
    padding: 0,
  },
  success: {
    borderColor: argonTheme.COLORS.INPUT_SUCCESS,
  },
  error: {
    borderColor: argonTheme.COLORS.INPUT_ERROR,
  },
  shadow: {
    shadowColor: argonTheme.COLORS.BLACK,
    shadowOffset: { width: 0, height: 1 },
    shadowRadius: 2,
    shadowOpacity: 0.05,
    elevation: 2,
  }
});

export default ArInput;
