import React from "react";
import RNVIcons from 'react-native-vector-icons/FontAwesome';
import { Text } from '../components/custom/src';
import themes from "../constants/Theme";
import { ScreensTypes } from "../navigation/ScreensTypes";

class IconHelper {
    static icons = {
        Bomb: "bomb",
        User: "user",
        Info: "info"

    }

    // Screen Mappings to FontAwesome
    static screenIconMappings = {
        [ScreensTypes.Home]: "home",
        [ScreensTypes.Promotions]: "newspaper-o",
        [ScreensTypes.Points]: "diamond",
        [ScreensTypes.QRReader]: "qrcode",
        [ScreensTypes.Tasks]: "tasks",
        [ScreensTypes.MyTasks]: "list",
        [ScreensTypes.CreateTask]: "edit",
        [ScreensTypes.Maps]: "map-signs",
        [ScreensTypes.QRReader]: "qrcode",
        [ScreensTypes.AquirePoints]: "qrcode",
        [ScreensTypes.Login]: "sign-in",
        [ScreensTypes.LogOut]: "sign-out",
        [ScreensTypes.Register]: "pencil-square-o",
        [ScreensTypes.Teams]: "user",
        [ScreensTypes.TeamStack]: "user",
        [ScreensTypes.Info]: "home",
    }

    static getIconNameByScreenName(screenName: ScreensTypes): string {
        // Set default icon mapping
        const iconMapping = IconHelper.screenIconMappings;
        let iconName = iconMapping[screenName] || "info";
        return iconName;
    }
    static renderItem(iconName, style = undefined): React.ReactNode {
        return <Text><RNVIcons name={iconName} style={{ fontSize: 12, paddingRight: 8, width: 20, color: themes.COLORS.INFO, ...style }} /></Text>;
    }
    static getIconComponentByScreenName(screenName: string, style = undefined): React.ReactNode {
        return IconHelper.renderItem(IconHelper.getIconNameByScreenName(ScreensTypes[screenName]), style);
    };

    static getIconComponent(iconName: string, style = undefined): React.ReactNode {
        return IconHelper.renderItem(iconName, style);
    }
}

export default IconHelper;