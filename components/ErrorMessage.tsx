import React from 'react';
import { BaseComponent } from "../BaseComponent";
import { View, FlatList, Text } from "react-native";

export class ErrorMessage extends BaseComponent {
    render() {
        const { errorResponse, style } = this.props;
        return (
            <View style={{ ...styles.container, ...style }}>
                <FlatList
                    data={this.errorToList(errorResponse)}
                    renderItem={({ item }: { item: any }) => <Text style={styles.errorText}>{item.field} - {item.message}</Text>}
                    keyExtractor={(item, index) => index.toString()}
                />
            </View>
        )
    }

    errorToList(responseError: ResponseError) {
        if (!responseError || !responseError.error)
            return [];
        const errorObject = responseError.error;
        const listErrors = [];
        for (const key in errorObject) {
            if (errorObject.hasOwnProperty(key)) {
                const element = errorObject[key];
                listErrors.push({ field: key, message: typeof element === 'string' || Array.isArray(element) ? element : typeof element });
            }
        }
        console.log(listErrors);
        return listErrors;
    }

}

const styles = {
    container: {
        flex: 1
    },
    errorText: {
        color: 'red'
    }
}

export class ResponseError {
    errorStatus: string;
    error: any;
}

export default ErrorMessage;