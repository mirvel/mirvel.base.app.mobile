
import React from "react";
import {
    Dimensions, ImageBackground, StatusBar, StyleSheet
} from "react-native";
import { BaseComponent } from "../BaseComponent";
import { argonTheme, Images } from "../constants";
import { Block } from "./custom/src";

const { width, height } = Dimensions.get("screen");

class Dialog extends BaseComponent {
    render() {
        const { children, style } = this.props;
        return (
            <Block flex middle style={{ ...styles.dialogContainer }}>
                <StatusBar hidden />
                <Block flex middle>
                    <Block style={{ ...styles.registerContainer, ...style }}>
                        {children}
                    </Block>
                </Block>
            </Block>

        );
    }
}

const styles = StyleSheet.create({
    dialogContainer: {
        backgroundColor: argonTheme.COLORS.SECONDARY
    },
    registerContainer: {
        width: width * 0.9,
        height: height * 0.78,
        backgroundColor: argonTheme.COLORS.DIALOG_BACKGROUND,
        borderRadius: 4,
        shadowColor: argonTheme.COLORS.BLACK,
        shadowOffset: {
            width: 0,
            height: 4
        },
        shadowRadius: 8,
        shadowOpacity: 0.1,
        elevation: 1,
        overflow: "hidden",
        flexDirection: 'column',
        alignItems:'center'
    },

});

export default Dialog;
