import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, Dimensions, Image, TouchableWithoutFeedback } from 'react-native';
import { Block, Text, theme } from '../components/custom/src';

import { argonTheme } from '../constants';
import { BaseComponent } from "../BaseComponent";


class CardTask extends BaseComponent {
  static propTypes: any;
  render() {
    const { navigation, item, horizontal, full, } = this.props;
    const { languageService } = this.context.serviceProviders;
    const imageStyles = [
      full ? styles.fullImage : styles.horizontalImage
    ];
    const pointsStyle = { fontSize: 14, alignSelf: 'flex-end', position: 'absolute', zIndex: 1, right: 10, top: 5, padding: 3, minWidth: 60, textAlign: 'center' };
    const assignedToStyle = { fontSize: 14, alignSelf: 'flex-end' };
    const titleStyle = { fontWeight: 'bold', color: 'black', fontSize: 16, maxWidth: 200, alignSelf: 'flex-start', };
    const imageContainerStyle = { display: 'none' };
    const style = { minHeight: 40, marginBottom: 2, marginVertical: 2 };
    const contentContainerStyle = { alignItems: 'flex-start', justifyContent: 'center' };
    const descriptionStyle = { marginLeft: 50 };
    const dateStyle = {};
    const cardContainer = [styles.card, styles.shadow, style];
    const contentContainer = [styles.cardDescription, contentContainerStyle]
    const cardTitle = [styles.cardTitle, titleStyle]
    const cardPoints = [styles.cardTitle, pointsStyle]
    const cardDescription = [descriptionStyle]
    const cardDate = [dateStyle]
    const imgContainer = [styles.imageContainer,
    horizontal ? styles.horizontalStyles : styles.verticalStyles,
    styles.shadow, imageContainerStyle
    ];
    const imageSource = item.image ? { uri: item.image } : item.imageSource;

    return (
      <Block row={horizontal} card flex style={cardContainer}>
        <TouchableWithoutFeedback onPress={() => { console.log('touch'); this.props.onCustomClick && this.props.onCustomClick() }}>
          <Block flex style={imgContainer}>
            <Image source={imageSource} style={imageStyles} />
          </Block>
        </TouchableWithoutFeedback>
        <TouchableWithoutFeedback onPress={() => { console.log('touch'); this.props.onCustomClick && this.props.onCustomClick() }}>
          <Block flex space="between" style={contentContainer}>
            <Text size={14} style={cardTitle}>{item.title}</Text>
            <Text size={14}>({item.state})</Text>
            <Text size={14} style={cardPoints}>{item.points ? item.points.toString() : ''}</Text>
            <Text size={14} style={cardDescription}>{item.description}</Text>
            <Text size={14} style={cardDate}>{item.scheduled_date ? new Date(item.scheduled_date).toLocaleDateString(languageService.language) : ''}</Text>
            <Text size={14} style={assignedToStyle}>{item.assigned_to_user ? item.assigned_to_user : ''}</Text>
          </Block>
        </TouchableWithoutFeedback>
      </Block>
    );
  }
}

CardTask.propTypes = {
  item: PropTypes.object,
  horizontal: PropTypes.bool,
  full: PropTypes.bool,
  ctaColor: PropTypes.string,
  imageStyle: PropTypes.any,
}

const styles = StyleSheet.create({
  card: {
    backgroundColor: theme.COLORS.WHITE,
    marginVertical: theme.SIZES.BASE,
    borderWidth: 0,
    minHeight: 114,
    marginBottom: 16
  },
  cardTitle: {
    flex: 1,
    flexWrap: 'wrap',
    paddingBottom: 6,
  },
  cardDescription: {
    padding: theme.SIZES.BASE / 2
  },
  imageContainer: {
    borderRadius: 3,
    elevation: 1,
    overflow: 'hidden',
    backgroundColor: 'white',

  },
  image: {
    // borderRadius: 3,
  },
  horizontalImage: {
    height: 40,
    width: 40,
  },
  horizontalStyles: {
    borderTopRightRadius: 0,
    borderBottomRightRadius: 0,
  },
  verticalStyles: {
    borderBottomRightRadius: 0,
    borderBottomLeftRadius: 0
  },
  fullImage: {
    height: 215
  },
  shadow: {
    shadowColor: theme.COLORS.BLACK,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 4,
    shadowOpacity: 0.1,
    elevation: 2,
  },
});

export default CardTask;