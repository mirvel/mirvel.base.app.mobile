import React from 'react';
import { Dimensions, Platform, StyleSheet, TouchableOpacity } from 'react-native';
import RNVIcons from 'react-native-vector-icons/FontAwesome';
import { merge } from 'rxjs';
import { AppContext } from '../AppContext';
import { BaseComponent } from "../BaseComponent";
import { Block, Button, NavBar, Text, theme } from '../components/custom/src';
import argonTheme from '../constants/Theme';
import { ScreensTypes } from '../navigation/ScreensTypes';
import Icon from './Icon';
import Input from './Input';
import Tabs from './Tabs';


const { height, width } = Dimensions.get('window');
const iPhoneX = () => Platform.OS === 'ios' && (height === 812 || width === 812 || height === 896 || width === 896);

const BellButton = ({ isWhite, style, navigation }) => (
  <TouchableOpacity style={[styles.button, style]} onPress={() => navigation.navigate(ScreensTypes.Default)}>
    <Icon
      family="ArgonExtra"
      size={16}
      name="bell"
      color={argonTheme.COLORS[isWhite ? 'WHITE' : 'ICON']}
    />
    <Block middle style={styles.notify} />
  </TouchableOpacity>
);

const BasketButton = ({ isWhite, style, navigation }) => (
  <TouchableOpacity style={[styles.button, style]} onPress={() => navigation.navigate(ScreensTypes.Default)}>
    <Icon
      family="ArgonExtra"
      size={16}
      name="basket"
      color={argonTheme.COLORS[isWhite ? 'WHITE' : 'ICON']}
    />
  </TouchableOpacity>
);

const ProfileButton = ({ isWhite, style, navigation }) => (
  <TouchableOpacity style={[styles.button, style]} onPress={() => navigation.navigate(ScreensTypes.Default)}>
    <Icon
      family="ArgonExtra"
      size={16}
      name="diamond"
      color={argonTheme.COLORS[isWhite ? 'WHITE' : 'ICON']}
    />
  </TouchableOpacity>
);

const SearchButton = ({ isWhite, style, navigation }) => (
  <TouchableOpacity style={[styles.button, style]} onPress={() => navigation.navigate(ScreensTypes.Default)}>
    <Icon
      size={16}
      family="Galio"
      name="search-zoom-in"
      color={theme.COLORS[isWhite ? 'WHITE' : 'ICON']}
    />
  </TouchableOpacity>
);

class Header extends BaseComponent {
  static contextType = AppContext;

  state = {
    totalPoints: undefined,
    currentUser: undefined
  }

  componentDidMount() {
    const { authService, transactionService } = this.context.serviceProviders;
    authService.currentUser.subscribe((currentUser) => {
      this.setState({ currentUser });
    });
    transactionService.summary().subscribe((response) => {
      console.log(response);
      if (response && response.summary)
        this.setState({ totalPoints: response.summary.amount });
    },
      e => {
        console.log('home error');
      });
    const updateEvents = merge(transactionService.updated, authService.signinComplete);

    updateEvents.subscribe(() => {
      console.log('updated');
      transactionService.summary().subscribe((response) => {
        console.log(response);
        if (response && response.summary)
          this.setState({ totalPoints: response.summary.amount });
      });
    },
      e => {
        console.log('home error');
      });

  }
  handleLeftPress = () => {
    const { back, navigation } = this.props;
    return (back ? navigation.goBack() : navigation.openDrawer());
  }
  renderRight = () => {
    const { white, title, navigation } = this.props;
    const { routeName } = navigation.state;
    const { env } = this.context.serviceProviders;
    const rightIcons = () => {
      if (title === 'Title') {
        return [
          <BellButton key='chat-title' navigation={navigation} isWhite={white} style />,
          <BasketButton key='basket-title' navigation={navigation} isWhite={white} style />
        ]
      }

      switch (routeName) {
        case 'Home':
          return ([
            <BellButton key='chat-home' navigation={navigation} isWhite={white} style />,
            <BasketButton key='basket-home' navigation={navigation} isWhite={white} style />,
          ]);
        case 'Promotions':
          return ([
            <BellButton key='chat-home' navigation={navigation} isWhite={white} style />,
            <BasketButton key='basket-home' navigation={navigation} isWhite={white} style />,
          ]);
        case 'Maps':
          return ([
            <BellButton key='chat-home' navigation={navigation} isWhite={white} style />,
            <BasketButton key='basket-home' navigation={navigation} isWhite={white} style />,
          ]);
        case 'Points':
          return ([
            <BellButton key='chat-home' navigation={navigation} isWhite={white} style />,
            <BasketButton key='basket-home' navigation={navigation} isWhite={white} style />,
          ]);
        case 'MyTasks':
          return ([
            <BellButton key='chat-home' navigation={navigation} isWhite={white} style />,
            <BasketButton key='basket-home' navigation={navigation} isWhite={white} style />,
          ]);
        case 'Tasks':
          return ([
            <BellButton key='chat-home' navigation={navigation} isWhite={white} style />,
            <BasketButton key='basket-home' navigation={navigation} isWhite={white} style />,
          ]);
        case 'Task':
          return ([
            <BellButton key='chat-home' navigation={navigation} isWhite={white} style />,
            <BasketButton key='basket-home' navigation={navigation} isWhite={white} style />,
          ]);
        case 'Deals':
          return ([
            <BellButton key='chat-categories' navigation={navigation} isWhite={white} style />,
            <BasketButton key='basket-categories' navigation={navigation} isWhite={white} style />
          ]);
        case 'Categories':
          return ([
            <BellButton key='chat-categories' navigation={navigation} isWhite={white} style />,
            <BasketButton key='basket-categories' navigation={navigation} isWhite={white} style />,
          ]);
        case 'Category':
          return ([
            <BellButton key='chat-deals' navigation={navigation} isWhite={white} style />,
            <BasketButton key='basket-deals' navigation={navigation} isWhite={white} style />,
          ]);
        case 'Profile':
          return ([
            <BellButton key='chat-profile' navigation={navigation} isWhite={white} style />,
            <BasketButton key='basket-deals' navigation={navigation} isWhite={white} style />,
          ]);
        case 'Product':
          return ([
            <SearchButton key='search-product' navigation={navigation} isWhite={white} style />,
            <BasketButton key='basket-product' navigation={navigation} isWhite={white} style />,
          ]);
        case 'Search':
          return ([
            <BellButton key='chat-search' navigation={navigation} isWhite={white} style />,
            <BasketButton key='basket-search' navigation={navigation} isWhite={white} style />,
          ]);
        case 'Settings':
          return ([
            <BellButton key='chat-search' navigation={navigation} isWhite={white} style />,
            <BasketButton key='basket-search' navigation={navigation} isWhite={white} style />,
          ]);
        case 'Teams':
          return ([
            <BellButton key='chat-search' navigation={navigation} isWhite={white} style />,
            <BasketButton key='basket-search' navigation={navigation} isWhite={white} style />,
          ]);
        default:
          return ([]);
          break;
      }
    }

    return ([
      <Block key="environment-info"><Text >{env && env.envName}</Text></Block>,
      <Block key='profile-summary' style={{ paddingLeft: 3 }}>
        {this.state.totalPoints && <Text style={{ color: 'orange', fontWeight: 'bold' }}>{this.state.totalPoints}</Text>}
      </Block>,
      ...rightIcons(),
      <Block key='profile-button'>
        {this.state.currentUser && <ProfileButton navigation={navigation} isWhite={white} style />}
      </Block>,
      <Block key='profile-name'>
        {this.state.currentUser && <Text>{this.state.currentUser.username}</Text>}
      </Block>

    ]);

  }
  renderSearch = () => {
    const { navigation } = this.props;
    return (
      <Input
        right
        color="black"
        style={styles.search}
        placeholder="Что вы ищете?"
        placeholderTextColor={'#8898AA'}
        onFocus={() => navigation.navigate(ScreensTypes.Default)}
        iconContent={<Icon size={16} color={theme.COLORS.MUTED} name="search-zoom-in" family="ArgonExtra" />}
      />
    );
  }
  renderOptions = () => {
    const { navigation, optionLeft, optionRight } = this.props;
    const { languageService } = this.context.serviceProviders;

    return (
      <Block row style={styles.options}>
        <Button shadowless style={[styles.tab, styles.divider]} onPress={() => navigation.navigate(ScreensTypes.Tasks)}>
          <Block row middle>
            <Text style={{ paddingRight: 8 }}><RNVIcons name="newspaper-o" size={16} style={styles.tabIconColor} /></Text>
            <Text size={16} style={styles.tabTitle}>{optionLeft || languageService.tags.Tasks}</Text>
          </Block>
        </Button>
        <Button shadowless style={[styles.tab, styles.divider]} onPress={() => navigation.navigate(ScreensTypes.Points)}>
          <Block row middle>
            <Text style={{ paddingRight: 8 }}><RNVIcons name="diamond" size={16} style={styles.tabIconColor} /></Text>
            <Text size={16} style={styles.tabTitle}>{optionLeft || languageService.tags.Points}</Text>
          </Block>
        </Button>
        <Button shadowless style={styles.tab} onPress={() => navigation.navigate(ScreensTypes.Accounts)}>
          <Block row middle>
            <Text style={{ paddingRight: 8 }}><RNVIcons name="users" size={16} style={styles.tabIconColor} /></Text>
            <Text size={16} style={styles.tabTitle}>{optionRight || languageService.tags.Accounts}</Text>
          </Block>
        </Button>
      </Block>
    );
  }
  renderTabs = () => {
    const { tabs, tabIndex, navigation } = this.props;
    const defaultTab = tabs && tabs[0] && tabs[0].id;

    if (!tabs) return null;

    return (
      <Tabs
        data={tabs || []}
        initialIndex={tabIndex || defaultTab}
        onChange={id => navigation.setParams({ tabId: id })} />
    )
  }
  renderHeader = () => {
    const { search, options, tabs } = this.props;
    if (search || tabs || options) {
      return (
        <Block center>
          {search ? this.renderSearch() : null}
          {options ? this.renderOptions() : null}
          {tabs ? this.renderTabs() : null}
        </Block>
      );
    }
  }
  render() {
    const { back, title, white, transparent, bgColor, iconColor, titleColor, navigation, ...props } = this.props;
    const { routeName } = navigation.state;
    const noShadow = ['Search', 'Categories', 'Deals', ScreensTypes.Default, 'Profile'].includes(routeName);
    const headerStyles = [
      !noShadow ? styles.shadow : null,
      transparent ? { backgroundColor: 'rgba(0,0,0,0)' } : null,
    ];

    const navbarStyles = [
      styles.navbar,
      bgColor && { backgroundColor: bgColor },
    ];

    return (
      <Block style={headerStyles}>
        <NavBar
          back={back}
          title={title}
          style={navbarStyles}
          transparent={transparent}
          right={this.renderRight()}
          rightStyle={{ flexDirection: 'row', justifyContent: 'flex-end' }}
          left={
            <TouchableOpacity
              onPress={this.handleLeftPress} style={{ flexDirection: 'row', justifyContent: 'center' }}>
              <Icon
                style={{ padding: 15 }}
                name={back ? 'nav-left' : "menu-8"} family="ArgonExtra"
                size={14}
                color={iconColor || argonTheme.COLORS.ICON} />
            </TouchableOpacity>
          }
          leftStyle={{ paddingVertical: 4, flex: 0.5, marginLeft: 2 }}
          titleStyle={[
            styles.title,
            { color: argonTheme.COLORS[white ? 'WHITE' : 'HEADER'] },
            titleColor && { color: titleColor }
          ]}
          {...props}
        />
        {this.renderHeader()}
      </Block>
    );
  }
}

const styles = StyleSheet.create({
  button: {
    padding: 12,
    position: 'relative',
  },
  title: {
    width: '100%',
    fontSize: 16,
    fontWeight: 'bold',
  },
  navbar: {
    paddingVertical: 0,
    paddingBottom: theme.SIZES.BASE * 1.5,
    paddingTop: iPhoneX ? theme.SIZES.BASE * 4 : theme.SIZES.BASE - 20,
    zIndex: 5,
    backgroundColor: '#33CC99',
  },
  shadow: {
    backgroundColor: theme.COLORS.WHITE,
    shadowColor: 'black',
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 6,
    shadowOpacity: 0.2,
    elevation: 3,
  },
  notify: {
    backgroundColor: argonTheme.COLORS.LABEL,
    borderRadius: 4,
    height: theme.SIZES.BASE / 2,
    width: theme.SIZES.BASE / 2,
    position: 'absolute',
    top: 9,
    right: 12,
  },
  header: {
    backgroundColor: '#33CC99',
  },
  divider: {
    borderRightWidth: 0.3,
    borderRightColor: theme.COLORS.ICON,
  },
  search: {
    height: 48,
    width: width - 32,
    marginHorizontal: 16,
    borderWidth: 1,
    borderRadius: 3,
    borderColor: argonTheme.COLORS.BORDER
  },
  options: {
    elevation: 4,
    backgroundColor: '#003333',
    display: 'flex'
  },
  tab: {
    backgroundColor: theme.COLORS.TRANSPARENT,
    borderRadius: 0,
    borderWidth: 0,
    height: 24,
    elevation: 0,
    flex: 1
  },
  tabTitle: {
    lineHeight: 19,
    fontWeight: '400',
    color: '#FFFFFF'
  },
  tabIconColor: {
    color: '#FFFFFF'
  }
});

export default Header;
