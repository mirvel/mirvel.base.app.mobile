import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, Dimensions, Image, TouchableWithoutFeedback } from 'react-native';
import { Block, Text, theme } from '../components/custom/src';

import { argonTheme } from '../constants';
import { BaseComponent } from "../BaseComponent";
import { AppContext } from '../AppContext';


class Card extends BaseComponent {
  static contextType = AppContext;

  static propTypes: any;
  render() {
    const { navigation, item, horizontal, full, style, ctaColor, imageStyle, imageContainerStyle, contentContainerStyle, titleStyle, pointsStyle, descriptionStyle } = this.props;

    const imageStyles = [
      full ? styles.fullImage : styles.horizontalImage,
      imageStyle
    ];
    const cardContainer = [styles.card, styles.shadow, style];
    const contentContainer = [styles.cardDescription, contentContainerStyle]
    const cardTitle = [styles.cardTitle, titleStyle]
    const cardPoints = [styles.cardTitle, pointsStyle]
    const cardDescription = [descriptionStyle]
    const imgContainer = [styles.imageContainer,
    horizontal ? styles.horizontalStyles : styles.verticalStyles,
    styles.shadow, imageContainerStyle
    ];
    const imageSource = item.image ? { uri: item.image } : item.imageSource;

    return (
      <Block row={horizontal} card flex style={cardContainer}>
        <TouchableWithoutFeedback onPress={() => { console.log('touch'); this.props.onCustomClick && this.props.onCustomClick() }}>
          <Block flex style={imgContainer}>
            <Image source={imageSource} style={imageStyles} />
          </Block>
        </TouchableWithoutFeedback>
        <TouchableWithoutFeedback onPress={() => { console.log('touch'); this.props.onCustomClick && this.props.onCustomClick() }}>
          <Block flex space="between" style={contentContainer}>
            <Text size={14} style={cardTitle}>{item.title}</Text>
            <Text size={14} style={cardPoints}>{item.points}</Text>
            <Text size={14} style={cardDescription}>{item.description}</Text>
            <Text size={12} muted={!ctaColor} color={ctaColor || argonTheme.COLORS.ACTIVE} bold>{item.cta}</Text>
          </Block>
        </TouchableWithoutFeedback>
      </Block>
    );
  }
}

Card.propTypes = {
  item: PropTypes.object,
  horizontal: PropTypes.bool,
  full: PropTypes.bool,
  ctaColor: PropTypes.string,
  imageStyle: PropTypes.any,
}

const styles = StyleSheet.create({
  card: {
    backgroundColor: theme.COLORS.WHITE,
    marginVertical: theme.SIZES.BASE,
    borderWidth: 0,
    minHeight: 114,
    marginBottom: 16
  },
  cardTitle: {
    flex: 1,
    flexWrap: 'wrap',
    paddingBottom: 6,
  },
  cardDescription: {
    padding: theme.SIZES.BASE / 2
  },
  imageContainer: {
    borderRadius: 3,
    elevation: 1,
    overflow: 'hidden',
    backgroundColor: 'white',

  },
  image: {
    // borderRadius: 3,
  },
  horizontalImage: {
    height: 40,
    width: 40,
  },
  horizontalStyles: {
    borderTopRightRadius: 0,
    borderBottomRightRadius: 0,
  },
  verticalStyles: {
    borderBottomRightRadius: 0,
    borderBottomLeftRadius: 0
  },
  fullImage: {
    height: 215
  },
  shadow: {
    shadowColor: theme.COLORS.BLACK,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 4,
    shadowOpacity: 0.1,
    elevation: 2,
  },
});

export default Card;