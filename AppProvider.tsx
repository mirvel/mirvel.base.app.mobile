import React from 'react';
import { Subject } from 'rxjs';
import { AppContext } from './AppContext';
import getEnvVars, { EnvironmentVariables } from './environment';
import { AccountService } from './services/AccountService';
import { AudioService } from './services/AudioService';
import { AuthService } from './services/AuthService';
import { DateTimeService } from './services/DateTimeService';
import { LanguageService } from './services/LanguageService';
import { WebSocketWrapprer } from './services/models/WebSocketWrapper';
import { NotificationService } from './services/NotificationService';
import { PerformanceAwardsService } from './services/PerformanceAwardsService';
import { ProjectService } from './services/ProjectService';
import { QrService } from './services/QrService';
import { RequestService } from './services/RequestService';
import { StorageService } from './services/StorageService';
import { TaskService } from './services/TaskService';
import { TeamMembershipService } from './services/TeamMembershipService';
import { TeamService } from './services/TeamService';
import { TransactionService } from './services/TransactionService';
import { UserService } from './services/UserService';
import { WebSocketService } from './services/WebSocketService';



export class ServiceProviders {
    env: EnvironmentVariables = getEnvVars();
    storageService = new StorageService();
    audioService = new AudioService();
    requestService = new RequestService(this);
    notificationService = new NotificationService(this);
    webSocket = new WebSocketWrapprer(WebSocket);
    webSocketService = new WebSocketService(this);
    authService = new AuthService(this);
    userService = new UserService(this);
    dateTimeService = new DateTimeService(this);
    accountService = new AccountService(this);
    taskService = new TaskService(this);
    transactionService = new TransactionService(this);
    performanceAwardsService = new PerformanceAwardsService(this);
    qrService = new QrService(this);
    languageService = new LanguageService();
    teamService = new TeamService(this);
    teamMembershipService = new TeamMembershipService(this);
    projectService = new ProjectService(this);

    appStateSubject = new Subject();
    appStateUpdated = this.appStateSubject.asObservable();
    updateAppState(state) {
        this.appStateSubject.next(state);
    }
}
export const serviceProviders = new ServiceProviders();


export class AppProvider extends React.Component<any, any> {
    constructor(props) {
        super(props);
        this.state = {
            svc: { serviceProviders: serviceProviders },
        };
    }

    componentDidMount() {
        const { authService, webSocketService } = serviceProviders;
        authService.syncCurrentUser();
        webSocketService.establishConnection();
    }
    componentWillUnmount() {
        const { notificationService } = serviceProviders;
        notificationService.unregisterNotification();
    }
    render() {
        const { children } = this.props;
        return (
            <AppContext.Provider value={this.state.svc}>
                {children}
            </AppContext.Provider >
        );
    }
}

export const AppConsumer = AppContext.Consumer;
