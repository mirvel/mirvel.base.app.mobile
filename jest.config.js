module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  testMatch: [
    "**/testing/jest/*.test.ts"
  ],

};
