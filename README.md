### Installation  

* Install nodejs https://nodejs.org/en/
* Run
    ```bash
    npm install --global expo-cli
    npm install
    nvm install 10.13.0
    ```


### Publishing to expo
```bash
npm run build
```


### Unit testing
```bash
npm run test
```