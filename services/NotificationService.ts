import { Subject } from "rxjs";
import { ServiceProviders } from "../AppProvider";
import { PushNotificationTokenModel } from "./models/PushNotificationTokenModel";
import Constants from 'expo-constants';
import * as Notifications from 'expo-notifications'
import { Platform, Vibration } from 'react-native';
import * as Permissions from 'expo-permissions';

export class NotificationService {
    expoPushToken;
    notificationReceived = new Subject<any>();
    requestService;
    eventSubscription;
    constructor({ requestService }: ServiceProviders) {
        this.requestService = requestService;
    }
    registerForPushNotificationsAsync = async () => {
        try {
            if (Constants.isDevice) {
                const { status: existingStatus } = await Permissions.getAsync(Permissions.NOTIFICATIONS);
                let finalStatus = existingStatus;
                if (existingStatus !== 'granted') {
                    const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
                    finalStatus = status;
                }
                if (finalStatus !== 'granted') {
                    alert('Failed to get push token for push notification!');
                    return;
                }

                Notifications.getExpoPushTokenAsync().then(
                    (token) => {
                        this.setToken(token);
                        this.eventSubscription = Notifications.setNotificationHandler(this._handleNotification);
                    }
                    , (error) => {
                        alert(error);
                    })

            } else {
                alert('Must use physical device for Push Notifications');
            }

            if (Platform.OS === 'android') {
                Notifications.setNotificationChannelAsync('default', {
                    name: 'default',
                    importance: Notifications.AndroidImportance.MAX,
                    vibrationPattern: [0, 250, 250, 250],
                    lightColor: '#FF231F7C',
                  });
            }
        } catch (registerForPushNotificationsAsync) {
            console.log({ registerForPushNotificationsAsync })
        }

    };

    unregisterNotification() {
        if (this.eventSubscription && this.eventSubscription.remove)
            this.eventSubscription.remove();
    }
    _handleNotification = {
        handleNotification: async () => ({
            shouldShowAlert: true,
            shouldPlaySound: true,
            shouldSetBadge: true,
        }),
    };
    dispatch(dispatchedData) {
        console.log({ dispatchedData })
        this.notificationReceived.next(dispatchedData);
    }
    setToken(expoPushToken) {
        console.log({ expoPushToken });
        this.expoPushToken = expoPushToken;
        this.savePushNotificationToken(new PushNotificationTokenModel(expoPushToken, undefined)).subscribe();
    }
    // Can use this function below, OR use Expo's Push Notification Tool-> https://expo.io/dashboard/notifications
    sendPushNotification(title, body, data) {
        console.log('sendPushNotification');
        const message = {
            to: this.expoPushToken,
            sound: 'default',
            title: title,
            body: body,
            data: { data: data },
            _displayInForeground: true,
        };
        console.log(message);
        fetch('https://exp.host/--/api/v2/push/send', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Accept-encoding': 'gzip, deflate',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(message),
        }).then((pushMessageSent) => { console.log(JSON.stringify({ pushMessageSent })); }, (errorMessageReceived) => { alert(JSON.stringify({ errorMessageReceived })) })
    };

    savePushNotificationToken(data: PushNotificationTokenModel) {
        return this.requestService.postApi('push-notification-token/', data);
    }
}