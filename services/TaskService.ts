import { TaskModel } from "./models/Task";
import { RequestService } from "./RequestService";
import { tap } from "rxjs/operators";
import { Subject, Observable } from "rxjs";
import { ApiResponse } from "./models/ApiResponse";
import { ServiceProviders } from "../AppProvider";

export class TaskService {
    requestService: RequestService;
    updated = new Subject();
    constructor({ requestService, webSocketService, audioService }: ServiceProviders) {
        this.requestService = requestService;

        webSocketService.modelUpdated.subscribe((modelUpdateMessage) => {
            if (modelUpdateMessage.class_name == 'Task') {
                this.updated.next(modelUpdateMessage);
                if (modelUpdateMessage.created)
                    audioService.playSound();
            }
        })
    }

    createTask(data: TaskModel) {
        return this.requestService.postApi('task/', data).pipe(tap(() => { this.updated.next(); }));
    }
    createOrUpdateTask(data: TaskModel) {
        if (data.id)
            return this.requestService.putApi(`task/${data.id}/`, data).pipe(tap(() => { this.updated.next(); }));
        else
            return this.requestService.postApi('task/', data).pipe(tap(() => { this.updated.next(); }));
    }

    getTask(id: number): Observable<TaskModel> {
        return this.requestService.getApi(`task/${id}/`);
    }
    getLast(limit: number, offset: number = 0): Observable<ApiResponse<TaskModel[]>> {
        return this.requestService.getApi(`task/?limit=${limit}&offset=${offset}&ordering=-id`);
    }

    getAll(): Observable<TaskModel[]> {
        return this.requestService.getApi(`task/`);
    }

    getAllFiltered(): Observable<TaskModel[]> {
        return this.requestService.getApi(`tasks-filtered/`);
    }

    getAllAssignedToMe(): Observable<TaskModel[]> {
        return this.requestService.getApi(`tasks-assigned-to-me/`);
    }
}