import { RequestService } from "./RequestService";
import { tap } from "rxjs/operators";
import { Subject, Observable } from "rxjs";
import { TeamModel } from "./models/TeamModel";
import { ApiResponse } from "./models/ApiResponse";

export class TeamService {
    requestService: RequestService;
    updated = new Subject();
    constructor({ requestService }) {
        this.requestService = requestService;
    }

    create(data: TeamModel) {
        return this.requestService.postApi('team/', data).pipe(tap(() => { this.updated.next(); }));
    }

    get(id: number): Observable<TeamModel> {
        return this.requestService.getApi(`team/${id}/`);
    }

    getAll(): Observable<TeamModel[]> {
        return this.requestService.getApi(`team/`);
    }

    getLast(limit: number, offset: number = 0): Observable<ApiResponse<TeamModel[]>> {
        return this.requestService.getApi(`team/?limit=${limit}&offset=${offset}&ordering=-id`);
    }

    update(id: number, data: TeamModel): Observable<TeamModel> {
        return this.requestService.putApi(`team/${id}/`, data).pipe(tap(() => { this.updated.next(); }));
    }
}