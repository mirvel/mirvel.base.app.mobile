
export class RegisterService {
    requestService;
    constructor({ requestService }) {
        this.requestService = requestService;
    }

    register(data) {
        const headers = { 'Accept': '*/*' };
        return this.requestService.postApi('register/', data, headers, true, '');
    }
}