import { BehaviorSubject, Observable, of, Subject, throwError } from 'rxjs';
import { switchMap, tap } from 'rxjs/operators';
import { ServiceProviders } from '../AppProvider';
import { RegisterUser } from './models/RegisterUser';
import { NotificationService } from './NotificationService';
import { RequestService } from './RequestService';
import { StorageService } from './StorageService';
import { WebSocketService } from './WebSocketService';

export class AuthService {
    syncTime = 10000; //milliseconds
    requestService: RequestService;
    storageService: StorageService;
    webSocketService: WebSocketService;
    app;
    currentUser = new BehaviorSubject(undefined);
    signinComplete = new Subject();
    notificationService: NotificationService;

    constructor({ requestService, storageService, notificationService, webSocketService }: ServiceProviders) {
        console.log('AuthService created');
        this.requestService = requestService;
        this.storageService = storageService;
        this.notificationService = notificationService;
        this.webSocketService = webSocketService;
    }

    setCurrentUser(user) {
        this.currentUser.next(user);
    }

    syncCurrentUser(): void {
        const success = () => { }
        const error = (syncUserError) => {
            console.log({ syncUserError, time: this.syncTime })
            setTimeout(() => { this.syncCurrentUser() }, this.syncTime);
        }

        this.updateCurrentUser().subscribe(success, error);
    }

    updateCurrentUser(): Observable<any> {
        return this.storageService.getItem('token').pipe(switchMap((token) => {
            if (!token)
                return throwError('no token');

            return this.requestService.getApi('currentuser/').pipe(switchMap((response: any) => {
                if (!response.currentUser)
                    return throwError('no user');

                this.setCurrentUser(response.currentUser);
                return of(response.currentUser);
            }));

        }));
    }

    login(data: LoginCredentials) {
        const headers = { 'Accept': '*/*' };
        return this.requestService.postApi('login/', data, headers, true, '')
            .pipe(
                switchMap((response: any) => {
                    if (response.token) {
                        this.setCurrentUser(Object.assign({}, response, { token: 'stored in AsyncStorage' }));
                        return this.storageService.setItem('token', response.token).pipe(switchMap(() => this.updateCurrentUser()), tap(_ => this.signinComplete.next()));
                    }

                    else {
                        console.log({ response });
                        throw response;
                    }

                }),

            );
    }

    register(data: RegisterUser): Observable<ApiResponseRegister> {
        const headers = { 'Accept': '*/*' };
        return this.requestService.postApi('register/', data, headers, true, '');
    }

    logout() {
        return new Promise<void>((resolve) => {
            this.storageService.removeItem('token').subscribe(() => {
                this.setCurrentUser(undefined);
                resolve();
            });
        })

    }
}

export class LoginCredentials {
    username: string;
    password: string;
}

export interface ApiResponseRegister {
    username: string;
    email: string;
    id: number;
}