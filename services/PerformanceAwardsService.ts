export class PerformanceAwardsService {
    requestService;
    constructor({ requestService }) {
        this.requestService = requestService;
    }

    getUsers() {
        return this.requestService.getApi('user/');
    }
}