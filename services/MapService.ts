import { RequestService } from "./RequestService";
import { tap } from "rxjs/operators";
import { Subject, Observable } from "rxjs";
import { UserModel } from "./models/UserModel";
import { ApiResponse } from "./models/ApiResponse";

export class MapService {
    requestService: RequestService;
    updated = new Subject();
    constructor({ requestService }) {
        this.requestService = requestService;
    }

    directions(): Observable<UserModel> {
        const headers = { "Access-Control-Allow-Origin": "*" }
        return this.requestService.json('https://maps.googleapis.com/maps/api/directions/json?origin=%D0%9B%D1%8C%D0%B2%D0%B0%20%D0%A2%D0%BE%D0%BB%D1%81%D1%82%D0%BE%D0%B3%D0%BE%202-%D0%B0%2C%2061%2C%20%D0%91%D0%B8%D1%88%D0%BA%D0%B5%D0%BA&destination=8%20%D0%92%D0%BE%D0%BB%D0%BE%D0%BA%D0%B0%D0%BB%D0%B0%D0%BC%D1%81%D0%BA%D0%B0%D1%8F%20%D1%83%D0%BB%D0%B8%D1%86%D0%B0%2C%20Bishkek%2C%20Kyrgyzstan&key=AIzaSyBxb2oSyXjhMmMODTrp2Xm6-sLmBBbqE9M', JSON.stringify(headers));
    }


}