import { Observable, Subject } from "rxjs";
import { map, tap } from "rxjs/operators";
import { DateTimeService } from "./DateTimeService";
import { Project } from "./models/Project";
import { RequestService } from "./RequestService";

export class ProjectService {
    requestService: RequestService;
    dateTimeService: DateTimeService;
    updated = new Subject();
    constructor({ requestService, dateTimeService }) {
        this.requestService = requestService;
        this.dateTimeService = dateTimeService;
    }

    get(id: number): Observable<Project> {
        return this.requestService.getApi(`project/${id}/`);
    }

    create(data: Project): Observable<Project> {
        return this.requestService.postApi('project/', data).pipe(tap(() => { this.updated.next(); }));
    }

    update(id: number, data: Project): Observable<Project> {
        return this.requestService.putApi(`project/${id}/`, data).pipe(tap(() => { this.updated.next(); }));
    }

    getAll(): Observable<Project[]> {
        return this.requestService.getApi('project/');
    }

    getAllByParticipant(id: number): Observable<Project[]> {
        return this.requestService.getApi(`project-participant/?participant=${id}`).pipe(map((result) => result.map((item) => item.project_details)));
    }
}