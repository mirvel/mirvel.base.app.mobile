export const defaultLanguageTags = {
    About: 'About',
    Amount: 'Amount',
    Title: 'Subject',
    Description: 'Description',
    Submit: 'Submit',
    Done: 'Done',
    Save: 'Save',
    Cancel: 'Cancel',
    TaskTitle: 'Task',
    Scan: 'Scan',
    Close: 'Close',
    Username: 'Username',
    Password: 'Password',
    Email: 'Email',
    Register: 'Register',
    CreateTask: 'Create task',
    CreateTransaction: 'Create Transaction',
    AquirePointsTitle: 'Aquire Points on your Account',
    AquirePoints: 'Aquire points',
    Registration: 'Registration',
    SignIn: 'Sing-in',
    Points: 'Points',
    Date: 'Date',
    Team: 'Team',
    TeamName: 'Team name',
    Update: 'Update',
    ExistingMembers: 'Existing members',
    Users: 'Users',
    Tasks: 'Tasks',
    MyTasks: 'My Tasks',
    NonExistingTranslation: 'Non Existing Translation',
    SignOut: 'Log Out',
    InvokeError: 'Invoke Error',
    AddTeamMember: 'Add Team Member',
    Accounts: 'Accounts',
    CreateAccount: 'Create Account',
    CreateProject: 'Create Project',
    EditProject: 'Edit Project',
    Projects: 'Projects',
    Search: 'Search',
    SearchAccount: 'Search Account',
    Select: 'Select',
    Participants: 'Participants'
}


const translationTags = {
    'ru-ru': {
        Title: 'Название',
        Description: 'Описание',
        TaskTitle: 'Лист закупок',
        Submit: 'Сохранить',
        Done: 'Выполнено',
        Save: 'Сохранить',
        Scan: 'Сканировать',
        Close: 'Закрыть',
        Username: 'Имя пользователя',
        Password: 'Пароль',
        Email: 'Электронная почта',
        Register: 'Создать аккаунт',
        CreateTask: 'Создать лист покупок',
        AquirePointsTitle: 'Зачислить поинты на свой аккаунт',
        AquirePoints: 'Зачислить поинты',
        Registration: 'Регистрация',
        SignIn: 'Вход',
        Date: 'Дата',
        Points: 'Поинты',
        SignOut: 'Выход',
    }
}

export class LanguageService {
    language = 'en-us';
    default = Object.assign({}, defaultLanguageTags);
    tags = Object.assign({}, defaultLanguageTags);
    translations = translationTags;
    constructor(language?: string) {
        this.setLanguage(language || this.language);
    }
    translate(key: string) {
        return this.translations[this.language] && this.translations[this.language][key] || this.default[key] || key;
    }
    setLanguage(language) {
        this.language = language;
        this.tags = Object.assign(this.default, this.translations[this.language]);
    }

}


