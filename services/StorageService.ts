import AsyncStorage from "@react-native-async-storage/async-storage";
import { from, Observable } from "rxjs";

export class StorageService {
    setItem(key: string, data: any): Observable<void> {
        return from(AsyncStorage.setItem(key, data));
    }

    getItem(key): Observable<string> {
        return from(AsyncStorage.getItem(key));
    }

    removeItem(key): Observable<void> {
        return from(AsyncStorage.removeItem(key));
    }
}