import * as rxjsAjax from "rxjs/ajax";
import { map, catchError, switchMap } from 'rxjs/operators';
import { of, from } from 'rxjs';
import { StorageService } from "./StorageService";
import { ServiceProviders } from "../AppProvider";

export class RequestService {
    apiBaseUrl;
    storageService: StorageService;
    rxjs_ajax_options: any;

    constructor({ storageService, env }: ServiceProviders) {
        this.storageService = storageService;
        this.apiBaseUrl = env.url;
    }

    customError(e, url) {
        console.log('Error in request to url:' + url);
        const error = { errorStatus: e.status, error: e.response, message: e.message };
        console.log(JSON.stringify(e));
        console.log(error);
        if (error.errorStatus === 401)
            this.storageService.setItem('token', undefined);
        if (!error.errorStatus && !error.error)
            error.error = { message: error.message }

        throw error;
    }

    // Base Methods
    ajax(url, body, headers, method, no_token) {
        console.log(`${method} ${url}`);
        let headersExtended = Object.assign({}, headers);
        const token$ = this.storageService.getItem('token');

        return token$.pipe(
            switchMap((token) => {
                if (!no_token)
                    headersExtended = Object.assign(headersExtended, { 'Authorization': 'Token ' + token });

                const rxjsAjaxOptions = Object.assign({
                    url: url,
                    method: method,
                    headers: headersExtended,
                    body: JSON.stringify(body)
                }, this.rxjs_ajax_options)
                return rxjsAjax.ajax(rxjsAjaxOptions).pipe(
                    map(response => { return response.response; }),
                    catchError(e => {
                        throw this.customError(e, url);
                    })

                );
            }
            ))

    }
    json(url, headers = undefined) {
        return rxjsAjax.ajax.getJSON(url, headers).pipe(
            map((response: any) => response.response),
            catchError(e => {
                throw this.customError(e, url);
            })
        );;
    }
    post(url, body = undefined, headers = undefined, no_token = false) {
        const headersExtended = Object.assign({
            'Content-Type': "application/json"
        }, headers);
        return this.ajax(url, body, headersExtended, 'POST', no_token);
    }
    get(url, headers = undefined, no_token = false) {
        const headersExtended = Object.assign({
            'Content-Type': "application/json",
            'Expires': '0',
            'Pragma': 'no-cache',
            'Cache-Control': 'no-cache'
        }, headers);
        return this.ajax(url, null, headersExtended, 'GET', no_token);
    }
    put(url, body = undefined, headers = undefined, no_token = false) {
        const headersExtended = Object.assign({
            'Content-Type': "application/json"
        }, headers);
        return this.ajax(url, body, headersExtended, 'PUT', no_token);
    }
    delete(url, headers = undefined, no_token = false) {
        const headersExtended = Object.assign({
            'Content-Type': "application/json"
        }, headers);
        return this.ajax(url, null, headersExtended, 'DELETE', no_token);
    }


    // Extended Methods
    postApi(url, body, headers = undefined, no_token = false, apiVersion = 'v1/') {
        return this.post(this.apiBaseUrl + 'api/' + apiVersion + url, body, headers, no_token);
    }

    getApi(url, headers = undefined, no_token = false, apiVersion = 'v1/') {
        return this.get(this.apiBaseUrl + 'api/' + apiVersion + url, headers, no_token);
    }

    getApiJson(url, apiVersion = 'v1/') {
        return this.json(this.apiBaseUrl + 'api/' + apiVersion + url);
    }

    putApi(url, body, headers = undefined, no_token = false, apiVersion = 'v1/') {
        return this.put(this.apiBaseUrl + 'api/' + apiVersion + url, body, headers, no_token);
    }

    deleteApi(url, headers = undefined, no_token = false, apiVersion = 'v1/') {
        return this.delete(this.apiBaseUrl + 'api/' + apiVersion + url, headers, no_token);
    }

}