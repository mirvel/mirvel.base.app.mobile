import { of, Subject, Subscription, throwError } from "rxjs";
import { catchError, switchMap } from "rxjs/operators";
import { ServiceProviders } from "../AppProvider";
import { WebSocketReadyStates, WebSocketWrapprer } from "./models/WebSocketWrapper";
import { StorageService } from "./StorageService";
export interface ModelUpdatedMessage {
    class_name: string;
    id: string;
    send_to: string;
    created: boolean;
}

export class WebSocketService {
    otherTime = 10000;
    storageService: StorageService;
    modelUpdated = new Subject<ModelUpdatedMessage>();
    webSocketUrl;
    webSocket: WebSocketWrapprer;
    subscriptions: Subscription[] = [];
    timers: any[] = [];

    constructor({ storageService, env, webSocket }: ServiceProviders) {
        this.storageService = storageService;
        this.webSocketUrl = env.webSocketUrl;
        this.webSocket = webSocket;
    }

    establishConnection(): void {
        const success = (message) => { console.log(message) };
        const error = (_connectionError) => {
            console.log({ _connectionError, otherTime: this.otherTime });
            setTimeout(() => { this.establishConnection() }, this.otherTime);
        }

        this.connect().subscribe(success, error);
    }

    connect() {
        // Skip connecting when open or connecting
        if (this.webSocket.getReadyState() === WebSocketReadyStates.OPEN || this.webSocket.getReadyState() === WebSocketReadyStates.CONNECTING)
            return;

        console.log('Trying to connect to websocket....');
        return this.storageService.getItem('token').pipe(switchMap((token) => {
            if (!token)
                return throwError('no token');

            return this.webSocket.connection(`${this.webSocketUrl}?token=${token}`).pipe(
                catchError((catchedError) => {
                    console.log({ catchedError });
                    if (catchedError.error)
                        this.onError(catchedError.error)
                    if (catchedError.close)
                        this.onClose(catchedError.close)
                    return throwError('disconnected');
                }),
                switchMap((establishedChannel) => {
                    this.onOpen(establishedChannel);
                    return of('connected');
                }));


        }));
    }
    onOpen(establishedChannel: any) {
        console.log('opened')
        this.subscriptions.push(establishedChannel.subscribe((message) => {
            this.onMessage(message)
        }));
    }
    onError(onWebSocketError: WebSocketErrorEvent) {
        console.log({ onWebSocketError });
    }
    onClose(onWebSocketClose: WebSocketCloseEvent) {
        console.log({ onWebSocketClose });
        this.unsubscribeAll();
    }
    onMessage(onWebSocketMessage: WebSocketMessageEvent) {
        console.log({ onWebSocketMessage });
        const data = JSON.parse(onWebSocketMessage.data);
        if (data && data.message && data.message.class_name) {
            this.modelUpdated.next(data.message);
        }
    }
    unsubscribeAll(): void {
        if (this.subscriptions && this.subscriptions.length)
            this.subscriptions.forEach(element => {
                element.unsubscribe()
            });
        this.subscriptions = [];
    }
    clearTimers(): void {
        console.log('clear timers')
        if (this.timers && this.timers.length)
            this.timers.forEach(timer => {
                console.log('clearTimers');
                console.log(this.timers.length);
                clearTimeout(timer);
            });
        this.timers.length = 0;
    }
    disconnect() {
        console.log(this.disconnect);
        this.webSocket.close();
    }
}
