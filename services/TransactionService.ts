import { Observable, Subject } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { DateTimeService } from './DateTimeService';
import { Transaction } from './models/Transaction';
import { RequestService } from './RequestService';

export class TransactionService {
    requestService: RequestService;
    dateTimeService: DateTimeService;
    updated = new Subject();
    constructor({ requestService, dateTimeService }) {
        this.requestService = requestService;
        this.dateTimeService = dateTimeService;
    }

    create(transaction: Transaction): Observable<Transaction> {
        return this.requestService.postApi('transaction/', transaction).pipe(tap(() => { this.updated.next(1); }));
    }

    getAll(): Observable<Transaction[]> {
        return this.requestService.getApi('transaction/').pipe(map((items: Transaction[]) => {
            return items.map((item) => {
                console.log({ item });
                return {
                    ...item,
                    created: item.created_date ? new Date(item.created_date) : undefined,
                    created_date_formatted: this.dateTimeService.toDateDisplayFormat(item.created_date),
                    transaction_date: item.date ? new Date(item.date) : undefined,
                    transaction_date_formatted: this.dateTimeService.toDateDisplayFormat(item.date)
                }
            })
        }));
    }

    getLast() {
        return this.requestService.getApi('transaction/?limit=3&ordering=-id');
    }

    summary() {
        return this.requestService.getApi('summary/');
    }
}