import { Observable, Subject } from "rxjs";
import { map, tap } from "rxjs/operators";
import { DateTimeService } from "./DateTimeService";
import { AccountModel } from "./models/AccountModel";
import { Transaction } from "./models/Transaction";
import { RequestService } from "./RequestService";

export class AccountService {
    requestService: RequestService;
    dateTimeService: DateTimeService;
    updated = new Subject();
    constructor({ requestService, dateTimeService }) {
        this.requestService = requestService;
        this.dateTimeService = dateTimeService;
    }

    get(accountId: number): Observable<AccountModel> {
        return this.requestService.getApi(`account/${accountId}/`);
    }

    create(data: AccountModel): Observable<AccountModel> {
        return this.requestService.postApi('account/', data).pipe(tap(() => { this.updated.next(); }));
    }

    getAll(): Observable<AccountModel[]> {
        return this.requestService.getApi('account/');
    }

    transactions(recipient_id: number): Observable<Transaction[]> {
        return this.requestService.getApi(`account/${recipient_id}/transactions/`).pipe(map((items) => {
            return items.map((item) => {
                return {
                    ...item,
                    created: item.created_date ? new Date(item.created_date) : undefined,
                    created_date_formatted: this.dateTimeService.toDateDisplayFormat(item.created_date),
                    transaction_date: item.date ? new Date(item.date) : undefined,
                    transaction_date_formatted: this.dateTimeService.toDateDisplayFormat(item.date)

                }
            })

        }));
    }
}