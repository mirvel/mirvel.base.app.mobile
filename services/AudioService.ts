import { Audio } from 'expo-av';
import { justSaying } from '../constants/Sounds';
export class AudioService {
    sound = new Audio.Sound();
    ready: boolean = false;
    constructor() {
        //this.loadSound();
    }
    loadSound() {
        try {
            this.sound.loadAsync(justSaying).then((status) => {
                this.ready = status.isLoaded;
            }).catch((loadAsyncError) => {
                console.log({ loadAsyncError })
            });
        } catch (audioError) {
            console.log({ error: audioError });
        }
    }
    playSound() {
        if (this.ready)
            this.sound.playAsync().then(() => {
                this.sound.unloadAsync();
                this.ready = false;
                this.loadSound();
            }).catch((playAsyncError) => {
                console.log({ playAsyncError });
            });
    }
    playSound2() {
        try {
            const sound = new Audio.Sound();
            sound.loadAsync(justSaying).then((status) => {
                console.log(status)
                if (status.isLoaded)
                    sound.playAsync().then(() => {
                        sound.unloadAsync();
                    }).catch((playAsyncError) => {
                        console.log({ playAsyncError });
                    });
            }).catch((loadAsyncError) => {
                console.log({ loadAsyncError })
            });


            // Your sound is playing!
        } catch (audioError) {
            // An error occurred!
            console.log({ error: audioError });
        }
    }

}