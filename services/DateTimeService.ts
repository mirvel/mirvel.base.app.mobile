export class DateTimeService {
    constructor({ }) {
    }
    toDisplayFormat(date: Date): string {
        return date ? date.toLocaleDateString('en-US', { year: 'numeric', month: '2-digit', day: '2-digit' }) : ''
    }

    toDateDisplayFormat(dateString: string | Date): string {
        return dateString ? this.toDisplayFormat(new Date(dateString)) : '';
    }
}