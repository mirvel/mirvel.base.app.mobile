import { RequestService } from "./RequestService";
import { tap } from "rxjs/operators";
import { Subject, Observable } from "rxjs";
import { UserModel } from "./models/UserModel";
import { ApiResponse } from "./models/ApiResponse";

export class UserService {
    requestService: RequestService;
    updated = new Subject();
    constructor({ requestService }) {
        this.requestService = requestService;
    }

    create(data: UserModel) {
        return this.requestService.postApi('user/', data).pipe(tap(() => { this.updated.next(); }));
    }

    get(id: number): Observable<UserModel> {
        return this.requestService.getApi(`user/${id}/`);
    }

    getAll(): Observable<UserModel[]> {
        return this.requestService.getApi(`user/`);
    }

    getLast(limit: number, offset: number = 0): Observable<ApiResponse<UserModel[]>> {
        return this.requestService.getApi(`user/?limit=${limit}&offset=${offset}&ordering=-id`);
    }
    update(id: number, data: UserModel): Observable<UserModel> {
        return this.requestService.putApi(`user/${id}/`, data).pipe(tap(() => { console.log('tapped'); this.updated.next(); }));
    }

    getAllFiltered(): Observable<UserModel[]> {
        return this.requestService.getApi(`users-filtered/`);
    }
}