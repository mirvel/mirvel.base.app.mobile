export class UserModel {
    readonly id: number;
    username: string;
    email: string;
}
