export class TaskModel {
    id?: number;
    title: string;
    description: string;
    completed_date: Date;
    assigned_date: Date;
    scheduled_date: Date;
    confirmed_date: Date;
    category: any;
    completed_by: any;
    assigned_to: any;
    confirmed_by: any;
    parent_task: any;
    points: number;
    visible_to_team: number;
}
export enum TaskState {
    rejected = -2,
    cancelled = -1,
    open = 0,
    complete = 1,
    approved = 2
}
