import { Observable, Subject } from "rxjs";

export interface IWebSocket {
    onopen: (r: any) => void;
    onmessage: (r: any) => void;
    onerror: (e: any) => void;
    onclose: (r: any) => void;
    close: (r?: any, d?: any) => {}
    readyState: number;

}
export enum WebSocketReadyStates {
    CONNECTING = 0,
    OPEN = 1,
    CLOSING = 2,
    CLOSED = 3,
    UNKNOWN = 10
}
export interface ConnectionResponse {
    connection: Observable<Observable<any>>;
    instance: any;
}

export class WebSocketWrapprer {
    private regularWebSocket: boolean;
    private instance: any;
    private onOpenSubject: Subject<any> = new Subject<any>();
    public onOpen: Observable<any> = this.onOpenSubject.asObservable();

    private onMessageSubject: Subject<any> = new Subject<any>();
    onMessage: Observable<any> = this.onMessageSubject.asObservable();

    private onErrorSubject: Subject<any> = new Subject<any>();
    onError: Observable<any> = this.onErrorSubject.asObservable();

    private onCloseSubject: Subject<any> = new Subject<any>();
    onClose: Observable<any> = this.onCloseSubject.asObservable();

    constructor(private webSocket) {
        if (typeof (WebSocket) === 'function' && WebSocket.name === 'WebSocket')
            this.regularWebSocket = true;
    }

    close(r?: any, d?: any) {
        if (this.regularWebSocket)
            this.instance.close(r, d);
    };

    getReadyState() {
        if (!this.instance)
            return WebSocketReadyStates.UNKNOWN;
        switch (this.instance.readyState) {
            case 0:
                return WebSocketReadyStates.CONNECTING
            case 1:
                return WebSocketReadyStates.OPEN
            case 2:
                return WebSocketReadyStates.CLOSING
            case 3:
                return WebSocketReadyStates.CLOSED
            default:
                return WebSocketReadyStates.UNKNOWN;
        }
    }
    connectWithRegularWebSocket(url) {
        console.log('connecting with regular socket');
        this.instance && this.instance.close();
        this.instance = new this.webSocket(url);
        this.instance.onopen = (r) => {
            console.log('Success!!!')
            this.onOpenSubject.next(r);
        };
        this.instance.onmessage = (r: any) => {
            this.onMessageSubject.next(r);
        };
        this.instance.onerror = (e: any) => {
            this.onErrorSubject.next(e);
        }
        this.instance.onclose = (r: any) => {
            this.onCloseSubject.next(r);
        }
    }
    connect(url) {
        if (this.regularWebSocket)
            this.connectWithRegularWebSocket(url);
    }

    connection(url): any {
        const subject = new Subject<Observable<any>>();
        console.log('connecting with regular socket');
        const instance = new this.webSocket(url);

        const messageDispatcher = new Subject();
        instance.onopen = (r) => {
            console.log('native websocket is open')
            subject.next(messageDispatcher.asObservable());
        };
        instance.onmessage = (r: any) => {
            messageDispatcher.next(r);
        };
        instance.onerror = (e: any) => {
            console.log('native websocket error')
            subject.error(e);
        }
        instance.onclose = (r: any) => {
            console.log('native websocket close')

        }
        return subject;
        //return { connection: subject.asObservable(), instance: instance };
    }
}