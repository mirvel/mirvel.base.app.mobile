import { BaseModel } from "./BaseModel";

export class Project extends BaseModel {
    id?: number;
    title: string;
    participants: number[];
}