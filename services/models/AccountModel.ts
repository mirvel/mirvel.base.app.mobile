export class AccountModel {
    username: string;
    user?: number;
    id?: number;
    created_by?: number;
    phone?: string;
    total_amount?: number;
}