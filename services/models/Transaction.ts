export class Transaction {
    id: number;
    date: Date;
    transaction_date_formatted: string;
    created_date_formatted: string;
    created_date: string;
    created: Date;
    transaction_type: number;
    comment: string;
    recipient: number;
    amount: number;
    source_FK: number;
    source_Type: string;
    title: string;
    created_by: number;
    description: string;
    project: number;
}