import { RequestService } from "./RequestService";
import { tap } from "rxjs/operators";
import { Subject, Observable } from "rxjs";
import { TeamMembership } from "./models/TeamMembership";
import { ApiResponse } from "./models/ApiResponse";

export class TeamMembershipService {
    requestService: RequestService;
    updated = new Subject();
    constructor({ requestService }) {
        this.requestService = requestService;
    }

    create(data: TeamMembership): Observable<TeamMembership> {
        return this.requestService.postApi('team-members/', data).pipe(tap(() => { this.updated.next(); }));
    }

    get(id: number): Observable<TeamMembership> {
        return this.requestService.getApi(`team-members/${id}/`);
    }

    getAll(): Observable<ApiResponse<TeamMembership[]>> {
        return this.requestService.getApi(`team-members/`);
    }

    getLast(limit: number, offset: number = 0): Observable<ApiResponse<TeamMembership[]>> {
        return this.requestService.getApi(`team-members/?limit=${limit}&offset=${offset}&ordering=-id`);
    }

    update(id: number, data: TeamMembership): Observable<TeamMembership> {
        return this.requestService.putApi(`team-members/${id}/`, data).pipe(tap(() => { this.updated.next(); }));
    }

    delete(id: number): Observable<TeamMembership> {
        return this.requestService.deleteApi(`team-members/${id}/`);
    }

    findByMemberAndTeamId(memberId: number, teamId: number) {
        return this.requestService.getApi(`team-members/?member=${memberId}&team=${teamId}`);

    }
}