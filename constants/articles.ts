export default [
  {
    title: 'магазин Народный',
    image: 'http://www.elsyst.kg/uploads/default/projects/5334ba51d932deba749d0b9128b49752_big_r.jpg',
    description: 'Акция магазина Народный. Купите на сумму 1000 и получите 1500 поинтов',
    cta: 'Подробнее...',
    horizontal: true
  },
  {
    title: 'Мегаком',
    image: 'https://24.kg/thumbnails/faa57/cdd96/106649_w750_h_r.jpg',
    description: 'Акция Мегаком. Подключитесь на новый тариф Bravo и получите 2000 поинтов',
    cta: 'Подробнее...',
    horizontal: true
  },
  {
    title: 'магазин Фрунзе',
    image: 'https://gipermarket.kg/wp-content/uploads/2018/02/fru-512-512.png',
    description: 'Акция магазина Фрунзе. Купите на сумму 1000 и получите 1000 поинтов',
    cta: 'Подробнее...',
    horizontal: true
  },
  {
    title: 'Технодом',
    image: 'https://magnolia.kz/uploads/posts/2016-11/1478752598_bez-imeni-1.jpg',
    description: 'Акция магазина Технодом. Купите телевизор и получите 20000 поинтов',
    cta: 'Подробнее...',
    horizontal: true
  },
  {
    title: 'Шоро',
    image: 'https://popkult.org/wp-content/uploads/2017/07/shoro-1.jpg',
    description: 'Акция Шоро. Купите 10 бутылок шоро и получите 1000 поинтов',
    cta: 'Подробнее...',
    horizontal: true
  },
];