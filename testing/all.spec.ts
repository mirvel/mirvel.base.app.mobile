import { TestService } from "../services/TestService";
import { AuthService } from "../services/AuthService";
import { RequestService } from "../services/RequestService";
import { XMLHttpRequest } from 'xmlhttprequest';

import { of, Subject } from "rxjs";
import { TaskService } from "../services/TaskService";
import { TaskModel } from "../services/models/Task";
import { LanguageService, defaultLanguageTags } from "../services/LanguageService";
import { TeamModel } from "../services/models/TeamModel";
import { TeamService } from "../services/TeamService";
import { EnvironmentVariables } from "../environment";
import { TeamMembershipService } from "../services/TeamMembershipService";
import { switchMap } from "rxjs/operators";
import { TeamMembership } from "../services/models/TeamMembership";
import { Assert } from "./asserts"
import { WebSocketService } from "../services/WebSocketService";
import { PushNotificationTokenModel } from "../services/models/PushNotificationTokenModel";
import { MapService } from "../services/MapService";


function createXHR() {
    return new XMLHttpRequest();
}

class MockStorageService {
    storage = {}
    setItem(key, value) {
        return of(this.storage[key] = value);
    }
    getItem(key) { return of(this.storage[key]) };
}
class MockNotificationService {
    unregisterNotification() { }
    registerForPushNotificationsAsync(): any { }
    expoPushToken;
    notificationReceived;
    requestService;
    eventSubscription;
    _handleNotification = notification => { };
    dispatch(dispatchedData) { }
    setToken(expoPushToken) { }
    sendPushNotification(title, body, data) { };
    savePushNotificationToken(data: PushNotificationTokenModel) { }

}

class MockWebSocketService {
    modelUpdated = new Subject();
    connect() { };
}

// Services
const storageService = new MockStorageService();
const env: EnvironmentVariables = {
    url: 'http://0.0.0.0:8888/',
    webSocketUrl: '',
    envName: 'test'
};
const requestService = new RequestService({ storageService, env } as any);
requestService.rxjs_ajax_options = { createXHR }; // To Allow CORS
const notificationService = new MockNotificationService();
const webSocketService = new MockWebSocketService();
const authService = new AuthService({ requestService, storageService, notificationService, webSocketService } as any);
const taskService = new TaskService({ requestService, webSocketService } as any);
const teamService = new TeamService({ requestService });
const teamMembershipService = new TeamMembershipService({ requestService });
const languageService = new LanguageService('ru-ru');
const mapService = new MapService({ requestService });

function testLog(done) {
    console.log('--testLog')
    const testService = new TestService();
    testService.log();
    done();
}

function testStorageService(done) {
    storageService.getItem('token').subscribe((token) => {
        console.log(token);
        done();
    })
}
function testUpdateCurrentUser(done) {
    console.log(authService.currentUser.value);
    authService.setCurrentUser(undefined);
    console.log(authService.currentUser.value);
    authService.updateCurrentUser().subscribe(() => {
        console.log(authService.currentUser.value);
        done();
    }, error => console.log(error))
}

let lastUserSuffix;
const userPassword = '123';
let lastRegisteredUser;
function testRegister(done) {
    lastUserSuffix = `z${Math.round(Math.random() * 10000)}`;
    authService.register({ username: `user${lastUserSuffix}`, password: userPassword, email: `user${lastUserSuffix}@gmail.com` }).subscribe((registeredUser) => {
        lastRegisteredUser = registeredUser;
        console.log(registeredUser);
        done();
    });
}

let lastLoggedInUser;
function testLogin(done) {
    const newUserName = `user${lastUserSuffix}`;
    authService.login({ username: newUserName, password: userPassword }).subscribe((loggedInUser) => {
        console.log({ loggedInUser });
        console.log({ currentUser: authService.currentUser.value });
        lastLoggedInUser = authService.currentUser.value;
        storageService.getItem('token').subscribe((token) => {
            console.log(token);
            done();
        });
    });
}

// TaskService
let newTaskSuffix;
let lastCreatedTask;

function testCreateTask(done) {
    newTaskSuffix = Math.round(Math.random() * 10000);
    const value = new TaskModel();
    value.title = `Task #${newTaskSuffix} Sed ut perspiciatis unde omnis`;
    value.description = `#${newTaskSuffix} Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. `;
    value.points = Math.round(Math.random() * 100) * 100;

    taskService.createTask(value).subscribe((response) => {
        Assert.toBe(response.title, value.title);
        Assert.toBe(response.points, value.points);
        console.log(response.title);
        lastCreatedTask = response;
        done();
    })
}

function testGetTask(done) {
    taskService.getTask(lastCreatedTask.id).subscribe((response) => {
        Assert.toBe(response.id, lastCreatedTask.id)
        console.log(`${response.title} ${response.points}`);
        done();
    })
}

function testGetTasks(done) {
    taskService.getLast(10, 0).subscribe((response) => {
        Assert.toBe(response.count, 2);
        response.results.forEach(element => console.log(`${element.title} ${element.points}`));
        done();
    })
}

function testGetMyTasksOrOfMyTeams(done) {
    // Register as a new user
    const randomPrefix = `b${Math.round(Math.random() * 10000)}`;
    const taskCreated = authService.register({ username: `user${randomPrefix}`, password: '123', email: 'email@gmail.com' }).pipe(switchMap((registeredUser) => {
        return authService.login({ username: registeredUser.username, password: '123' }).pipe(switchMap(_ => {
            return teamService.create(Object.assign(new TeamModel(), { title: `Team #${randomPrefix}` })).pipe(switchMap((team) => {
                console.log({ lastLoggedInUser });
                return teamMembershipService.create(Object.assign(new TeamMembership(), { team: team.id, member: lastLoggedInUser.id })).pipe(switchMap((teamMemberCreated) => {
                    const newTask = new TaskModel();
                    newTask.title = `Task #${randomPrefix} Task created by user${randomPrefix} for Team #${randomPrefix}`;
                    newTask.description = `Description #${randomPrefix} Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. `;
                    newTask.points = Math.round(Math.random() * 100) * 100;
                    newTask.visible_to_team = teamMemberCreated.team;
                    return taskService.createTask(newTask);
                }));

            }));
        }));

    }));

    taskCreated.subscribe((task) => {
        authService.login({ username: lastLoggedInUser.username, password: '123' }).subscribe(() => {
            taskService.getAllFiltered().subscribe((response) => {
                Assert.toBe(response.length, 3);
                response.forEach(element => console.log(`${element.title} ${element.points}`));
                done();
            });
        });

    })

}

const testsTask = [testCreateTask, testCreateTask, testGetTask, testGetTasks, testGetMyTasksOrOfMyTeams];

// Teams
let teamPrefix;
let lastCreatedTeam;

function testCreateTeam(done) {
    teamPrefix = Math.round(Math.random() * 10000);
    const value = new TeamModel();
    value.title = `Team #${teamPrefix}`;

    teamService.create(value).subscribe((response) => {
        Assert.toBe(response.title, value.title);
        console.log(response.title);
        lastCreatedTeam = response;
        done();
    })
}

function testGetTeam(done) {
    teamService.get(lastCreatedTeam.id).subscribe((response) => {
        Assert.toBe(response.id, lastCreatedTeam.id)
        console.log(`${response.title}`);
        done();
    })
}

function testGetTeams(done) {
    teamService.getLast(10, 0).subscribe((response) => {
        Assert.toBeTruthy(response.count > 0);
        response.results.forEach(element => console.log(`${element.title}`));
        done();
    })
}

function testUpdateTeam(done) {
    teamPrefix = Math.round(Math.random() * 10000);
    const newModel = new TeamModel()
    newModel.title = `Team Updated #${newTaskSuffix}`;
    teamService.update(lastCreatedTeam.id, newModel).subscribe((response) => {
        Assert.toBe(response.title, newModel.title);
        console.log(response.title);
        done();
    })
}

function testGetAllMyTeams(done) {
    teamService.getAll().subscribe((response) => {
        Assert.toBeTruthy(response.length > 0);
        response.forEach(element => console.log(`${element.title}`));
        done();
    })
}

const testsTeam = [testCreateTeam, testGetTeam, testGetTeams, testUpdateTeam, testGetAllMyTeams]

// Team Membership
let lastCreatedTeamMembershipId;
function testCreateTeamMembership(done) {
    const teamMember = { team: lastCreatedTeam.id, member: authService.currentUser.value.id };
    console.log({ teamMember });
    teamMembershipService.create(teamMember).subscribe((createdTeamMembership) => {
        lastCreatedTeamMembershipId = createdTeamMembership.id;
        console.log({ createdTeamMembership });
        done();
    })
}


function testGetTeamMembership(done) {
    teamMembershipService.get(lastCreatedTeamMembershipId).subscribe((teamMembership) => {
        Assert.toBe(lastCreatedTeamMembershipId, teamMembership.id);
        console.log({ teamMembership });
        done();
    })
}

function testDeleteTeamMembership(done) {
    teamMembershipService.delete(lastCreatedTeamMembershipId).subscribe((response) => {
        Assert.toBe(response, null);
        console.log(`TeamMembers #${lastCreatedTeamMembershipId} Deleted successifully`);
        done();
    })
}

const testsTeamMembership = [testCreateTeamMembership, testGetTeamMembership, testDeleteTeamMembership];

// Language Service
function testTranslateTitleTORussion(done) {
    languageService.setLanguage('en-us');
    Assert.equal(languageService.tags.Title, languageService.translations["ru-ru"].Title)
    console.log(languageService.tags.Title);
    console.log(languageService.translations["ru-ru"].Title);
    done();
}

function testTranslateFallback(done) {
    languageService.setLanguage('ru-ru-store');
    Assert.equal(languageService.tags.NonExistingTranslation, languageService.default.NonExistingTranslation)
    console.log(languageService.tags.NonExistingTranslation);
    done();
}
function testTranslateToDefault(done) {
    languageService.setLanguage('en-us');
    Assert.equal(languageService.tags.Title, languageService.default.Title)
    console.log(languageService.tags.Title);
    done();
}

// Map Service
function testMapsDirections(done) {
    const response = mapService.directions();
    response.subscribe(r => {
        console.log(r);
        done();
    })

}

const languageTesting = [
    testTranslateTitleTORussion,
    testTranslateFallback,
    testTranslateToDefault

]

const testsWebSocket = [testLog,
    testRegister,
    testLogin];

const defaultFlow = [
    testLog,
    testRegister,
    testLogin,
    testStorageService,
    testUpdateCurrentUser,
    ...testsTask,
    ...testsTeam,
    ...testsTeamMembership,
    ...testsWebSocket
]

const tests = [...defaultFlow, ...languageTesting];







// Execute tests Syncronically
const fn = (tests) => {
    if (tests && tests.length > 0) {
        const currentTestFn = tests.shift();
        console.log();
        console.log(`-----------------${currentTestFn.name}---------------`)
        currentTestFn(() => {
            fn(tests);
        });
    }

}
fn(tests);