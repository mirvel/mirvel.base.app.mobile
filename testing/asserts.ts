export const Assert = {
    equal(a, b) {
        if (a != b)
            throw `Failed: ${a} != ${b}`
    },
    notEqual(a, b) {
        if (a == b)
            throw `Failed: ${a} == ${b}`
    },

    toBe(a, b) {
        if (a !== b)
            throw `Failed: ${a} !== ${b}`
    },

    toBeGreater(a, b) {
        if (a <= b)
            throw `Failed: ${a} <= ${b}`
    },

    notToBe(a, b) {
        if (a === b)
            throw `Failed: ${a} === ${b}`
    },


    toBeTruthy(exp) {
        if (!exp)
            throw `Failed: ${exp} to be truthy`
    },
}