import "jest";
import { Observable, of, Subject } from "rxjs";
import { XMLHttpRequest } from 'xmlhttprequest';
import { AccountService } from "../../services/AccountService";
import { ApiResponseRegister, AuthService } from "../../services/AuthService";
import { DateTimeService } from "../../services/DateTimeService";
import { AccountModel } from "../../services/models/AccountModel";
import { Project } from "../../services/models/Project";
import { PushNotificationTokenModel } from "../../services/models/PushNotificationTokenModel";
import { ProjectService } from "../../services/ProjectService";
import { RequestService } from "../../services/RequestService";
import { TeamMembershipService } from "../../services/TeamMembershipService";
import { TransactionService } from "../../services/TransactionService";
import { UserService } from "../../services/UserService";
import { env } from "./settings";
import { randomName } from "./utils";

function createXHR() {
    return new XMLHttpRequest();
}
class MockStorageService {
    storage = {}
    setItem(key, value) {
        return of(this.storage[key] = value);
    }
    getItem(key) { return of(this.storage[key]) };
}
const storageService = new MockStorageService();


class MockNotificationService {
    unregisterNotification() { }
    registerForPushNotificationsAsync(): any { }
    expoPushToken;
    notificationReceived;
    requestService;
    eventSubscription;
    _handleNotification = notification => { };
    dispatch(dispatchedData) { }
    setToken(expoPushToken) { }
    sendPushNotification(title, body, data) { };
    savePushNotificationToken(data: PushNotificationTokenModel) { }

}

class MockWebSocketService {
    modelUpdated = new Subject();
    connect() { };
}
const requestService = new RequestService({ storageService, env } as any);
requestService.rxjs_ajax_options = { createXHR }; // To Allow CORS
const notificationService = new MockNotificationService();
const webSocketService = new MockWebSocketService();
const authService = new AuthService({ requestService, storageService, notificationService, webSocketService } as any);
const teamMembershipService = new TeamMembershipService({ requestService });
const userService = new UserService({ requestService });
const dateTimeService = new DateTimeService({});
const accountService = new AccountService({ requestService, dateTimeService });
const transactionService = new TransactionService({ requestService, dateTimeService });
const projectService = new ProjectService({ requestService, dateTimeService });

const userPassword = 'defaultPassword';

function registerUser(name) {
    return authService.register({ username: name, password: userPassword, email: `${name}@gmail.com` });
}

function createAccount(name): Observable<AccountModel> {
    return accountService.create({ username: name });
}

let currentUser: ApiResponseRegister
describe('Project', () => {
    beforeEach(async () => {
        const name = randomName('T');
        const registeredUser = await registerUser(name).toPromise();
        await authService.login({ username: registeredUser.username, password: userPassword }).toPromise();
        currentUser = registeredUser;
    });
    test('initial log test', () => {
        expect(3).toBe(3);
    });

    test('create project', async () => {
        const data = new Project();
        data.title = `Project ${randomName('P')}`;
        const createdData = await projectService.create(data).toPromise();
        expect(createdData).toBeDefined();
        expect(createdData.title).toBe(data.title);
        expect(createdData.created_by).toBe(currentUser.id);
        const list = await projectService.getAll().toPromise();
        expect(list.some((item) => item.id == createdData.id)).toBeTruthy();
    });

    test('update project', async () => {
        const data = new Project();
        data.title = `Update Project ${randomName('C')}`;
        const createdData = await projectService.create(data).toPromise();
        expect(createdData).toBeDefined();
        const new_data = { ...createdData, title: `${data.title}_updated` };
        const updatedData = await projectService.update(new_data.id, new_data).toPromise();
        expect(updatedData).toBeDefined();
        expect(updatedData.title).toBe(new_data.title);
    });

    test('create project with participants', async () => {
        const data = new Project();
        data.title = `Project with Participants ${randomName('P')}`;
        const user1 = await registerUser(randomName('U1')).toPromise();
        const user2 = await registerUser(randomName('U2')).toPromise();
        const user3 = await registerUser(randomName('U3')).toPromise();
        data.participants = [user1.id, user2.id, user3.id];
        const createdData = await projectService.create(data).toPromise();
        expect(createdData.participants).toBeDefined();
        expect(createdData.participants.length).toBeGreaterThan(0);
        expect(createdData.participants.sort()).toEqual(data.participants.sort());
    });

    test('update project with new participants', async () => {
        const data = new Project();
        data.title = `New Participants ${randomName('C')}`;
        const user1 = await registerUser(randomName('U1')).toPromise();
        const user2 = await registerUser(randomName('U2')).toPromise();
        const user3 = await registerUser(randomName('U3')).toPromise();
        data.participants = [user1.id, user2.id];
        const createdData = await projectService.create(data).toPromise();
        expect(createdData.participants).toBeDefined();
        expect(createdData.participants.sort()).toEqual(data.participants.sort());
        const new_data = { ...createdData, paticipants: [user1.id, user3.id] };
        const updatedData = await projectService.update(new_data.id, new_data).toPromise();
        expect(updatedData).toBeDefined();
        expect(updatedData.participants.sort()).toEqual(new_data.participants.sort());
    });

    test('update project with by removing participants', async () => {
        const data = new Project();
        data.title = `RemoveParticipants ${randomName('C')}`;
        const user1 = await registerUser(randomName('U1')).toPromise();
        const user2 = await registerUser(randomName('U2')).toPromise();
        const user3 = await registerUser(randomName('U3')).toPromise();
        data.participants = [user1.id, user2.id, user3.id];
        const createdData = await projectService.create(data).toPromise();
        expect(createdData.participants).toBeDefined();
        expect(createdData.participants.sort()).toEqual(data.participants.sort());
        const new_data = { ...createdData, participants: [] };
        const updatedData = await projectService.update(new_data.id, new_data).toPromise();
        expect(updatedData.participants.sort()).toEqual(new_data.participants.sort());
    });

    test('get project by participant', async () => {
        const data = new Project();
        data.title = randomName('Prj1');
        const user1 = await registerUser(randomName('Prj1_User')).toPromise();
        const user2 = await registerUser(randomName('Prj1_User')).toPromise();
        const user3 = await registerUser(randomName('Prj1_User')).toPromise();
        data.participants = [user1.id, user2.id, user3.id];
        const prj1 = await projectService.create(data).toPromise();

        const data2 = new Project();
        data2.title = randomName('Prj2');
        const user4 = await registerUser(randomName('Prj2_User')).toPromise();
        const user5 = await registerUser(randomName('Prj2_User')).toPromise();
        const user6 = await registerUser(randomName('Prj2_User')).toPromise();

        data.participants = [user3.id, user4.id, user5.id, user6.id];
        const prj2 = await projectService.create(data).toPromise();

        let projectsOfUser1 = await projectService.getAllByParticipant(user1.id).toPromise();
        let projectsOfUser3 = await projectService.getAllByParticipant(user3.id).toPromise();
        console.log({ projectsOfUser1 });
        expect(projectsOfUser1.length).toEqual(1);
        expect(projectsOfUser1[0].id).toEqual(prj1.id);

        expect(projectsOfUser3.length).toEqual(2);
        expect(projectsOfUser3.some((item) => {
            return item.id === prj2.id || item.id === prj2.id;
        })).toBeTruthy();

        const loginUser = await registerUser(randomName('Login_User')).toPromise();
        await authService.login({ username: loginUser.username, password: userPassword }).toPromise();
        currentUser = loginUser;

        projectsOfUser1 = await projectService.getAllByParticipant(user1.id).toPromise();
        projectsOfUser3 = await projectService.getAllByParticipant(user3.id).toPromise();
        expect(projectsOfUser1.length).toEqual(0);
        expect(projectsOfUser3.length).toEqual(0);


    });


})
