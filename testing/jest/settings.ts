import { EnvironmentVariables } from "../../environment";

export const env: EnvironmentVariables = {
    url: process.env.API_TEST_URL ? process.env.API_TEST_URL : 'http://0.0.0.0:8888/',
    webSocketUrl: '',
    envName: 'test'
};
