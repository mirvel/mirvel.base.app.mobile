import "jest";
import { Observable, of, Subject } from "rxjs";
import { XMLHttpRequest } from 'xmlhttprequest';
import { AccountService } from "../../services/AccountService";
import { ApiResponseRegister, AuthService } from "../../services/AuthService";
import { DateTimeService } from "../../services/DateTimeService";
import { AccountModel } from "../../services/models/AccountModel";
import { Project } from "../../services/models/Project";
import { PushNotificationTokenModel } from "../../services/models/PushNotificationTokenModel";
import { Transaction } from "../../services/models/Transaction";
import { ProjectService } from "../../services/ProjectService";
import { RequestService } from "../../services/RequestService";
import { TeamMembershipService } from "../../services/TeamMembershipService";
import { TransactionService } from "../../services/TransactionService";
import { UserService } from "../../services/UserService";
import { env } from "./settings";
import { randomName } from "./utils";

function createXHR() {
    return new XMLHttpRequest();
}
class MockStorageService {
    storage = {}
    setItem(key, value) {
        return of(this.storage[key] = value);
    }
    getItem(key) { return of(this.storage[key]) };
}
const storageService = new MockStorageService();

console.log(process.env.API_TEST_URL);

class MockNotificationService {
    unregisterNotification() { }
    registerForPushNotificationsAsync(): any { }
    expoPushToken;
    notificationReceived;
    requestService;
    eventSubscription;
    _handleNotification = notification => { };
    dispatch(dispatchedData) { }
    setToken(expoPushToken) { }
    sendPushNotification(title, body, data) { };
    savePushNotificationToken(data: PushNotificationTokenModel) { }

}

class MockWebSocketService {
    modelUpdated = new Subject();
    connect() { };
}
const requestService = new RequestService({ storageService, env } as any);
requestService.rxjs_ajax_options = { createXHR }; // To Allow CORS
const notificationService = new MockNotificationService();
const webSocketService = new MockWebSocketService();
const authService = new AuthService({ requestService, storageService, notificationService, webSocketService } as any);
const teamMembershipService = new TeamMembershipService({ requestService });
const userService = new UserService({ requestService });
const dateTimeService = new DateTimeService({});
const accountService = new AccountService({ requestService, dateTimeService });
const transactionService = new TransactionService({ requestService, dateTimeService });
const projectService = new ProjectService({ requestService, dateTimeService });

const userPassword = 'defaultPassword';

function registerUser(name) {
    return authService.register({ username: name, password: userPassword, email: `${name}@gmail.com` });
}

function createAccount(name): Observable<AccountModel> {
    return accountService.create({ username: name });
}

let currentUser: ApiResponseRegister
describe('Account', () => {
    beforeEach(async () => {
        const name = randomName('T');
        const registeredUser = await registerUser(name).toPromise();
        await authService.login({ username: registeredUser.username, password: userPassword }).toPromise();
        currentUser = registeredUser;
    });
    test('initial log test', () => {
        expect(3).toBe(3);
    });

    test('get account transactions', async () => {
        const recipientName = randomName('TA');
        const recipient = await createAccount(recipientName).toPromise();

        const project = new Project();
        project.title = `Project ${randomName('P')}`;
        const createdProject = await projectService.create(project).toPromise();


        const transaction = new Transaction();
        transaction.recipient = recipient.id;
        transaction.amount = 300;
        transaction.source_FK = 3;
        transaction.source_Type = 'Source';
        transaction.title = 'Points';
        transaction.project = createdProject.id;
        transaction.date = new Date();
        const createdTransaction = await transactionService.create(transaction).toPromise();
        expect(createdTransaction).toBeDefined;
        expect(createdTransaction.recipient).toBe(recipient.id);
        expect(createdTransaction.created_by).toBe(currentUser.id);
        const transactions = await transactionService.getAll().toPromise();
        expect(transactions.length).toBeFalsy();
        const account_recipient_transactions = await accountService.transactions(recipient.id).toPromise();
    });

    test('get account', async () => {
        const recipientName = randomName('GA');
        const recipient = await createAccount(recipientName).toPromise();
        const account = await accountService.get(recipient.id).toPromise();
        expect(account.username).toBe(recipientName);
    })




})
