import "jest";
import { of, Subject } from "rxjs";
import { XMLHttpRequest } from 'xmlhttprequest';
import { EnvironmentVariables } from "../../environment";
import { AccountService } from "../../services/AccountService";
import { ApiResponseRegister, AuthService } from "../../services/AuthService";
import { DateTimeService } from "../../services/DateTimeService";
import { PushNotificationTokenModel } from "../../services/models/PushNotificationTokenModel";
import { TeamMembership } from "../../services/models/TeamMembership";
import { RequestService } from "../../services/RequestService";
import { TeamMembershipService } from "../../services/TeamMembershipService";
import { UserService } from "../../services/UserService";
import { env } from "./settings";
import { randomName } from "./utils";

function createXHR() {
    return new XMLHttpRequest();
}
class MockStorageService {
    storage = {}
    setItem(key, value) {
        return of(this.storage[key] = value);
    }
    getItem(key) { return of(this.storage[key]) };
}
const storageService = new MockStorageService();

class MockNotificationService {
    unregisterNotification() { }
    registerForPushNotificationsAsync(): any { }
    expoPushToken;
    notificationReceived;
    requestService;
    eventSubscription;
    _handleNotification = notification => { };
    dispatch(dispatchedData) { }
    setToken(expoPushToken) { }
    sendPushNotification(title, body, data) { };
    savePushNotificationToken(data: PushNotificationTokenModel) { }

}

class MockWebSocketService {
    modelUpdated = new Subject();
    connect() { };
}
const requestService = new RequestService({ storageService, env } as any);
requestService.rxjs_ajax_options = { createXHR }; // To Allow CORS
const notificationService = new MockNotificationService();
const webSocketService = new MockWebSocketService();
const authService = new AuthService({ requestService, storageService, notificationService, webSocketService } as any);
const teamMembershipService = new TeamMembershipService({ requestService });
const userService = new UserService({ requestService });
const dateTimeService = new DateTimeService({});
const accountService = new AccountService({ requestService, dateTimeService });

const userPassword = 'defaultPassword';

function registerUser() {
    const lastUserSuffix = randomName('z');
    return authService.register({ username: `user${lastUserSuffix}`, password: userPassword, email: `user${lastUserSuffix}@gmail.com` });
}

function createAccount(name) {
    return accountService.create({ username: name });
}

let currentUser: ApiResponseRegister
describe('TeamMembers', () => {
    beforeEach(async () => {
        const registeredUser = await registerUser().toPromise();
        await authService.login({ username: registeredUser.username, password: userPassword }).toPromise();
        currentUser = registeredUser;
    });
    test('initial log test', () => {
        expect(3).toBe(3);
    });

    test('create team member', async () => {
        const teamMember = new TeamMembership();
        teamMember.member = authService.currentUser.value.id;
        const teamMemberCreated = await teamMembershipService.create(teamMember).toPromise();
        expect(teamMemberCreated.member).toBe(currentUser.id);
    });

    test('create account', async () => {
        const userName1 = randomName('U');
        const userName2 = randomName('U');
        const userName3 = randomName('U');
        const r1 = await createAccount(userName1).toPromise();
        const r2 = await createAccount(userName2).toPromise();
        const r3 = await createAccount(userName3).toPromise();
        expect(r1.username).toBe(userName1);
        expect(r2.username).toBe(userName2);
        expect(r3.username).toBe(userName3);
    })

    test('get all accounts', async () => {
        const userName1 = randomName('U');
        const userName2 = randomName('U');
        const userName3 = randomName('U');
        const r1 = await createAccount(userName1).toPromise();
        const r2 = await createAccount(userName2).toPromise();
        const r3 = await createAccount(userName3).toPromise();
        const response = await accountService.getAll().toPromise();
        response.forEach(element => {
            expect(element.created_by).toBe(currentUser.id);
        });
    });
})
