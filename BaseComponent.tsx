import React from 'react';
import { AppContext } from './AppContext';
import { ServiceProviders } from './AppProvider';
interface AppContextInterface {
    serviceProviders: ServiceProviders
}
export class BaseComponent extends React.Component<BaseComponentProps, any> {
    [key: string]: any;
    public static contextType = AppContext;
    context: AppContextInterface;
}

interface BaseComponentProps {
    [key: string]: any;
    navigation?: any
}

