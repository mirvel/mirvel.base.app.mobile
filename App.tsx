import * as SplashScreen from 'expo-splash-screen';
import { Asset } from 'expo-asset';
import React from 'react';
import { Image } from 'react-native';
import * as Sentry from 'sentry-expo';
import { AppProvider } from './AppProvider';
import { Block, GalioProvider } from './components/custom/src';
import { argonTheme, articles, Images } from './constants';
import getEnvVars from './environment';
import Screens from './navigation/Screens';

Sentry.init({
  dsn: 'https://3c65d7b026154d39a0d529520cc48a88@o501152.ingest.sentry.io/5581950',
  enableInExpoDevelopment: true,
  environment: getEnvVars().envName,
  debug: true, // Sentry will try to print out useful debugging information if something goes wrong with sending an event. Set this to `false` in production.
});


// cache app images
const assetImages = [
  Images.Onboarding,
  Images.LogoOnboarding,
  Images.Logo,
  Images.Pro,
  Images.ArgonLogo,
  Images.iOSLogo,
  Images.androidLogo,
  Images.roomLogo
];

// cache product images
articles.map(article => assetImages.push(article.image));

function cacheImages(images) {
  return images.map(image => {
    if (typeof image === 'string') {
      return Image.prefetch(image);
    } else {
      return Asset.fromModule(image).downloadAsync();
    }
  });
}


SplashScreen.preventAutoHideAsync();

export default class App extends React.Component<any> {
  state = {
    isLoadingComplete: false,
    notification: { origin: 'no', data: 'no' },
  }
  componentDidMount() {
    this._loadResourcesAsync().then(() => {
      this._handleFinishLoading();
      console.log('loaded');
      SplashScreen.hideAsync();

    });
  }

  render() {
    if (this.state.isLoadingComplete)  {
      return (
        <AppProvider>
          <GalioProvider theme={argonTheme}>
            <Block flex>
              <Screens />
            </Block>
          </GalioProvider>
        </AppProvider>

      );
    }
  }

  _loadResourcesAsync = async () => {
    Promise.all(cacheImages(assetImages)).then(() => {
      return Promise.resolve();
    }).catch((e) => {
      return Promise.reject(e);
    })
  };

  _handleLoadingError = error => {
    // In this case, you might want to report the error to your error
    // reporting service, for example Sentry
    console.warn(error);
  };

  _handleFinishLoading = () => {
    this.setState({ isLoadingComplete: true });
  };

}
